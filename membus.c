#include "membus.h"
#include <stddef.h>
#include "ppu.h"
#include "logger.h"
#include "loopy.h"

static byte_t devnull = 0x69;

#define CARD_START 0x4020
#define CONTROLLER_A 0x4016
#define CONTROLLER_B 0x4017

#define toggle(var) var = !(var);

byte_t* current_cpu_byte(Membus* bus, MemoryBusMode mode)
{
    return cpu_byte(bus, bus->address_bus, mode);
}

byte_t* cpu_byte(Membus* bus, uint16_t addr, MemoryBusMode mode) {
    if(addr <= 0x1FFF) {
        return &bus->cpu_mem[addr & 0x07FF];
    } else if (addr >= 0x2000 && addr <= 0x3fff) {
        // Mirror [0x2000;0x2007] is mirrored from [0x2008;0x3fff]
        return &bus->ppu_reg[addr & 0x7];
    } else if (addr >= 0x4000 && addr <= 0x4017) {
        return &bus->apu_reg[addr - 0x4000];
    } else if (addr >= CARD_START) {
        if(bus->card == NULL) return NULL;
        return cardridge_cpu_bus(bus->card, addr, mode);
    } else if(addr >= 0x4018 && addr <= 0x401f) {
        // This is some debug hardware in some revisions, so i'll just send it to nirvana
        return &devnull;
    }
    return NULL;
}

byte_t* ppu_byte(Membus* bus, uint16_t addr, MemoryBusMode mode) {
    // The ppu only has an 14-bit address bus, so 0x4000 wraps around to 0x0000
    addr &= 0x3FFF;
    if(addr <= 0x1FFF) {
        if(bus->card == NULL) return NULL;
        return cardridge_chr(bus->card, addr);
    } else if(addr >= 0x2000 && addr <= 0x3eFF) {
        if(bus->card == NULL) return NULL;
        addr &= 0x0FFF;
        if(bus->card->mapper_type == AxROM)
            return &bus->nametables[bus->card->registers[0] >> 4][addr & 0x3FF];
        if(bus->card->nametable_mirroring == VerticalMirror) {
            if(addr >= 0x000 && addr <= 0x3FF) return &bus->nametables[0][addr & 0x3FF];
            if(addr >= 0x400 && addr <= 0x7FF) return &bus->nametables[1][addr & 0x3FF];
            if(addr >= 0x800 && addr <= 0xBFF) return &bus->nametables[0][addr & 0x3FF];
            if(addr >= 0xC00 && addr <= 0xFFF) return &bus->nametables[1][addr & 0x3FF];
        } else {
            if(addr >= 0x000 && addr <= 0x3FF) return &bus->nametables[0][addr & 0x3FF];
            if(addr >= 0x400 && addr <= 0x7FF) return &bus->nametables[0][addr & 0x3FF];
            if(addr >= 0x800 && addr <= 0xBFF) return &bus->nametables[1][addr & 0x3FF];
            if(addr >= 0xC00 && addr <= 0xFFF) return &bus->nametables[1][addr & 0x3FF];
        }
    } else if(addr >= 0x3000 && addr <= 0x3eFF) {
        // Mirrors of $2000-$2eff
        return ppu_byte(bus, addr - 0x1000, mode);
    } else if(addr >= PALETTE_START && addr <= 0x3fff) {
        addr &= 0x001F;
        if (addr == 0x10) addr = 0x00;
        else if (addr == 0x14) addr = 0x04;
        else if (addr == 0x18) addr = 0x08;
        else if (addr == 0x1c) addr = 0x0c;
        return &bus->color_palettes[addr];
    }
    return NULL;
}

void init_membus(Membus* bus, nes_ppu* ppu, struct s_6502_cpu* cpu) {
    bus->card = NULL;
    bus->ppu = ppu;
    bus->cpu = cpu;
}

int handle_ppu_write_access(Membus* bus, byte_t* loc) {
    if(bus->address_bus == PPUAddr) {
        if(bus->ppu->address_latch == false) {
            bus->ppu->t_reg = (uint16_t)((bus->data_bus & 0x3F) << 8) | (bus->ppu->t_reg & 0x00FF);

            toggle(bus->ppu->address_latch);
        } else {
            bus->ppu->t_reg = (bus->ppu->t_reg & 0xFF00) | bus->data_bus;
            bus->ppu->v_reg = bus->ppu->t_reg;
            bus->vaddr_bus = bus->ppu->v_reg;

            toggle(bus->ppu->address_latch);
        }
    } else if (bus->address_bus == PPUData) {

        byte_t* to_write = ppu_byte(bus, bus->ppu->v_reg, Write);
        if(to_write == NULL) {
            log_error("PPU Segfault: Write operation at %#04x", bus->ppu->v_reg);
            return 1;
        } else {
            *to_write = bus->data_bus;
            bus->ppu->v_reg += vram_increment(bus->ppu);
        }
    } else if (bus->address_bus == PPUScroll) {
        if(bus->ppu->address_latch == false) {
            bus->ppu->fine_x = bus->data_bus & 0x07;
            loopy_set_coarse_x(&bus->ppu->t_reg, bus->data_bus >> 3);

            toggle(bus->ppu->address_latch);
        } else {
            loopy_set_fine_y(&bus->ppu->t_reg, bus->data_bus & 0x07);
            loopy_set_coarse_y(&bus->ppu->t_reg, bus->data_bus >> 3);

            toggle(bus->ppu->address_latch);
        }
    } else if (bus->address_bus == PPUCtrl) {
        loopy_set_nt(&bus->ppu->t_reg, bus->data_bus & 0x3);
    } else if (bus->address_bus == PPUOamData) {
        bus->ppu->OAM.bytes[oamaddr(bus->ppu)] = bus->data_bus;
        oamaddr(bus->ppu)++;
    } else if (bus->address_bus == PPUDma) {
        bus->ppu->dma_page = bus->data_bus;
        bus->ppu->dma_write_addr = oamaddr(bus->ppu);
        bus->ppu->dma_read_addr = 0x00;
        bus->ppu->dma_active = true;
        bus->ppu->dma_cpu_cycle = 0;
        bus->cpu->rdy_line++;
    }
    return 0;
}

void handle_ppu_read_access(Membus* bus, byte_t* loc) {
	// Sets the register to the buffer and loads the currently requested byte into the buffer
    if(bus->address_bus == PPUData) {
        *loc = bus->ppu_data_buffer;
        bus->ppu_data_buffer = *ppu_byte(bus, bus->ppu->v_reg, Read);

        // The palette memory doesn't need this extra clock cycle and can be read in 1 cycle
        if(bus->ppu->v_reg >= PALETTE_START) *loc = bus->ppu_data_buffer;

        bus->ppu->v_reg += vram_increment(bus->ppu);
    }
}

void handle_ppu_post_read(Membus* bus, byte_t* loc) {
    if(bus->address_bus == PPUStatus) {
        // The vblank is cleared after reading the register
        *loc &= ~0x80;
        bus->ppu->address_latch = false;
    }
}

int cycle_bus(Membus* bus, MemoryBusMode* mode) {
    byte_t* loc = current_cpu_byte(bus, *mode);
    if(*mode == Write) {
        if(loc == NULL) {
            log_error("Segfault: Write operation at %#02x", bus->address_bus);
        } else {
            if(bus->address_bus == CONTROLLER_A) {
                if((bus->data_bus & 0x1) == 0) controller_buffer(&bus->player1);
            } else if(bus->address_bus == CONTROLLER_B) {
                if((bus->data_bus & 0x1) == 0) controller_buffer(&bus->player2);
            } if(bus->address_bus >= CARD_START) {
                if(cardridge_write(bus->card, bus->address_bus, bus->data_bus)) {
                    log_error("Segfault: Write operation at %#02x, cardridge rejected", bus->address_bus);
                }
            } else {
                *loc = bus->data_bus;
            }
            if(handle_ppu_write_access(bus, loc)) return 1;
#ifdef log_access
            log_trace("Write %#02x to %#02x", bus->data_bus, bus->address_bus);
#else
#ifdef log_write
            log_trace("Write %#02x to %#02x", bus->data_bus, bus->address_bus);
#endif
#endif
        }
        *mode = Read;
    } else {
        if(loc == NULL) {
            log_error("Segfault: Read operation at %#02x", bus->address_bus);
        } else {
            if(bus->address_bus == CONTROLLER_A) {
                bus->data_bus = controller_read(&bus->player1);
            } else if(bus->address_bus == CONTROLLER_B) {
                bus->data_bus = controller_read(&bus->player2);
            } else if(bus->address_bus == PPUOamData) {
                bus->data_bus = bus->ppu->OAM.bytes[oamaddr(bus->ppu)];
            } else {
                handle_ppu_read_access(bus, loc);
                bus->data_bus = *loc;
                handle_ppu_post_read(bus, loc);
            }
#ifdef log_access
            log_trace("Read %#02x from %#02x", bus->data_bus, bus->address_bus);
#endif
        }
    }
    return 0;
}

int cycle_ppu_bus(Membus* bus) {
    byte_t* ppu_loc  = ppu_byte(bus, bus->vaddr_bus, Read);
    if(ppu_loc != NULL) {
        bus->vdata_bus = *ppu_loc;
    } else {
        return 1;
    }
    return 0;
}
