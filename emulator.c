#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/time.h>
#include <assert.h>

#include "djevent.h"
#include "logger.h"
#include "djwindow.h"
#include "drw.h"
#include "util.h"

#include "disassembler.h"
#include "debug_drawer.h"
#include "x_keys.h"
#include "ines.h"
#include "debug.h"
#include "loopy.h"
#include "button.h"
#include "nes.h"
#include "mappers/mapper_004.h"
#include "config.h"

//#define LOG_INSTRUCTIONS
#define JUMP_TO_ADDR  0xc0c3
// only draw a frame every n frames.
// This effectively cuts the displayed framerate to 60/frame_division, but the rendering stays the same
#define skip_cycles 126597614

struct s_nes nes;
struct s_disasm disasm;

DJWindow* window;
bool should_stop = false;
bool run_nonstop = false;
bool stack_or_zp = false;
byte_t current_palette = 0;
bool should_drw_tile_lines = false;
bool should_drw_attribute_lines = false;
bool update_ui = true;

DJimg* left_table = NULL;
DJimg* right_table = NULL;
DJimg* fb = NULL;
DJimg* nametable_viewer = NULL;

struct timeval last_drw;
struct timeval last_window_update;
struct timeval last_clock;

#ifdef LOG_INSTRUCTIONS
FILE* logfile = NULL;
#endif

void drw(DJEvent* ev, void* a);
void drw_nametables(struct s_nes* nes);
void drw_pattern_table();

byte_t observe_byte(uint16_t addr) {
    return *cpu_byte(&nes.bus, addr, Observer);
}
byte_t observe_ppu(uint16_t addr) {
    return *ppu_byte(&nes.bus, addr, Observer);
}

const char* rom_file = NULL;

Button btn_tgl_stack;
const char* lbl_tgl_stack = "Stack";
Button btn_tgl_pt;
const char* lbl_tgl_pt = "Pattern tables";
Button btn_tgl_disassembly;
const char* lbl_tgl_disassembly = "Disassembly";
Button btn_tgl_nt;
const char* lbl_tgl_nt = "Nametables";

#define num_buttons 4
Button* buttons[num_buttons] = {
        &btn_tgl_stack,
        &btn_tgl_pt,
        &btn_tgl_disassembly,
        &btn_tgl_nt
};

void on_click(DJEvent* ev, void* a) {
    for(int i=0; i<num_buttons; i++) {
        btn_update(buttons[i], ev->buttonpress.x, ev->buttonpress.y);
    }
    drw(0, 0);
}


void key_callback(DJEvent* ev, void* a) {
    switch(ev->keypress.key) {
        case(K_ESC):
            should_stop = true;
            break;
        case(K_SPACE):
            for (int i = 0; i < 3 && !nes.lockup; i++) {
                clock(&nes);
            }
            drw(0, 0);
            break;
        case(K_BACKSPACE):
            run_nonstop = !run_nonstop;
            break;
        case(K_1): {
            unsigned long n=0;
            log_trace("Fast forwarding until i get to pc %#04x", JUMP_TO_ADDR);
            while(nes.cpu.program_counter != JUMP_TO_ADDR && !nes.lockup) {
                clock(&nes);
                n++;
            }
            log_trace("Skipped %lu cycles", n);
            drw(0, (void*)1);
            break; }
        case(K_2):
            for(int i=0; i<skip_cycles && !nes.lockup; i++) {
                clock(&nes);
            }
            drw(0, 0);
            break;
        case(K_3):
            nes.ppu.begin_log = true; break;
		case(K_4):
			// when blarggs tests run we can expect a valid string output at $6000+
#ifdef NDEBUG
			if(observe_byte(0x6001) == 0xDE && observe_byte(0x6002) ) {
#endif
				log_info("[$6000+] %s", cpu_byte(&nes.bus, 0x6004, Observer));
#ifdef NDEBUG
			}
#endif
			break;
        case(K_5):
            while(!(observe_byte(PPUStatus) & 0x80)) {
                clock(&nes);
            }
            break;
        case(K_8):
            should_drw_tile_lines = !should_drw_tile_lines; break;
        case(K_9):
            should_drw_attribute_lines = !should_drw_attribute_lines; break;
        case(K_0):
            update_ui = !update_ui;
            log_debug("Updating up: %d", update_ui);
            break;
        case(K_B):
            stack_or_zp = !stack_or_zp;
			drw_stack(&nes.cpu, window, stack_or_zp, &btn_tgl_stack);
            break;
        case(K_P):
            current_palette++;
			drw_pattern_table();
            break;
        default:
            //log_info("The key %u was pressed", ev->keypress.key);
            break;
    }
}

void controller_press(DJEvent* ev, void* a) {
    struct s_nes* nes = (struct s_nes*) a;
    switch(ev->keypress.key) {
        case(K_W): controller_set(&nes->bus.player1, Up_Btn, true); break;
        case(K_A): controller_set(&nes->bus.player1, Left_Btn, true); break;
        case(K_S): controller_set(&nes->bus.player1, Down_Btn, true); break;
        case(K_D): controller_set(&nes->bus.player1, Right_Btn, true); break;
        case(K_RETURN): controller_set(&nes->bus.player1, Start_Btn, true); break;
        case(K_BACKSLASH): controller_set(&nes->bus.player1, Select_Btn, true); break;
        case(K_I): controller_set(&nes->bus.player1, A_btn, true); break;
        case(K_O): controller_set(&nes->bus.player1, B_btn, true); break;
        default: break;
    }
	drw_controller(&nes->bus.player1, window, 50, fb_height*fb_scale + 100, 320);
}
void controller_release(DJEvent* ev, void* a) {
    struct s_nes* nes = (struct s_nes*) a;
    switch(ev->keyrelease.key) {
        case(K_W): controller_set(&nes->bus.player1, Up_Btn, false); break;
        case(K_A): controller_set(&nes->bus.player1, Left_Btn, false); break;
        case(K_S): controller_set(&nes->bus.player1, Down_Btn, false); break;
        case(K_D): controller_set(&nes->bus.player1, Right_Btn, false); break;
        case(K_RETURN): controller_set(&nes->bus.player1, Start_Btn, false); break;
        case(K_BACKSLASH): controller_set(&nes->bus.player1, Select_Btn, false); break;
        case(K_I): controller_set(&nes->bus.player1, A_btn, false); break;
        case(K_O): controller_set(&nes->bus.player1, B_btn, false); break;
        default: break;
    }
	drw_controller(&nes->bus.player1, window, 50, fb_height*fb_scale + 100, 320);
}

void drw_registers() {
	int x = fb_width * fb_scale + 240;
    int y = 40;
    draw_rect(window, x, y, 300, 130, WHITE);
    print_text(window, x + 50, y+20, "Registers: ", RED);
    char texts[5][32];
    sprintf(texts[0], " A: %s%#02x [%d]", nes.cpu.accumulator == 0 ? "0x" : "", nes.cpu.accumulator, nes.cpu.accumulator);
    sprintf(texts[1], " X: %s%#02x [%d]", nes.cpu.reg_x == 0 ? "0x" : "", nes.cpu.reg_x, nes.cpu.reg_x);
    sprintf(texts[2], " Y: %s%#02x [%d]", nes.cpu.reg_y == 0 ? "0x" : "", nes.cpu.reg_y, nes.cpu.reg_y);
    sprintf(texts[3], "PC: %s%#04x", nes.cpu.program_counter == 0 ? "0x" : "", nes.cpu.program_counter);
    sprintf(texts[4], "SP: %s%#04x", nes.cpu.stack_ptr == 0 ? "0x" : "", nes.cpu.stack_ptr);
    for(int i=0; i<5; i++) {
        print_text(window, x+10, y+40 + 15 * i, texts[i], BLACK);
    }
	static FlagMask masks[] = {NegativeMask, OverflowMask, 0, BreakMask, DecimalMask, InterruptMask, ZeroMask, CarryMask};
	static const char* labels[] = {"N", "V", "-", "B", "D", "I", "Z", "C"};
	for(int i=0; i<8; i++) {
		Color c = flag_set(&nes.cpu, masks[i]) ? DARK_GREEN : LIGHT_RED;
		if(i == 2) c = BLACK;
		print_text(window, x + 10*(i+1), y + 40 + 15 * 5, labels[i], c);
	}

    sprintf(texts[0], "PPUCTRL: $%02X", nes.ppu.registers[0]);
    sprintf(texts[1], "PPUMASK: $%02X", nes.ppu.registers[1]);
    sprintf(texts[2], "PPUSTAT: $%02X", nes.ppu.registers[2]);
    for(int i=0; i<3; i++) {
        print_text(window, x+160, y+40 + 15 * i, texts[i], BLACK);
    }
}
void drw_sprites() {
    int x = fb_width * fb_scale + 900;
    int y = 50;
    draw_rect(window, x, y, 200, (64 * 15), DARK_GRAY);
    for(int i=0; i<64; i++) {
        char txt[32];
        struct s_oamentry sprite = nes.ppu.OAM.entries[i];
        sprintf(txt, "X: %02x, Y: %02x, A: %02x, id: %02x", sprite.x_pos, sprite.y_pos, sprite.attributes, sprite.tile_id);
        print_text(window, x+5, y + (i+1)*15, txt, WHITE);
    }
}
void drw_pattern_table() {
    if(!btn_tgl_pt.state) return;
    if(left_table != NULL) {
        read_pattern_table(&nes.ppu, &nes.bus, image_raw_data(left_table), 0, current_palette % 8);
        draw_image(window, left_table, 25, fb_height * fb_scale + 100);
    }
    if(right_table != NULL) {
        read_pattern_table(&nes.ppu, &nes.bus, image_raw_data(right_table), 1, current_palette % 8);
        draw_image(window, right_table, 50 + 256, fb_height * fb_scale + 100);
    }

    int w = 15;
    int h = 15;
    int b = 8;
    draw_rect(window, 50-2, fb_height * fb_scale + 256 + 100 + 50 - 2, 16*w+3*b+4, 2*h+b+4, GRAY);
    for(int i=0; i<2; i++) {
        for(int j=0; j<4; j++) {
            int index = i * 4 + j;
            int x = 50 + (4 * w + 1 * b) * j;
            int y = fb_height * fb_scale + 256 + 100 + 50 + (h + b) * i;
            if(index == (current_palette % 8))
                draw_rect(window, x-2, y-2, w*4+4, h+4, WHITE);
            // Draw palette index
            for(int c=0; c<4; c++) {
                struct s_rgb color = get_color(&nes.ppu, index, c, Observer);
                draw_rect(window, x + (w + 0) * c, y, w, h, rgb_to_color(color));
            }
        }
    }
}
void drw_screen() {
    int x = 50;
    int y = 50;
    draw_image(window, fb, x, y);
#ifdef drw_nametable_overlay
    int start_x = 53;
    int start_y = 62;
    for(int x=0; x<32; x++) {
        for(int y=0; y<30; y++) {
            uint16_t addr = 0x2000 + x + y * 32;
            byte_t entry = *ppu_byte(&bus, addr, Observer);
            char id[5];
            if(isprint(entry)) {
                sprintf(id, " %c", entry);
            } else {
                sprintf(id, "%02X", entry);
            }
            print_text(window, start_x + x * 16, start_y + y * 16, id, WHITE);
        }
    }
#endif
    int tile_w = 8 * fb_scale;
    if(should_drw_tile_lines) {
        for(int i=1; i<32; i++) {
            draw_line(window, i * tile_w+x, y, i*tile_w+x, fb_height * fb_scale+y, RED);
        }
        for(int i=1; i<30; i++) {
            draw_line(window, x, i*tile_w+y, fb_width * fb_scale + x, i*tile_w+y, RED);
        }
    }
    if(should_drw_attribute_lines) {
        int attr_w = tile_w * 2;
        for(int i=1; i<16; i++) {
            draw_line(window, i * attr_w+x, y, i*attr_w+x, fb_height * fb_scale+y, GREEN);
        }
        for(int i=1; i<16; i++) {
            draw_line(window, x, i*attr_w+y, fb_width * fb_scale + x, i*attr_w+y, GREEN);
        }
        attr_w *= 2;
        for(int i=1; i<8; i++) {
            draw_line(window, i * attr_w+x, y, i*attr_w+x, fb_height * fb_scale+y, BLUE);
        }
        for(int i=1; i<8; i++) {
            draw_line(window, x, i*attr_w+y, fb_width * fb_scale + x, i*attr_w+y, BLUE);
        }
    }
    drw_nametables(&nes);
}
void drw_disassembly(struct s_nes* nes)
{
    if(!btn_tgl_disassembly.state) return;
    int x = fb_width * fb_scale + 80;
    uint16_t pagesize = 64;
    uint16_t page = nes->cpu.current_instruction_pc / pagesize;
    uint16_t start = page * pagesize;

    draw_rect(window, x, 30, 150, pagesize * 15 + 20, DARK_GRAY);


    disasm_update(&disasm);

    char lbl[16];
    // The first valid instruction on the current page
	uint16_t first_instr = first_valid_instruction_after(&disasm, start);
    // Basically fill out all the unknown instructions fields.
	for(int i=start; i<first_instr; i++) {
        sprintf(lbl, "[%#04x]", i);
        print_text(window, x+10, 50 + 15 * (i-start), lbl, GREEN);
        print_text(window, x+70, 50 + 15 * (i-start), "---",WHITE);
	}

    char disassembly[pagesize][24];
    uint16_t addresses[pagesize];

    uint16_t current_instr = disasm_instructions(&disasm, first_instr, pagesize, disassembly, addresses);

    uint16_t diff = first_instr - start;
    for(uint16_t i=diff; i<pagesize; i++) {
        sprintf(lbl, "[%#04x]", addresses[i-diff]);
        print_text(window, x+10, 50 + 15 * i, lbl, GREEN);
        print_text(window, x+70, 50 + 15 * i, disassembly[i-diff], current_instr == i ? LIGHT_RED : WHITE);
    }

}
void drw_nametables(struct s_nes* nes) {
    if(!btn_tgl_nt.state) return;

    uint16_t nt_starts[4] = {0x2000, 0x2400, 0x2800, 0x2c00};
    for(int x_side = 0; x_side<2; x_side++) {
        for(int y_side = 0; y_side<2; y_side++) {
            uint16_t nt_start = nt_starts[x_side+y_side*2];

            for(uint16_t x = 0; x < 32; x++) {
                for(uint16_t y = 0; y < 30; y++) {

                    uint16_t addr = nt_start +y*32 + x;
                    byte_t tileId = *ppu_byte(&nes->bus, addr, Observer);

                    byte_t palette = *ppu_byte(&nes->bus, nt_start + 0x3c0 + (y/4)*8 +(x/4), Observer);

                    if(x % 4 < 2) {
                        if(y % 4 < 2) {
                            palette &= 0x3;
                        } else {
                            palette >>= 4;
                            palette &= 0x3;
                        }
                    } else {
                        if(y % 4 < 2) {
                            palette >>= 2;
                            palette &= 0x3;
                        } else {
                            palette >>= 6;
                            palette &= 0x3;
                        }
                    }

                    for(uint8_t row=0; row<8; row++) {

                        addr = bg_pattern_table_addr(&nes->ppu) + tileId*16 + row;
                        byte_t lsb = *ppu_byte(&nes->bus, addr, Observer);
                        byte_t msb = *ppu_byte(&nes->bus, addr + 8, Observer);

                        for(uint8_t col=0; col<8; col++) {
                            uint8_t pixel = ((msb & 0x80) >> 6) | ((lsb & 0x80) >> 7);
                            lsb <<= 1;
                            msb <<= 1;

                            struct s_rgb c = get_color(&nes->ppu, palette, pixel, Observer);
                            img_pixel(nametable_viewer, 240*y_side+8*y+row, 256*x_side+8*x+col, c);
                        }
                    }
                }
            }
        }
    }
    draw_image(window, nametable_viewer, fb_width * fb_scale + 50 + 20, 200);
    int scrollX = loopy_coarse_x(nes->ppu.last_t_reg)*8 + nes->ppu.fine_x + loopy_nt_x(nes->ppu.last_t_reg)*256;
    int scrollY = loopy_coarse_y(nes->ppu.last_t_reg)*8 + loopy_fine_y(nes->ppu.last_t_reg) + loopy_nt_y(nes->ppu.last_t_reg)*240;
    int w = 256;
    int h = 240;
    if(scrollX >= 256) {
        w = 512 - scrollX;
        draw_rect_outline(window, fb_width * fb_scale + 70, 200 + scrollY, 256-w, h, RED);
    } else if (scrollY >= 240) {
        h = 480 - scrollY;
        draw_rect_outline(window, fb_width * fb_scale + 70 + scrollX, 200, w, 240-h, RED);
    }
    draw_rect_outline(window, fb_width * fb_scale + 70 + scrollX, 200 + scrollY, w, h, RED);
}
void drw(DJEvent* ev, void* a) {
    if(!update_ui) return;
    struct timeval current;
    gettimeofday(&current, NULL);
    unsigned long usec_since_drw = (current.tv_sec - last_drw.tv_sec) * 1000000 + current.tv_usec - last_drw.tv_usec;
    if(usec_since_drw > 10000 || a != 0) {
        fill(window, GRAY);
        drw_disassembly(&nes);
        drw_registers();
        if(!run_nonstop) drw_screen();
        drw_stack(&nes.cpu, window, stack_or_zp, &btn_tgl_stack);
        drw_pattern_table();
        drw_sprites();
		drw_controller(&nes.bus.player1, window, 50, fb_height*fb_scale + 100, 320);

        for(int i=0; i<num_buttons; i++) {
            btn_drw(window, buttons[i]);
        }

        dj_win_flip(window);

        gettimeofday(&last_drw, NULL);
    }

}

static void print_word(const char* lbl, uint16_t addr) {
    log_info("%s: %#04x", lbl, observe_byte(addr) | (observe_byte(addr + 1) << 8));
}

static void render_frame() {
    while(!nes.lockup) {
        clock(&nes);
        if(nes.ppu.frame_finished) break;
    }
    dj_win_update(window);
}

int main(int argc, const char** argv)
{
    init_disasm(&disasm, &nes);
    nes.lockup = false;
    nes.initial_reset = true;
    nes.cycle_count = 0;

    gettimeofday(&last_drw, NULL);
	if(argc < 2) {
        log_error("Usage: %s <rom>", argv[0]);
        exit(1);
	}
    rom_file = argv[1];
    if(access(rom_file, F_OK) != 0) {
        log_error("Could not find the rom file %s", rom_file);
        exit(1);
    }

	nes.use_custom_reset_vector = false;
    if(argc >= 3) {
		if(argv[2][0] == '-') {
			if(!strcmp(argv[2], "-r")) {
				run_nonstop = true;
			} else {
				log_error("Unknown argument %s", argv[2]);
			}
		} else {
			nes.use_custom_reset_vector = true;
			nes.custom_reset_vector = strtol(argv[2], NULL, 16);
			log_info("Starting the program at %#04x", nes.custom_reset_vector);
		}
    }

    init_membus(&nes.bus, &nes.ppu, &nes.cpu);
    init(&nes.cpu, &nes.bus.address_bus, &nes.bus.data_bus);
    init_ppu(&nes.ppu, (byte_t*) &nes.bus.ppu_reg,
             &nes.bus.address_bus, &nes.bus.data_bus,
             &nes.bus.vaddr_bus, &nes.bus.vdata_bus,
             &nes.cpu.nmi_line);
    nes.ppu.membus = &nes.bus;
    nes.bus.card = &nes.card;
	nes.current_romfile = rom_file;

    insert_cardridge(&nes.card, rom_file);
    nes.card.irq_line = &nes.cpu.irq_line;
    load_pal(&nes.ppu, "NTSC_classic.pal");
    init_apu(&nes.apu, &nes.bus.apu_reg);

#ifdef LOG_INSTRUCTIONS
    logfile = fopen("out.log", "w");
#endif

	window = dj_win_init(DRW_X11);
	dj_win_set_title(window, "Nes Emulator 0.0.1");
	dj_win_sub_event(window, KeyPressEvent, key_callback, NULL);
    dj_win_sub_event(window, WinResizeEvent, drw, (void*)1);
    dj_win_sub_event(window, KeyPressEvent, controller_press, &nes);
    dj_win_sub_event(window, KeyReleaseEvent, controller_release, &nes);
    dj_win_sub_event(window, MousePressEvent, on_click, NULL);
    left_table = create_image(window, 256, 256);
    right_table = create_image(window, 256, 256);
    fb = create_image(window, fb_width * fb_scale, fb_height * fb_scale);
    nametable_viewer = create_image(window, 512, 480);
    ppu_set_fb(&nes.ppu, (struct s_rgb*) image_raw_data(fb), fb_scale);
    ppu_win = window;

    print_word("NMI", NMIVec);
    print_word("RST", ResetVec);
    print_word("IRQ", IRQVec);
    log_debug("The cardridge uses %s mirroring", nes.card.nametable_mirroring == VerticalMirror ? "vertical": "horizontal");
    log_debug("Does the cardridge have battery backed ram?: %s", nes.card.battery_ram ? "yes" : "no");

    btn_init(&btn_tgl_stack, 5, 5, 50, 20, DARK_GREEN, lbl_tgl_stack);
    btn_init(&btn_tgl_pt, 60, 5, 85, 20, DARK_GREEN, lbl_tgl_pt);
    btn_init(&btn_tgl_disassembly, 150, 5, 70, 20, DARK_GREEN, lbl_tgl_disassembly);
    btn_init(&btn_tgl_nt, 225, 5, 60, 20, DARK_GREEN, lbl_tgl_nt);

    struct timeval current;
    gettimeofday(&last_clock, NULL);
    while(!should_stop) {
        gettimeofday(&current, NULL);
        unsigned long long usecs_since_frame = (current.tv_sec - last_clock.tv_sec) * 1000000 + current.tv_usec - last_clock.tv_usec;
        // 60 fps
        if(usecs_since_frame >= 16667 && run_nonstop) {
            render_frame();
            last_clock = current;
        }
        if(!run_nonstop) {
            dj_win_update(window);
        }
    }

    destroy_image(left_table);
    destroy_image(right_table);
    destroy_image(fb);
    destroy_image(nametable_viewer);
	dj_win_close(&window);
#ifdef LOG_INSTRUCTIONS
    fclose(logfile);
#endif

    destroy_cardridge(&(nes.card));
    cleanup_ppu(&(nes.ppu));
    cleanup_apu(&(nes.apu));
    return 0;
}

