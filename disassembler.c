#include "disassembler.h"
#include <string.h>
#include <assert.h>
#include "debug.h"

void init_disasm(struct s_disasm* dis, struct s_nes* nes)
{
    memset(dis->is_opcode, 0, 1 << 16);
    dis->nes = nes;
    assert(nes != NULL);
}

uint16_t first_valid_instruction_after(struct s_disasm* as, uint16_t addr)
{
    while(!as->is_opcode[addr]) addr++;
    return addr;
}

void disasm_update(struct s_disasm* disasm)
{
    disasm->is_opcode[disasm->nes->cpu.current_instruction_pc] = true;
}

uint16_t disasm_instructions(struct s_disasm* as, uint16_t start, uint16_t n, char disassembly[n][24], uint16_t addresses[n])
{
    parse_opcodes(observe_byte, addresses, disassembly, n, start);
    uint16_t ret = -1;

    for(uint16_t i=0; i<n; i++) {
        bool selected = (as->nes->cpu.current_instruction_pc == addresses[i]);
        if(selected) {
            ret = addresses[i];
        }
        if(!strcmp(disassembly[i], "!!! (\?\?\?)")) {
            as->is_opcode[addresses[i]] = true;
        }
    }
    return ret;
}