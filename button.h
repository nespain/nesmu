#ifndef BUTTON_H
#define BUTTON_H

#include <stdbool.h>
#include "color.h"
#include "djwindow.h"

typedef struct s_button {
    int x;
    int y;
    int w;
    int h;
    void (*on_click)(bool new_state);
    bool state;
    const char* title;
    Color color;
} Button;

void btn_init(Button* btn, int x, int y, int w, int h, Color clr, const char* name);
void btn_update(Button* btn, int x, int y);
void btn_drw(DJWindow* window, Button* btn);

#endif