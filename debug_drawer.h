#ifndef DEBUG_DRW_H
#define DEBUG_DRW_H
#include "6502.h"
#include "djwindow.h"
#include "button.h"
#include "controller.h"

void drw_stack(const nmos6502* cpu, DJWindow* window, bool stack_or_zp, Button* btn_tgl_stack);

void drw_controller(NesController* ctr, DJWindow* window, int x, int y, int width);

#endif
