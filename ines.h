#ifndef INES_H
#define INES_H
#include "cardridge.h"

typedef unsigned char byte_t;

typedef struct s_nesheader{
	char title[4];
	byte_t prg_size;
	byte_t chr_size;
	byte_t flags[2];
	byte_t prg_ram_size;
	byte_t tv_system;
	char padding[6];
} NesHeader;

void insert_cardridge(Cardridge* cardridge, const char* filename);

#endif
