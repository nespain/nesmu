#ifndef MEMBUS_H
#define MEMBUS_H

#include "cardridge.h"
#include "controller.h"
#include "6502.h"

struct s_ppu;

typedef struct s_membus {
    Cardridge* card;

    // CPU centered stuff
    uint16_t address_bus;
    uint8_t data_bus;
    // Mirrored 4x from 0x0000 to 0x1FFFF
    byte_t cpu_mem[2048];
    byte_t apu_reg[0x18];
    byte_t ppu_reg[0x8];

    // PPU centered stuff
    uint8_t vdata_bus;
    uint16_t vaddr_bus;
    uint8_t vaddr_buffer;
    byte_t nametables[2][0x400];
    byte_t color_palettes[32];

    byte_t ppu_data_buffer;

    struct s_ppu* ppu;
    struct s_6502_cpu* cpu;
    NesController player1;
    NesController player2;
} Membus;

void init_membus(Membus* membus, struct s_ppu* ppu, struct s_6502_cpu* cpu);
int cycle_bus(Membus* bus, MemoryBusMode* mode);
int cycle_ppu_bus(Membus* bus);
byte_t* current_cpu_byte(Membus* bus, MemoryBusMode mode);
byte_t* cpu_byte(Membus* bus, uint16_t addr, MemoryBusMode mode);
byte_t* ppu_byte(Membus* bus, uint16_t addr, MemoryBusMode mode);

#endif