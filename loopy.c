#include "loopy.h"


void loopy_set_nt(uint16_t* loopy, uint8_t index)
{
    *loopy &= ~(0x3 << 10);
    *loopy |= ((index & 0x3) << 10);
    *loopy &= 0x7FFF;
}
void loopy_set_coarse_x(uint16_t* loopy, uint8_t value)
{
    *loopy &= ~(0x1f);
    *loopy |= (value & 0x1f);
    *loopy &= 0x7FFF;
}
void loopy_set_coarse_y(uint16_t* loopy, uint8_t value)
{
    *loopy &= ~(0x1f << 5);
    *loopy |= ((value & 0x1f) << 5);
    *loopy &= 0x7FFF;
}
void loopy_set_fine_y(uint16_t* loopy, uint8_t value)
{
    *loopy &= ~(0x7 << 12);
    *loopy |= ((value & 0x7) << 12);
    *loopy &= 0x7FFF;
}
void loopy_inc_coarse_x(uint16_t* loopy) {
    uint8_t current_coarse_x = loopy_coarse_x(*loopy);
    if(current_coarse_x == 31) {
        loopy_set_coarse_x(loopy, 0);
        *loopy ^= (1 << 10); // Flip x nametable bit
    } else {
        loopy_set_coarse_x(loopy, current_coarse_x+1);
    }
    // Ensure, that the loopy register is only 15 bits wide
    *loopy &= 0x7FFF;
}
void loopy_inc_fine_y(uint16_t* loopy) {
    uint8_t current_fine_y = loopy_fine_y(*loopy);
    if(current_fine_y < 7) {
        loopy_set_fine_y(loopy, current_fine_y+1);
    } else {
        uint8_t current_coarse_y = loopy_coarse_y(*loopy);
        loopy_set_fine_y(loopy, 0);
        if(current_coarse_y == 29) {
            // We need to wrap around to the other nametable
            loopy_set_coarse_y(loopy, 0);
            *loopy ^= (1 << 11); // flip y nametable bit
        } else if(current_coarse_y == 31) {
            // If we're in attribute memory we just wrap around the current nametable
            loopy_set_coarse_y(loopy, 0);
        } else {
            loopy_set_coarse_y(loopy, current_coarse_y+1);
        }
    }
    *loopy &= 0x7FFF;
}
void loopy_transfer_x(uint16_t* dst, uint16_t src) {
    //loopy_set_coarse_x(dst, loopy_coarse_x(src));
    //loopy_set_nt(dst, loopy_nt_x(src) | (loopy_nt_y(*dst) << 1));
    // 0x41f = 0b10000011111 (masks x nametable and x coarse)
    *dst &= ~(0x41f);
    *dst |= (src & 0x41f);
    *dst &= 0x7FFF;
}
void loopy_transfer_y(uint16_t* dst, uint16_t src) {
    // 0x7be0 = 0b111101111100000 (masks fineY, y nametable and y coarse)
    *dst &= ~(0x7be0);
    *dst |= (src & 0x7be0);

    *dst &= 0x7FFF;
}

uint16_t loopy_tile_addr(uint16_t loopy) {
    return 0x2000 | (loopy & 0x0FFF);
}
uint16_t loopy_attr_addr(uint16_t loopy) {
    return 0x23c0 | (loopy & 0x0c00) | ((loopy >> 4) & 0x38) | ((loopy >> 2) & 0x07);
}
uint8_t loopy_fine_y(uint16_t loopy) {
    return (loopy >> 12) & 0x7;
}
uint8_t loopy_coarse_x(uint16_t loopy) {
    return loopy & 0x001f;
}
uint8_t loopy_coarse_y(uint16_t loopy) {
    return (loopy >> 5) & 0x1f;
}
uint8_t loopy_nt(uint16_t loopy) {
    return (loopy >> 10) & 0x3;
}
uint8_t loopy_nt_x(uint16_t loopy) {
    return loopy_nt(loopy) & 0x1;
}
uint8_t loopy_nt_y(uint16_t loopy) {
    return loopy_nt(loopy) >> 1;
}
