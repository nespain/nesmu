#include "opcodes.h"

const char mnemonics[1 << 8][6] = {
        "BRK", "ORA", "i_JAM", "i_SLO", I_OP, "ORA", "ASL", "i_SLO", "PHP", "ORA", "ASL", "i_ANC", I_OP, "ORA", "ASL", "i_SLO",
        "BPL", "ORA", "i_JAM", "i_SLO", "i_NOP", "ORA", "ASL", "i_SLO", "CLC", "ORA", "i_NOP", "i_SLO", "i_NOP", "ORA", "ASL", "i_SLO",
        "JSR", "AND", "i_JAM", "i_RLA", "BIT", "AND", "ROL", "i_RLA", "PLP", "AND", "ROL", "i_ANC", "BIT", "AND", "ROL", "i_RLA",
        "BMI", "AND", "i_JAM", "i_RLA", "i_NOP", "AND", "ROL", "i_RLA", "SEC", "AND", I_OP, "i_RAL", "i_NOP", "AND", "ROL", "i_RLA",
        "RTI", "EOR", "i_JAM", I_OP, I_OP, "EOR", "LSR", I_OP, "PHA", "EOR", "LSR", "i_ALR", "JMP", "EOR", "LSR", I_OP,
        "BVC", "EOR", "i_JAM", I_OP, "i_NOP", "EOR", "LSR", I_OP, "CLI", "EOR", I_OP, I_OP, "i_NOP", "EOR", "LSR", I_OP,
        "RTS", "ADC", "i_JAM", I_OP, I_OP, "ADC", "ROR", I_OP, "PLA", "ADC", "ROR", "i_ARR", "JMP", "ADC", "ROR", I_OP,
        "BVS", "ADC", "i_JAM", I_OP, "i_NOP", "ADC", "ROR", I_OP, "SEI", "ADC", I_OP, "i_RRA", "i_NOP", "ADC", "ROR", I_OP,
        "NOP", "STA", "i_NOP", I_OP, "STY", "STA", "STX", I_OP, "DEY", "NOP", "TXA", "i_ANE", "STY", "STA", "STX", I_OP,
        "BCC", "STA", "i_JAM", "i_SHA", "STY", "STA", "STX", I_OP, "TYA", "STA", "TXS", "i_TAS", "i_SHY", "STA", "i_SHX", "i_SHA",
        "LDY", "LDA", "LDX", "i_LAX", "LDY", "LDA", "LDX", "i_LAX", "TAY", "LDA", "TAX", I_OP, "LDY", "LDA", "LDX", "i_LAX",
        "BCS", "LDA", "i_JAM", "i_LAX", "LDY", "LDA", "LDX", "i_LAX", "CLV", "LDA", "TSX", "i_LAS", "LDY", "LDA", "LDX", "i_LAX",
        "CPY", "CMP", "i_NOP", "i_DCP", "CPY", "CMP", "DEC", "i_DCP", "INY", "CMP", "DEX", "i_SBX", "CPY", "CMP", "Dec", "i_DCP",
        "BNE", "CMP", "i_JAM", "i_DCP", "i_NOP", "CMP", "DEC", "i_DCP", "CLD", "CMP", "NOP", "i_DCP", "i_NOP", "CMP", "DEC", "i_DCP",
        "CPX", "SBC", "i_NOP", "i_ISC", "CPX", "SBC", "INC", "i_ISC", "INX", "SBC", "NOP", "i_USBC", "CPX", "SBC", "INC", "i_ISC",
        "BEQ", "SBC", "i_JAM", "i_ISC", "i_NOP", "SBC", "INC", "i_ISC", "SED", "SBC", "NOP", "i_ISC", "i_NOP", "SBC", "INC", "i_ISC",
};

const char* addressmode_desc[15] = {
        "NoAddr",
        "Imm",
        "Abs",
        "AbsX",
        "AbsY",
        "Rel",
        "Zpg",
        "ZpgX",
        "ZpgY",
        "Ind",
        "IndX",
        "IndY",
        "Impl",
        "Acc",
        "Error"
};

const enum e_addressmodes address_modes[1 << 8] = {
        Impl, IndX,Impl,IndX,ErrAddr,Zpg,Zpg,Zpg,Impl,Imm,Acc,Imm,ErrAddr,Abs,Abs,Abs,
        Rel, IndY,Impl,IndY,ZpgX,ZpgX,ZpgX,ZpgX,Impl,AbsY,Impl,AbsY,AbsX,AbsX,AbsX,AbsX,
        Abs, IndX,Impl,IndX,Zpg,Zpg,Zpg,Zpg,Impl,Imm,Acc,Imm,Abs,Abs,Abs,Abs,
        Rel, IndY,Impl,IndY,ZpgX,ZpgX,ZpgX,ZpgX,Impl,AbsY,ErrAddr,AbsY,AbsX,AbsX,AbsX,AbsX,
        Impl, IndX,Impl,ErrAddr,ErrAddr,Zpg,Zpg,ErrAddr,Impl,Imm,Acc,Imm,Abs,Abs,Abs,ErrAddr,
        Rel, IndY,Impl,ErrAddr,ZpgX,ZpgX,ZpgX,ErrAddr,Impl,AbsY,ErrAddr,ErrAddr,AbsX,AbsX,AbsX,ErrAddr,
        Impl, IndX,Impl,ErrAddr,ErrAddr,Zpg,Zpg,ErrAddr,Impl,Imm,Acc,Imm,Ind,Abs,Abs,ErrAddr,
        Rel, IndY,Impl,ErrAddr,ZpgX,ZpgX,ZpgX,ErrAddr,Impl,AbsY,ErrAddr,AbsY,AbsX,AbsX,AbsX,ErrAddr,
        Imm, IndX,Imm,ErrAddr,Zpg,Zpg,Zpg,ErrAddr,Impl,Imm,Impl,Imm,Abs,Abs,Abs,ErrAddr,
        Rel, IndY,Impl,IndY,ZpgX,ZpgX,ZpgY,ErrAddr,Impl,AbsY,Impl,AbsY,AbsX,AbsX,AbsY,AbsY,
        Imm, IndX,Imm,IndX,Zpg,Zpg,Zpg,Zpg,Impl,Imm,Impl,ErrAddr,Abs,Abs,Abs,Abs,
        Rel, IndY,Impl,IndY,ZpgX,ZpgX,ZpgY,ZpgY,Impl,AbsY,Impl,AbsY,AbsX,AbsX,AbsY,AbsY,
        Imm, IndX,Imm,IndX,Zpg,Zpg,Zpg,Zpg,Impl,Imm,Impl,Imm,Abs,Abs,Abs,Abs,
        Rel, IndY,Impl,IndY,ZpgX,ZpgX,ZpgX,ZpgX,Impl,AbsY,Impl,AbsY,AbsX,AbsX,AbsX,AbsX,
        Imm, IndX,Imm,IndX,Zpg,Zpg,Zpg,Zpg,Impl,Imm,Impl,Imm,Abs,Abs,Abs,Abs,
        Rel, IndY,Impl,IndY,ZpgX,ZpgX,ZpgX,ZpgX,Impl,AbsY,Impl,AbsY,AbsX,AbsX,AbsX,AbsX,
};
