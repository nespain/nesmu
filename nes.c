#include "nes.h"
#include "logger.h"
#include "debug.h"
#include "mappers/mapper_004.h"

bool is_opcode[1 << 16];

void clock(struct s_nes* nes) {
    if(nes->initial_reset) {
        nes->cpu.reset_line = true;
    }

    if(nes->lockup) return;
    if(nes->cycle_count % 3 == 0) {
        bool inject_reset_now = false;
        if(nes->cpu.instruction_state == R7) {
            inject_reset_now = true;
        }

        if(nes->ppu.dma_active) {
            // TODO: do this timing thing of even and odd clock cycles for beginning
            if(nes->ppu.dma_cpu_cycle % 2 == 0) {
                nes->ppu.dma_data = *cpu_byte(&(nes->bus), nes->ppu.dma_page << 8 | nes->ppu.dma_read_addr, Read);
                nes->ppu.dma_read_addr++;
            } else {
                nes->ppu.OAM.bytes[nes->ppu.dma_write_addr] = nes->ppu.dma_data;
                nes->ppu.dma_write_addr++;
            }
            nes->ppu.dma_cpu_cycle++;

            if(nes->ppu.dma_cpu_cycle == 512) {
                nes->ppu.dma_active = false;
                nes->cpu.rdy_line--; // Unsuspend the cpu
            }
        }

        if(cycle(&(nes->cpu))) {
            log_error("Lockup at pc = %#04x", nes->cpu.current_instruction_pc);
            log_info("Stacktrace: ");
            for(int i=nes->cpu.stacktrace.top; i>= 0; i--) {
                printf("\t%04x\n", nes->cpu.stacktrace.data[i]);
            }
            nes->lockup = true;
        }
        is_opcode[nes->cpu.current_instruction_pc] = true;

        // Inject custom start vector on reset
        if(inject_reset_now && nes->use_custom_reset_vector) {
            nes->cpu.program_counter = nes->custom_reset_vector;
            *(nes->cpu.address_pins) = nes->cpu.program_counter;
        }
        if(cycle_bus(&(nes->bus), &(nes->cpu.bus_mode))) {
            log_error("[%#04x] membus has locked", nes->cpu.current_instruction_pc);
            nes->lockup = true;
        }
#ifdef LOG_INSTRUCTIONS
        FILE* out = logfile;
#else
        FILE* out = stderr;
#endif
        if(log_nes(out, &nes->cpu, &nes->ppu, observe_byte, nes->current_romfile))
            nes->lockup = true;
    }
    cycle_ppu(&(nes->ppu));
    cycle_ppu_bus(&(nes->bus));
    if(nes->cycle_count % 6 == 0) {
        cycle_apu(&(nes->apu));
    }

    /*if(ppu.current_cycle == 260 && ppu.current_scanline < 240) {
        tick_irq_counter(&card);
    }*/
    if(nes->bus.vaddr_bus & (1 << 11))
        tick_irq_counter(&(nes->card));

    nes->cycle_count++;

    if(nes->lockup) {
        log_error("CPU Locked up");
        log_trace("Cpu ran for %llu cycle", nes->cycle_count);
    }
    if(nes->initial_reset) {
        nes->cpu.reset_line = false;
        nes->initial_reset = false;
    }

    if(nes->ppu.frame_finished && (nes->ppu.frame_nr % frame_division == 0)) drw_screen();
}
