#include "6502.h"
#include "logger.h"
#include "djtest.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <dirent.h>
#include <stdlib.h>
#include <cjson/cJSON.h>
#include "tpool.h"

#define cmp_reg(reg, name, clearname) expected = cJSON_GetObjectItemCaseSensitive(j_final, name); \
if(reg != expected->valueint) {                                                                   \
log_trace("Test %s failed", name);                                                                \
log_error(clearname" mismatch. Expected: %d, got %d", expected->valueint, reg); \
return 1; \
}

// This program uses the NES 6502 processor tests from https://github.com/dbaslom/ProcessorTests/tree/8088/nes6502
// It attemps to run them one after another

struct s_membus { ;
    uint8_t memory[1 << 16];
    uint16_t addr;
    uint8_t data;
};

void mem_cycle(struct s_membus* membus, struct s_6502_cpu* cpu) {
    if(cpu->bus_mode == Write) {
        membus->memory[membus->addr] = membus->data;
        //log_debug("Wrote %d to %d", membus.data, membus.addr);
    } else {
        membus->data = membus->memory[membus->addr];
        //log_debug("Read %d from %d", membus.data, membus.addr);
    }
}

int exec_test(cJSON* j_test) {

    struct s_membus membus;
    struct s_6502_cpu cpu;
    init(&cpu, &membus.addr, &membus.data);

    assert(cJSON_IsObject(j_test));
    cJSON* j_name = cJSON_GetObjectItemCaseSensitive(j_test, "name");
    const char* name = j_name->valuestring;

    // Prepare test
    cJSON* j_setup = cJSON_GetObjectItemCaseSensitive(j_test, "initial");
    cpu.program_counter = cJSON_GetObjectItemCaseSensitive(j_setup, "pc")->valueint;
    cpu.accumulator = cJSON_GetObjectItemCaseSensitive(j_setup, "a")->valueint;
    cpu.stack_ptr = cJSON_GetObjectItemCaseSensitive(j_setup, "s")->valueint;
    cpu.reg_x = cJSON_GetObjectItemCaseSensitive(j_setup, "x")->valueint;
    cpu.reg_y = cJSON_GetObjectItemCaseSensitive(j_setup, "y")->valueint;
    cpu.flags = cJSON_GetObjectItemCaseSensitive(j_setup, "p")->valueint;
    cpu.sync = false;
    cpu.nmi_line = 0;
    cpu.irq_line = 0;
    cpu.reset_line = false;
    cpu.rdy_line = 0;


    cJSON* j_ram = cJSON_GetObjectItemCaseSensitive(j_setup, "ram");
    assert(cJSON_IsArray(j_ram));
    for(int i=0; i< cJSON_GetArraySize(j_ram); i++) {
        cJSON* j_entry = cJSON_GetArrayItem(j_ram, i);
        cJSON* j_addr = cJSON_GetArrayItem(j_entry, 0);
        cJSON* j_value = cJSON_GetArrayItem(j_entry, 1);
        membus.memory[j_addr->valueint] = j_value->valueint;
    }

    // This fetch would have happened in the previous instruction
    cpu.instruction_state = T0;
    cpu.bus_mode = Read;
    membus.addr = cpu.program_counter;
    mem_cycle(&membus, &cpu);

    // Execute test
    while(!cpu.sync) {
        if(cycle(&cpu)) {
            log_error("Cpu locked up");
            return 1;
        }
        mem_cycle(&membus, &cpu);
    }

    // Evaluate test
    cJSON* j_final = cJSON_GetObjectItemCaseSensitive(j_test, "final");
    cJSON* expected;
    cmp_reg(cpu.program_counter, "pc", "Program Counter");
    cmp_reg(cpu.accumulator, "a", "Accumulator");
    cmp_reg(cpu.reg_x, "x", "X-Reg");
    cmp_reg(cpu.reg_y, "y", "Y-Reg");
    cmp_reg(cpu.stack_ptr, "s", "Stack Pointer");
    expected = cJSON_GetObjectItemCaseSensitive(j_final, "p");
    if (cpu.flags != expected->valueint) {
        log_trace("Test %s", name);
        log_error("Flags" " mismatch. Expected: %d, got %d", expected->valueint, cpu.flags);
        uint8_t mask = 1;
        const char* flag_names[8] = {"Carry", "Zero", "Interrupt", "Decimal", "Break", "Const", "Overflow", "Negative"};
        for(int i=0; i<8; i++) {
            if((cpu.flags & mask) != (expected->valueint & mask)) {
                log_warn("%s is differing", flag_names[i]);
            }
            mask <<= 1;
        }
        return 1;
    }

    // Evaluate RAM contents
    j_ram = cJSON_GetObjectItemCaseSensitive(j_final, "ram");
    assert(cJSON_IsArray(j_ram));
    for(int i=0; i< cJSON_GetArraySize(j_ram); i++) {
        cJSON* j_entry = cJSON_GetArrayItem(j_ram, i);
        cJSON* j_addr = cJSON_GetArrayItem(j_entry, 0);
        cJSON* j_value = cJSON_GetArrayItem(j_entry, 1);
        if(membus.memory[j_addr->valueint] != j_value->valueint) {
            log_trace("Test %s", name);
            log_error("RAM mismatch at %04x/%d Expected %02x Got: %02x", j_addr->valueint, j_addr->valueint, j_value->valueint, membus.memory[j_addr->valueint]);

            log_info("Expected: ");
            for(int j=0; j< cJSON_GetArraySize(j_ram); j++) {
                cJSON *e = cJSON_GetArrayItem(j_ram, j);
                cJSON *a = cJSON_GetArrayItem(e, 0);
                cJSON *v = cJSON_GetArrayItem(e, 1);
                printf("[%04x] %02x\n", a->valueint, v->valueint);
            }
            printf("\n");
            log_warn("Actual: ");
            for(int j=0; j< cJSON_GetArraySize(j_ram); j++) {
                cJSON *e = cJSON_GetArrayItem(j_ram, j);
                cJSON *a = cJSON_GetArrayItem(e, 0);
                printf("[%04x] %02x\n", a->valueint, membus.memory[a->valueint]);
            }
            return 1;
        }
    }

    return 0;
}

int test(const char* filename, int nr) {

    FILE* f = fopen(filename, "r");
    fseek(f, 0, SEEK_END);
    size_t filesize = ftell(f);
    fseek(f, 0, SEEK_SET);
    char* s = malloc(filesize+1);
    s[filesize] = '\0';
    fread(s, filesize, 1, f);
    fclose(f);

    cJSON* j_root = cJSON_Parse(s);
    assert(!cJSON_IsInvalid(j_root));
    assert(cJSON_IsArray(j_root));

    int num_tests = cJSON_GetArraySize(j_root);
    for(int i=0; i<num_tests; i++) {
        cJSON* t = cJSON_GetArrayItem(j_root, i);
        if(exec_test(t) != 0) {
            log_error("Test %d %s failed", nr, filename);
            cJSON_Delete(j_root);
            free(s);
            return 1;
        }
    }

    cJSON_Delete(j_root);
    free(s);
    return 0;
}

#define TEST_DIR "/home/dbauer/repos/ProcessorTests/nes6502/v1/"

#define blacklist_len 43
const char* blacklist[blacklist_len] = {
        "02.json", // Jam start
        "12.json",
        "22.json",
        "32.json",
        "42.json",
        "52.json",
        "62.json",
        "72.json",
        "92.json",
        "b2.json",
        "d2.json",
        "f2.json", // Jam end
        "03.json", // SLO indX
        "13.json", // SLO indY
        "8b.json", // ANE
        "ab.json", // LXA
        "47.json", // SRE start
        "57.json",
        "4f.json",
        "5f.json",
        "5b.json",
        "43.json",
        "53.json", // SRE end
        "67.json", // RRA start
        "77.json",
        "6f.json",
        "7f.json",
        "7b.json",
        "63.json",
        "73.json", // RRA end
        "9b.json", // TAS
        "c7.json", // DCP start
        "d7.json",
        "cf.json",
        "df.json",
        "db.json",
        "c3.json",
        "d3.json", // DCP end
        "6b.json", // ARR
        "e3.json", // ISC indX
        "f3.json", // ISC indY
        "23.json", // RLA indX
        "33.json", // RLA indY
};

struct s_testdata {
    char* fname;
    int id;
};
void test_worker(void* a) {
    struct s_testdata* attr = a;
    test(attr->fname, attr->id);
    free(attr->fname);
    free(attr);
}

int main(int argc, const char* argv[]) {

    DIR* d = opendir(TEST_DIR);
    //readdir(d);
    //readdir(d);

    int skip_tests = 0;
    if(argc > 1) {
        skip_tests = atoi(argv[1]);
    }
    int test_nr = 0;

    tpool_t* pool = tpool_create(12);

    while(d != NULL) {
        struct dirent* f = readdir(d);
        if(f != NULL) {
            int blacklisted = 0;
            for(int i=0; i<blacklist_len; i++) {
                if(!strcmp(f->d_name, blacklist[i])) blacklisted = 1;
            }
            if(blacklisted) continue;
            test_nr++;
            if(skip_tests > 0) {
                skip_tests--;
                continue;
            }

            struct s_testdata* arg = malloc(sizeof(struct s_testdata));
            char* fname = calloc(256, 1);
            strcat(fname, TEST_DIR);
            strcat(fname, f->d_name);
            arg->fname = fname;
            arg->id = test_nr-1;

            tpool_add_work(pool, test_worker, arg);
        } else {
            closedir(d);
            d = NULL;
        }
    }
    tpool_wait(pool);
    tpool_destroy(pool);

    return 0;
}
