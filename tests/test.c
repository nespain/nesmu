#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>

#include "6502.h"
#include "ines.h"
#include "debug.h"
#include "apu.h"
#include "cardridge.h"
#include "colors.h"
#include "membus.h"

Membus bus;
Cardridge mario;

byte_t read_byte(uint16_t addr) {
    return *cpu_byte(&bus, addr, Read);
}

void dump_startup(int num_instr) {
	char disassembly[num_instr][24];
	uint16_t addresses[num_instr];

    uint16_t pc = read_byte(0xfffc) | (read_byte(0xfffd) << 8);
    printf(BLUE"Beginning at %#04x\n"RESET, pc);

	parse_opcodes(read_byte, addresses, disassembly, num_instr, pc);
	for(int i=0; i<num_instr; i++) {
		printf(GREEN"[%#04x]"RESET" %s\n", addresses[i], disassembly[i]);
	}
}

int main() {

	insert_cardridge(&mario, "mariobros.nes");
    init_membus(&bus);
    bus.card = &mario;

	dump_startup(50);

	nmos6502 cpu;
	nmos6502* c = &cpu;
	init(c, &bus.address_bus, &bus.data_bus);

	APU apu;
	init_apu(&apu, &bus.apu_reg);

	for(int i=0; i<100; i++) {
		int error = cycle(c);
		if(error) {
			fprintf(stderr, YELLOW"An Error occured. Stopping execution\n"RESET);
			break;
		}
        cycle_bus(&bus, cpu.bus_mode);
		cycle_apu(&apu);
        printf("PC: %#04x\nAcc: %#02x\nInstr buf: %#02x\nState: %s\n\n", cpu.program_counter, cpu.accumulator, cpu.instruction_buffer, state_str(cpu.instruction_state));

    }

	cleanup_apu(&apu);
    destroy_cardridge(&mario);

	return 0;
}
