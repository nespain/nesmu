#ifndef NES_H
#define NES_H

#include "membus.h"
#include "ppu.h"
#include "apu.h"
#include "6502.h"

#define frame_division 1

extern bool is_opcode[1 << 16];

struct s_nes {
    Membus bus;
    nes_ppu ppu;
    struct s_apu apu;
    nmos6502 cpu;
    Cardridge  card;
    bool lockup;
    bool initial_reset;
    unsigned long long cycle_count;
	short custom_reset_vector;
	bool use_custom_reset_vector;
	const char* current_romfile;
};

void clock(struct s_nes* nes);
void drw_screen();
byte_t observe_byte(uint16_t addr);

#endif
