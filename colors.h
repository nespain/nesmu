#ifndef COLORS_H
#define COLORS_H

#define TTY_RED "\033[31m"
#define TTY_GREEN "\033[32m"
#define TTY_YELLOW "\033[33m"
#define TTY_BLUE "\033[34m"
#define TTY_PURPLE "\033[35m"
#define TTY_RESET "\033[0m"
#define TTY_BOLD "\033[1m"

#endif