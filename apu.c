#include "apu.h"
#include <assert.h>

#define status(apu) *apu->memory[0x15]
#define ch_enabled_p1(apu) (status(apu) & 0x1)
#define ch_enabled_p2(apu) (status(apu) & 0x2)
#define ch_enabled_trig(apu) (status(apu) & 0x4)
#define ch_enabled_noise(apu) (status(apu) & 0x8)
#define frame_counter_mode(apu) (*apu->memory[0x17] & 0x80)
#define irq_inhibit_flag(apu) (*apu->memory[0x17] & 0x40)
#define ch_lc_halted_p1(apu) (*apu->memory[0x00] & 0x20)

void init_apu(APU *apu, byte_t (*memory)[0x18])
{
	apu->memory = memory;
    apu->audio = audio_open(8000, 1, Unsigned, Bits8, 0);
    apu->step = 0;
}

void cleanup_apu(APU *apu)
{
    audio_close(apu->audio);
}

void envelopes (APU* apu) {
    // TODO: implement
}
void length_counter (APU* apu) {
    // TODO: implement
    // Pulse 1
    if(!ch_lc_halted_p1(apu) && apu->pulse1.length != 0) {
        apu->pulse1.length--;
        if(apu->pulse1.length == 0) {
            // TODO: silence channel
        }
    }
}
void sweep_units(APU* apu) {
    // TODO: implement
}
void irq(APU* apu) {
    if(!irq_inhibit_flag(apu)) return;
    // TODO: implement
}
void linear_counter(APU* apu) {
    // TODO: implement
}

void cycle_apu(APU *apu)
{
    // 5-step sequence
    if(frame_counter_mode(apu)) {
        if(apu->step != 3) {
            envelopes(apu);
            linear_counter(apu);
        }
        if(apu->step % 4 == 1) {
            length_counter(apu);
            sweep_units(apu);
        }
        apu->step++;
        apu->step %= 5;
    } else {
        envelopes(apu);
        linear_counter(apu);
        if(apu->step % 2 == 1) {
            length_counter(apu);
            sweep_units(apu);
        }
        if(apu->step == 3) {
            irq(apu);
        }
        apu->step++;
        apu->step %= 4;
    }
}
