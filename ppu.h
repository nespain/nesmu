#ifndef PPU_H
#define PPU_H

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "drw.h"
#include "membus.h"

typedef enum {PPUCtrl    = 0x2000,
              PPUMask    = 0x2001,
              PPUStatus  = 0x2002,
              PPUOamAddr = 0x2003,
              PPUOamData = 0x2004, PPUScroll,
              PPUAddr    = 0x2006,
              PPUData    = 0x2007,
              PPUDma     = 0x4014} PPURegs;

#define PALETTE_START 0x3F00
#define PALETTE_END 0x3F1F

extern DJWindow* ppu_win;

struct s_oamentry {
    uint8_t y_pos;
    uint8_t tile_id;
    uint8_t attributes;
    uint8_t x_pos;
};
// Returns the actual palette for a given sprite already offset ready to be queried
#define sprite_palette(sprite) ((sprite.attributes & 0x3) + 0x04)
#define sprite_priority(sprite) ((sprite.attributes >> 5) & 0x1)
#define sprite_hor_flip(sprite) ((sprite.attributes >> 6) & 0x1)
#define sprite_vert_flip(sprite) (sprite.attributes >> 7)

typedef struct s_ppu {
    uint16_t* cpu_addr;
    uint8_t* cpu_data;
    uint16_t* ppu_addr;
    uint8_t* ppu_data;

    uint8_t* registers;
    struct s_membus* membus;

    uint16_t current_cycle;
    uint16_t current_scanline;
    uint64_t frame_nr;
    bool frame_finished;

    int* nmi_lane;

    struct s_rgb colors[0x40];

    uint8_t fine_x;
    uint16_t v_reg;
    uint16_t t_reg;
    union u_bg_fetch_buffer {
        struct {
            uint8_t tile_id;
            uint8_t attribute;
            uint8_t pattern_lsb;
            uint8_t pattern_msb;
        };
        uint8_t buffer[4];
    } bg_next;
    union u_bg_shifter {
        struct {
            uint16_t pattern_lo;
            uint16_t pattern_hi;
            uint16_t attrib_lo;
            uint16_t attrib_hi;
        };
        uint16_t buffer[4];
    } bg_shifter;

    union {
        struct s_oamentry entries[64];
        uint8_t bytes[256];
    } OAM;

    struct s_rgb* fb;
    int scale;

    FILE* log_file;
    bool begin_log;
    bool should_log;
    bool address_latch;

    uint16_t dma_cpu_cycle;
    bool dma_active;
    uint8_t dma_data;
    uint8_t dma_page;
    uint8_t dma_write_addr;
    uint8_t dma_read_addr;

    // The secondary OAM, which holds the sprite data for the next scanline
    union { ;
        struct s_oamentry entries[8];
        uint8_t bytes[32];
    } secondary_oam;
    // The current number of sprites loaded up into secondary oam
    uint8_t sprite_count;
    // The current sprite from oam being processed during sprite evaluation
    uint8_t sprite_n;
    uint8_t sprite_shifter_pattern_lo[8];
    uint8_t sprite_shifter_pattern_hi[8];
    uint8_t sprite_evaluation_buffer;
    // Is set to true if sprite 0 is loaded up into secondary oam
    bool can_hit_sprite0;
    enum {ReadY, WriteY, ReadT, WriteT, ReadA, WriteA, ReadX, WriteX} sprite_evaluation_state;
    uint16_t last_t_reg;
} nes_ppu;

#define oamaddr(ppu) ppu->registers[3]

void init_ppu(nes_ppu* ppu, uint8_t registers[0x8], uint16_t* cpu_addr, uint8_t* cpu_data, uint16_t* ppu_addr, uint8_t* ppu_data, int* nmi);
void cleanup_ppu(nes_ppu* ppu);
void cycle_ppu(nes_ppu* ppu);
void load_pal(nes_ppu* ppu, const char* filename);
void ppu_set_fb(nes_ppu* ppu, struct s_rgb* fb, int scale);

/**
 * Sets the address of the ppus address bus to given value
 * @param ppu
 * @param addr to be put on bus
 */
void ppu_set_addr(nes_ppu* ppu, uint16_t addr);

uint8_t vram_increment(nes_ppu* ppu);
uint16_t base_nametable_addr(nes_ppu* ppu);
uint16_t base_attribute_addr(nes_ppu* ppu);
uint16_t base_sprite_addr(nes_ppu* ppu);
uint16_t bg_pattern_table_addr(nes_ppu* ppu);
uint8_t  sprite_height(nes_ppu* ppu);
bool is_nmi_enabled(nes_ppu* ppu);
bool should_render_bg(nes_ppu* ppu);
bool should_render_sprites(nes_ppu* ppu);
bool should_render_left_bg(nes_ppu* ppu);
bool should_render_left_sprites(nes_ppu* ppu);
struct s_rgb get_color(nes_ppu* ppu, byte_t palette, byte_t pixel, MemoryBusMode mode);
#define ppu_log(msg) if(ppu->should_log) fprintf(ppu->log_file, "[%d] %s ", ppu->current_cycle, msg);

void ppu_drw_pixel(nes_ppu* ppu);

/**
 * Performs the ppu bg-fetch cycle 
 * 0-1: Nametable
 * 2-3: Attribute table
 * 4-5: LSB of pattern table
 * 6-7: MSB of pattern table
 * @param cycle in the interval [0;7]
 */
void ppu_memory_access(nes_ppu* ppu, int cycle);

#endif
