#include "debug.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include "logger.h"
#include "colors.h"

#define fatal_error
#define LOGNAME "nestest"
//#define nestest

void hexdump_fmt(byte_t (*observe_func)(uint16_t), uint16_t addr, uint16_t num, uint8_t bytes_per_row)
{
    size_t lines = num / bytes_per_row;
    lines += (num % bytes_per_row) ? 1 : 0;
    for(int i=0; i<lines; i++) {
        printf("[$%04x] ", addr + i * bytes_per_row);
        for(int col=0; col<bytes_per_row && (i * bytes_per_row + col < num); col++) {
            printf("%02x ", observe_func(addr + i * bytes_per_row + col));
        }
        printf("\n");
    }
}
void hexdump(byte_t (*observe_func)(uint16_t), uint16_t addr, uint16_t num)
{
    return hexdump_fmt(observe_func, addr, num, 8);
}

void parse_opcodes(byte_t (*read_byte)(uint16_t addr), uint16_t* addresses, char (*out)[24], uint16_t num_instructions, uint16_t start_addr) {
	uint16_t pc = start_addr;
	for(uint16_t i=0; i<num_instructions; i++) {
		out[i][23] = '\0';
        if(addresses != NULL)
            addresses[i] = pc;
			

		byte_t instr = read_byte(pc);
		const char* mnemonic = mnemonics[instr];
		enum e_addressmodes mode = address_modes[instr];

		if(*mnemonic == '\0') {
			printf("Opcode parser: [%#04x] Don't have mnemonic for %#02x yet\n", pc, read_byte(pc));
			return;
		}
		pc++;
		/*for(int j=0; j<5; j++) {
			out[i][j] = mnemonic[j];
		}*/
		strcpy(out[i], mnemonic);
        char addr_mode_str[16];
		switch(mode) {
            case(Ind): {
                uint16_t addr = read_byte(pc) | (read_byte(pc + 1) << 8);
                sprintf(addr_mode_str, " ($%04x)", addr);
                pc += 2;
            } break;
			case(IndX):
				sprintf(addr_mode_str, " ($%02x,X)", read_byte(pc));
				pc++;
				break;
            case(IndY):
                sprintf(addr_mode_str, " ($%02X),Y", read_byte(pc));
                pc++;
                break;
			case(Imm):
				sprintf(addr_mode_str, " #%02X", read_byte(pc));
				pc++;
				break;
			case(Abs): {
				uint16_t addr = read_byte(pc) | (read_byte(pc+1) << 8);
				sprintf(addr_mode_str, " $%04X", addr);
				pc+=2;
			} break;
            case(AbsX): {
				uint16_t addr = read_byte(pc) | (read_byte(pc+1) << 8);
				sprintf(addr_mode_str, " $%04X,X", addr);
				pc+=2;
			} break;
            case(AbsY): {
				uint16_t addr = read_byte(pc) | (read_byte(pc+1) << 8);
				sprintf(addr_mode_str, " $%04X,Y", addr);
				pc+=2;
			} break;
			case(Impl):
                addr_mode_str[0] = '\0';
				break;
            case(Zpg):
                sprintf(addr_mode_str, " $%02X", read_byte(pc));
                pc++;
                break;
            case(ZpgX):
                sprintf(addr_mode_str, " $%02X,X", read_byte(pc));
                pc++;
                break;
            case(ZpgY):
                sprintf(addr_mode_str, " $%02X,Y", read_byte(pc));
                pc++;
                break;
			case(Rel): {
				uint16_t addr = pc + 1 + (int8_t) read_byte(pc);
				sprintf(addr_mode_str, " $%04X", addr);
				pc++;
			} break;
            case(Acc):
                sprintf(addr_mode_str, " A");
                break;
			case(ErrAddr):
				sprintf(addr_mode_str, " (\?\?\?)");
				break;
			default:
				log_error("Addressing mode %s not yet implemented", addressmode_desc[mode]);
				assert(0);
		}
        strcat(out[i], addr_mode_str);
	}
}

static void set_pixel(char dst[4 * 256 * 256], uint16_t x, uint16_t y, struct s_rgb color) {
    struct s_rgb* disp = (struct s_rgb*) dst;
    disp[256 * 2*y + 2*x] = color;
    disp[256 * 2*y + 2*x+1] = color;
    disp[256 * (2*y+1) + 2*x] = color;
    disp[256 * (2*y+1) + 2*x+1] = color;
}
Color rgb_to_color(struct s_rgb input)
{
    return (Color) {(float) input.r / 255.0, (float) input.g / 255.0, (float) input.b / 255.0};
}

void read_pattern_table(nes_ppu* ppu, struct s_membus* bus, char dst[4 * 256 * 256], int index, byte_t palette) {

    for(uint16_t tileY = 0; tileY < 16; tileY++) {
        for(uint16_t tileX = 0; tileX < 16; tileX++) {
            uint16_t offset = tileY * 256 + tileX * 16;
            for(uint16_t row = 0; row < 8; row++) {

                byte_t tile_lsb = *ppu_byte(bus, index * 0x1000 + offset + row, Observer);
                byte_t tile_msb = *ppu_byte(bus, index * 0x1000 + offset + row + 8, Observer);

                for(uint16_t col = 0; col < 8; col++) {
                    uint8_t pixel = ((tile_msb & 0x80) >> 6) | ((tile_lsb & 0x80) >> 7);
                    tile_lsb <<= 1;
                    tile_msb <<= 1;

                    uint16_t x = tileX * 8 + col;
                    uint16_t y = tileY * 8 + row;
                    struct s_rgb c = get_color(ppu, palette, pixel, Observer);
                    set_pixel(dst, x, y, c);
                }
            }
        }
    }
}

static char* nestestfile = NULL;
static char* nesteststream = NULL;
void free_nestest() {
    free(nestestfile);
}

int compare_nestest(nmos6502* cpu, nes_ppu* ppu)
{
    char* nextline = strchr(nesteststream, '\n');
    if(!nextline) {
        log_info("Nestest if over");
        return 5;
    }
    *nextline = '\0';

    char buf[5];
    buf[4] = '\0';
    memcpy(buf, &nesteststream[0], 4);
    uint16_t pc = strtoul(buf, NULL, 16);

    buf[2] = '\0';
    memcpy(buf, &nesteststream[50], 2);
    byte_t acc = strtoul(buf, NULL, 16);

    memcpy(buf, &nesteststream[55], 2);
    byte_t x = strtoul(buf, NULL, 16);

    memcpy(buf, &nesteststream[60], 2);
    byte_t y = strtoul(buf, NULL, 16);

    memcpy(buf, &nesteststream[65], 2);
    byte_t flags = strtoul(buf, NULL, 16);

    memcpy(buf, &nesteststream[71], 2);
    byte_t sp = strtoul(buf, NULL, 16);

    if(pc != cpu->program_counter) {
        log_error("PC wrong expected [%#04x] got [%#04x]", pc, cpu->program_counter);
        printf(TTY_YELLOW"%s\n"TTY_RESET, nesteststream);
#ifdef fatal_error
        return 1;
#endif
    }
    if(acc != cpu->accumulator) {
        log_error("Acc wrong expected [%#02x] got [%#02x]", acc, cpu->accumulator);
        printf(TTY_YELLOW"%s\n"TTY_RESET, nesteststream);
#ifdef fatal_error
        return 1;
#endif
    }
    if(x != cpu->reg_x) {
        log_error("X wrong expected [%#02x] got [%#02x]", x, cpu->reg_x);
        printf(TTY_YELLOW"%s\n"TTY_RESET, nesteststream);
#ifdef fatal_error
        return 1;
#endif
    }
    if(y != cpu->reg_y) {
        log_error("Y wrong expected [%#02x] got [%#02x]", y, cpu->reg_y);
        printf(TTY_YELLOW"%s\n"TTY_RESET, nesteststream);
#ifdef fatal_error
        return 1;
#endif
    }
    if((flags & ~ConstMask) != (cpu->flags & ~ConstMask)) {
        // TODO: later make the test more strict
        log_error("Flags wrong expected [%#02x] got [%#02x]", flags, cpu->flags);
        printf(TTY_YELLOW"%s\n"TTY_RESET, nesteststream);
        if((flags & NegativeMask) != (cpu->flags & NegativeMask)) log_error("Negative does not match up");
        if((flags & OverflowMask) != (cpu->flags & OverflowMask)) log_error("Overflow does not match up");
        if((flags & ConstMask) != (cpu->flags & ConstMask)) log_warn("Const does not match up");
        if((flags & BreakMask) != (cpu->flags & BreakMask)) log_error("Break does not match up");
        if((flags & DecimalMask) != (cpu->flags & DecimalMask)) log_error("Decimal does not match up");
        if((flags & InterruptMask) != (cpu->flags & InterruptMask)) log_error("Interrupt does not match up");
        if((flags & ZeroMask) != (cpu->flags & ZeroMask)) log_error("Zero does not match up");
        if((flags & CarryMask) != (cpu->flags & CarryMask)) log_error("Carry does not match up");
#ifdef fatal_error
        return 1;
#endif
    }
    if(sp != cpu->stack_ptr) {
        log_error("SP wrong expected [%#02x] got [%#02x]", sp, cpu->stack_ptr);
        printf(TTY_YELLOW"%s\n"TTY_RESET, nesteststream);
#ifdef fatal_error
        return 1;
#endif
    }

    *nextline = '\n';
    nesteststream = nextline + 1;
    return 0;
}


__attribute_maybe_unused__  int generate_debug_line(FILE* stream, nmos6502* cpu, nes_ppu* ppu, byte_t (*memdump_func)(uint16_t addr), const char* romname)
{
    if(nestestfile == NULL) {
        FILE* f = fopen(LOGNAME".log", "r");
        fseek(f, 0, SEEK_END);
        size_t size = ftell(f);
        fseek(f, 0, SEEK_SET);
        nestestfile = malloc(size);
        fread(nestestfile, size, 1, f);
        nesteststream = nestestfile;
        fclose(f);
        atexit(free_nestest);
    }

    char cur_instr[24];
    parse_opcodes(memdump_func, NULL, &cur_instr, 1, cpu->program_counter);
    fprintf(stream, "%04X \t%-12s A:%02X X:%02X Y:%02X P:%02X SP:%02X PPU:  %d,%d CYC:%llu\n",
            cpu->program_counter, cur_instr,
            cpu->accumulator,cpu->reg_x, cpu->reg_y, cpu->flags, cpu->stack_ptr,
            ppu->current_scanline, ppu->current_cycle, cpu->cycle-1);

    if(strcmp(romname, LOGNAME".nes"))
        return 0;
    else
        return compare_nestest(cpu, ppu);
}
int log_nes(FILE* stream, nmos6502* cpu, nes_ppu* ppu, byte_t (*memdump_func)(uint16_t addr), const char* romname)
{
    if(!stream) return 0;
    if(cpu->sync) {
#ifdef nestest
        return generate_debug_line(stream, cpu, ppu, memdump_func, romname);
#else
        return 0;
#endif
    }
    return 0;
}
