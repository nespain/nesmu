#ifndef PPU_RENDERERING_H
#define PPU_RENDERERING_H
#include "ppu.h"

void ppu_prerender_scanline(nes_ppu* ppu);
void ppu_visible_scanline(nes_ppu* ppu);
void ppu_postrender_scanline(nes_ppu* ppu);
void ppu_vblank_scanline(nes_ppu* ppu);

#endif
