#ifndef DISASSEMBLER_H
#define DISASSEMBLER_H
#include "nes.h"

struct s_disasm {
    bool is_opcode[1 << 16];
    struct s_nes* nes;
};

void init_disasm(struct s_disasm* as, struct s_nes* nes);
void disasm_update(struct s_disasm* as);

/**
 * @param as Disassembly project
 * @param start address to be disassembled from
 * @param n number of instructions to be decoded
 * @param buffer of strings to be written to
 * @param addresses of the instructions
 * @return the index of the current instruction begin decoded. -1 if none
 */
uint16_t disasm_instructions(struct s_disasm* as, uint16_t start, uint16_t n, char buffer[n][24], uint16_t addresses[n]);

uint16_t first_valid_instruction_after(struct s_disasm* as, uint16_t addr);


#endif