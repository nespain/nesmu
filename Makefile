SANITIZERS = -fsanitize=address -fsanitize=undefined -fsanitize=pointer-compare -fsanitize=leak -fsanitize=shift -fsanitize=integer-divide-by-zero -fsanitize=vla-bound -fsanitize=null -fsanitize=return -fsanitize=bounds -fsanitize=alignment -fsanitize=object-size -fsanitize=bool -fsanitize=enum

CFLAGS=-std=c99 -Wall -I/home/dbauer/repos/cstdj/include -I.
CFLAGS_DBG=${CFLAGS} -g ${SANITIZERS}
CFLAGS_REL=${CFLAGS} -DNDEBUG -O3

LDFLAGS=-lstdj -L/home/dbauer/repos/cstdj -lasound -lX11 -lGLX
LDFLAGS_DBG=${LDFLAGS} ${SANITIZERS}
LDFLAGS_REL=${LDFLAGS} -s 

OBJS=6502.o ines.o membus.o cardridge.o debug.o ppu.o apu.o controller.o ppu_rendering.o loopy.o button.o opcodes.o nes.o mappers/mapper_001.o mappers/mapper_004.o debug_drawer.o emulator.o disassembler.o
SOURCES:=$(wildcard *.c mappers/*.c)
OBJS:=$(patsubst %.c,%.o, ${SOURCES})
OBJS_REL:=$(patsubst %.c,%.release.o, ${SOURCES})
DEPENDENCIES:=$(patsubst %.c,%.d, ${SOURCES})

all: emu
release: emurelease

emu: ${OBJS}
	${CC} ${LDFLAGS_DBG} ${OBJS} -o emu

-include ${DEPENDENCIES}

emurelease: ${OBJS_REL}
	${CC} ${LDFLAGS_REL} ${OBJS_REL} -o emurelease

.PHONY: test
test: 6502.release.o opcodes.release.o tests/dbalsomTest.release.o
	${CC} ${LDFLAGS_REL} $^ -lcjson -o test

%.release.o: %.c Makefile
	${CC} ${CFLAGS_REL} -MMD -MP $< -c -o $@

%.o: %.c Makefile
	${CC} ${CFLAGS_DBG} -MMD -MP $< -c -o $@


clean:
	rm *.o *.d
