#include "debug_drawer.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "color.h"

#include "nes.h"
#include "config.h"


void drw_stack(const nmos6502* cpu, DJWindow* window, bool stack_or_zp, Button* btn_tgl_stack) {
    if(!btn_tgl_stack->state) return;
    char lbl[50];
    byte_t stack_item = 0x00;
    int x= fb_width * fb_scale + 240;
    int y = 200;
    draw_rect(window, x, y, 680, 680, WHITE);
    print_text(window, x + 10, y + 20, stack_or_zp ? "Zeropage:" : "Stack:", RED);
    static const uint16_t num_cols = 8;
    uint16_t items_per_col = 256 / num_cols;
    for(int i=0; i<num_cols; i++) {
        int ix = x + 20 + 80 * i;
        for(int row=0; row<items_per_col; row++) {
            int iy = y + 40 + row * 20;
            byte_t value = observe_byte((stack_or_zp ? 0x00 : STACK_START) + stack_item);
            sprintf(lbl, "[%#02x] %s%#02x",stack_item, value == 0 ? "0x" : "", value);
            bool highlight = stack_item == cpu->stack_ptr && !stack_or_zp;
            print_text(window, ix, iy, lbl, highlight ? BLUE : BLACK);
            if(stack_item == 0xFF) break;
            stack_item++;
        }
    }
}

void drw_controller(NesController* ctr, DJWindow* window, int x, int y, int width)
{
	// png: 2853:1230
	double aspect_ratio = 0.4311;
	int height = width * aspect_ratio;
	draw_rect(window, x, y, width, height, LIGHT_GRAY);
	{
		double w = 0.9436;
		draw_rect(window, x+width*(1-w)/2, y+height*0.1544, width*w, height*0.774, GRAY);
	}
	{ // Select/Start
	  	int w = 0.2787*width;
		int h = 0.23*height;
		int w2 = w/2;
		int h4 = h/4;
		int xp = x + width*0.3252;
		int yp = y + height*0.4325;
		draw_rect(window, xp, yp, w, h, LIGHT_GRAY);
		draw_rect(window, xp+w2/4, yp+(h-h4)/2, w2/2, h4, ctr->buttons[Select_Btn] ? RED : GRAY);
		draw_rect(window, xp+w2+w2/4, yp+(h-h4)/2, w2/2, h4, ctr->buttons[Start_Btn] ? RED : GRAY);
	}
	{ // A/B btn
		int btn_w = 0.11*width;
		draw_rect(window, x + width*0.6477, y+height*0.5813, btn_w, btn_w, LIGHT_GRAY);
		draw_rect(window, x + width*0.7788, y+height*0.5813, btn_w, btn_w, LIGHT_GRAY);
		int btn_r = 0.4*btn_w;
		draw_circle(window, x+width*0.6477+btn_w/2, y+height*0.5813+btn_w/2, btn_r, ctr->buttons[B_btn] ? RED : DARK_RED);
		draw_circle(window, x+width*0.7788+btn_w/2, y+height*0.5813+btn_w/2, btn_r, ctr->buttons[A_btn] ? RED : DARK_RED);
	}
	{ // D-Pad
	  int w = 0.18*width;
	  int thickness = w/3;
	  int xp = x + 0.076 * width;
	  int yp = y + 0.3658 * height;
	  int border = 0.1*thickness;
	  draw_rect(window, xp, yp+thickness, w, thickness, BLACK);
	  draw_rect(window, xp+thickness, yp, thickness, w, BLACK);
	  // up, down, left, right
	  int offsets[4][2] = {{thickness, 0}, 
		                   {thickness, 2*thickness}, 
						   {0, thickness}, 
						   {2*thickness, thickness}};
	  for(enum e_controllerbuttons btn=Up_Btn; btn<=Right_Btn; btn++) {
		  if(ctr->buttons[btn])
			  draw_rect(window, 
					  xp+border+offsets[btn-Up_Btn][0], yp+border+offsets[btn-Up_Btn][1], 
					  thickness-2*border, thickness-2*border, 
					  LIGHT_GRAY);
	  }
	}
}
