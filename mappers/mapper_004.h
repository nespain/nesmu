#ifndef MAPPER_004_H
#define MAPPER_004_H

#include "../cardridge.h"

uint8_t* mapper_004_chr(Cardridge* card, uint16_t addr);
uint8_t* mapper_004_prg(Cardridge* card, uint16_t addr, MemoryBusMode mode);
int mapper_004_prg_write(Cardridge* card, uint16_t addr, uint8_t data);

// Should be triggerd by a rising edge of PPU addressline 12 after the line has remained low for three falling edges of M2
void tick_irq_counter(Cardridge* card);

#endif