#include "mapper_004.h"
#include <stddef.h>
#include <assert.h>
#include "logger.h"
#include "util.h"

#define bank_select(card) card->registers[0]
#define bank_data(card) card->registers[1]
#define mirroring(card) card->registers[2]
#define prg_ram_protect(card) card->registers[3]
#define irq_latch(card) card->registers[4]
#define irq_reload(card) card->registers[5]
#define irq_disable(card) card->registers[6]
#define irq_enable(card) card->registers[7]
#define prg_bank_mode(card) ((bank_select(card) & 0x40) == 0x40)
#define chr_bank_mode(card) ((bank_select(card) & 0x80) == 0x80)
#define selected_register(card) (bank_select(card) & 0x7)
#define R0(card) card->registers[8]
#define R1(card) card->registers[9]
#define R2(card) card->registers[10]
#define R3(card) card->registers[11]
#define R4(card) card->registers[12]
#define R5(card) card->registers[13]
#define R6(card) card->registers[14]
#define R7(card) card->registers[15]
#define irq_active(card) card->registers[16]
#define irq_counter(card) card->registers[17]

uint8_t* mapper_004_chr(Cardridge* card, uint16_t addr)
{
    uint16_t bank;
    if(0x0000 <= addr && addr <= 0x03FF) {
        if(!chr_bank_mode(card))
            bank = R0(card);
        else
            bank = R2(card) & 0xFE; // Ignore the bottom bit
    } else if(0x0400 <= addr && addr <= 0x07FF) {
        if(!chr_bank_mode(card))
            bank = R0(card) | 0x1; // Set the bottom bit
        else
            bank = R3(card);
    } else if(0x0800 <= addr && addr <= 0x0bff) {
        if(!chr_bank_mode(card))
            bank = R1(card) & 0xFE;
        else
            bank = R4(card);
    } else if(0x0c00 <= addr && addr <= 0x0fff) {
        if(!chr_bank_mode(card))
            bank = R1(card) | 0x1;
        else
            bank = R5(card);
    } else if(0x1000 <= addr && addr <= 0x13FF) {
        if(!chr_bank_mode(card))
            bank = R2(card);
        else
            bank = R0(card) & 0xFE;
    } else if(0x1400 <= addr && addr <= 0x17FF) {
        if(!chr_bank_mode(card))
            bank = R3(card);
        else
            bank = R0(card) | 0x1;
    } else if(0x1800 <= addr && addr <= 0x1bff) {
        if(!chr_bank_mode(card))
            bank = R4(card);
        else
            bank = R1(card) & 0xFE;
    } else if(0x1c00 <= addr && addr <= 0x1fff) {
        if(!chr_bank_mode(card))
            bank = R5(card);
        else
            bank = R1(card) | 0x1;
    } else {
        return NULL;
    }
    assert(bank < MAX(card->num_chr_banks, 1)*8);
    return &card->CHR[bank * 0x400 + (addr & 0x03FF)];
}

uint8_t* mapper_004_prg(Cardridge* card, uint16_t addr, MemoryBusMode mode)
{
    if(0x6000 <= addr && addr <= 0x7fff) return &card->PRG_RAM[addr - 0x6000];
    if(!(0x8000 <= addr && addr <= 0xFFFF)) return NULL;
    if(mode == Write) {
        if (0x8000 <= addr && addr <= 0x9FFF) {
            if (addr % 2 == 0)
                return &bank_select(card);
            else
                return &bank_data(card);
        } else if (0xa000 <= addr && addr <= 0xbfff) {
            if (addr % 2 == 0)
                return &mirroring(card);
            else
                return &prg_ram_protect(card);
        } else if (0xc000 <= addr && addr <= 0xdfff) {
            if (addr % 2 == 0)
                return &irq_latch(card);
            else
                return &irq_reload(card);
        } else if (0xe000 <= addr && addr <= 0xffff) {
            if (addr % 2 == 0)
                return &irq_disable(card);
            else
                return &irq_enable(card);
        }
    }
    if(mode == Write) return NULL;
    uint16_t bank;
    if(0x8000 <= addr && addr <= 0x9fff) {
        if(!prg_bank_mode(card))
            bank = R6(card);
        else
            bank = card->num_prg_banks-2;
    } else if(0xa000 <= addr && addr <= 0xbfff) {
        bank = R7(card);
    } else if(0xc000 <= addr && addr <= 0xdfff) {
        if (!prg_bank_mode(card))
            bank = card->num_prg_banks - 2;
        else
            bank = R6(card);
    } else if(0xe000 <= addr && addr <= 0xffff) {
        bank = card->num_prg_banks - 1;
    } else assert(0);
    return &card->PRG[bank * 0x4000 + (addr & 0x3FFF)];
}

#define is_write_to_bank_select(addr) (0x8000 <= addr && addr <= 0x9FFF && addr % 2 == 0)
#define is_write_to_bank_data(addr) (0x8000 <= addr && addr <= 0x9FFF && addr % 2 == 1)
#define is_write_to_mirroring(addr) (0xa000 <= addr && addr <= 0xbfff && addr % 2 == 0)
#define is_write_enabling_or_disabling_irq(addr) (0xe000 <= addr && addr <= 0xffff)
#define is_write_to_irq_latch(addr) (0xc000 <= addr && addr <= 0xdfff && addr % 2 == 0)
#define is_write_to_irq_reload(addr) (0xc000 <= addr && addr <= 0xdfff && addr % 2 == 1)


int mapper_004_prg_write(Cardridge* card, uint16_t addr, uint8_t data)
{
    if(0x6000 <= addr && addr <= 0x7fff) {
        card->PRG_RAM[addr - 0x6000] = data;
        return 0;
    }

    uint8_t* reg = mapper_004_prg(card, addr, Write);
    if(reg == NULL) return 1;
    *reg = data;


    if(is_write_to_bank_select(addr)) {
        log_trace("Set bank select to %02x", bank_select(card));
    } else if (is_write_to_bank_data(addr)) {
        card->registers[8+selected_register(card)] = data;
        log_debug("Set R%d to %d",selected_register(card), data);
        // TODO: Implement ram protect
        return 0;
    }
    else if (is_write_to_mirroring(addr)) {
        if(data & 0x1)
            card->nametable_mirroring = HorizontalMirror;
        else
            card->nametable_mirroring = VerticalMirror;
        log_debug("Set mirroring to %d", card->nametable_mirroring);
        return 0;
    } else if (is_write_enabling_or_disabling_irq(addr)) {
        irq_active(card) = (addr % 2 == 1);
    } else if (is_write_to_irq_latch(addr)) {
        irq_reload(card) = 1;
    }

    return 0;
}

static void reload_irq(Cardridge* card) {
    irq_counter(card) = irq_latch(card);
}

void tick_irq_counter(Cardridge* card)
{
    if(card == NULL) return;
    if(card->mapper_type != MMC3) return;

    if(irq_counter(card) == 0) {
        if(irq_active(card))
            (*card->irq_line)++;

        reload_irq(card);
    } else if(irq_reload(card)) {
        reload_irq(card);
        irq_reload(card) = 0;
    } else {
        irq_counter(card)--;
    }
}