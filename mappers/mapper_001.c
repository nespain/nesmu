#include "mapper_001.h"
#include <assert.h>

#define prg_rom_mode(card) ((card->registers[1] & 0xc) >> 2)
#define prg_bank(card) (card->registers[4] & 0xF)
#define chr_bank(card, index) (card->registers[2 + index] & 0x1F)
#define write_cycle(card) card->registers[5]
#define shift_register(card) card->registers[0]
#define control_register(card) card->registers[1]

uint8_t* mapper_001_chr(Cardridge* card, uint16_t addr)
{
    if(card->registers[1] & 0x80) {
        // 2x 4kb banks
        if(addr >= 0x0000 && addr <= 0x0fff) {
            return &card->CHR[chr_bank(card, 0) * 0x1000 + addr];
        } else if (addr >= 0x1000 && addr <= 0x1fff) {
            return &card->CHR[chr_bank(card, 1) * 0x1000 + addr - 0x1000];
        }
    } else {
        uint8_t selected_bank = chr_bank(card, 0) >> 1;
        if(selected_bank < card->num_chr_banks) NULL;
        return &card->CHR[selected_bank * 0x2000 + addr];
    }
    return NULL;
}
uint8_t* mapper_001_prg(Cardridge* card, uint16_t addr, MemoryBusMode mode)
{
    if(0x6000 <= addr && addr <= 0x7fff) {
        if (card->battery_ram)
            return &card->battery_ram[addr - 0x6000];
        else
            return &card->PRG_RAM[addr - 0x6000];
    }
    if(addr < 0x8000 || addr > 0xFFFF)
        return NULL;
    if(prg_rom_mode(card) < 2) {
        assert(prg_bank(card) / 2 <= card->num_prg_banks);
        // 32k ROM banks
        return &card->PRG[(prg_bank(card) >> 1) * 0x8000 + addr - 0x8000];
    } else if(prg_rom_mode(card) == 2) {
        uint16_t bank;
        if(addr >= 0x8000 && addr <= 0xbfff)
            bank = 0;
        else
            bank = prg_bank(card);
        assert(bank < card->num_prg_banks);
        return &card->PRG[bank * 0x4000+ (addr & 0x3FFF)];
    } else { // mode = 3
        uint16_t bank;
        if(addr >= 0x8000 && addr <= 0xbfff)
            bank = prg_bank(card);
        else
            bank = (card->num_prg_banks-1);
        assert(bank < card->num_prg_banks);
        return &card->PRG[bank * 0x4000 + (addr & 0x3FFF)];
    }
}
int mapper_001_prg_write(Cardridge* card, uint16_t addr, uint8_t data)
{
    if(0x6000 <= addr && addr <= 0x7fff) {
        if(card->battery_ram) {
            card->battery_ram[addr - 0x6000] = data;
        } else {
            card->PRG_RAM[addr - 0x6000] = data;
        }
        return 0;
    }
    // The sixth register will be used to check in what the current write cycle is rn
    if(addr < 0x8000 || addr > 0xFFFF)
        return 1;
    if(data & (1 << 7)) {
        // Reset load register and control register
        shift_register(card) = 0;
        control_register(card) |= 0x0c;
        write_cycle(card) = 0; // Write cycle 0
        return 0;
    } else {
        shift_register(card) >>= 1;
        shift_register(card) |= (data & 0x1) << 4;
        //shift_register(card) |= ((data & 0x1) << (4 - write_cycle(card)));


        if (write_cycle(card) == 4) {
            byte_t select_register = (addr >> 13) & 0x3; // (0-3)
            card->registers[1 + select_register] = shift_register(card);
            shift_register(card) = 0;

            // Set the mirrormode correctly
            switch(control_register(card) & 0x03) {
                case 0: card->nametable_mirroring = OneScreen_LO; break;
                case 1: card->nametable_mirroring = OneScreen_HI; break;
                case 2: card->nametable_mirroring = VerticalMirror; break;
                case 3: card->nametable_mirroring = HorizontalMirror; break;
                default: assert(0);
            }

            write_cycle(card) = 0;
        } else {
            write_cycle(card)++;
        }
    }
    return 0;
}