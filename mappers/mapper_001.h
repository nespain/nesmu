#ifndef MAPPER_001_H
#define MAPPER_001_H

#include "../cardridge.h"

uint8_t* mapper_001_chr(Cardridge* card, uint16_t addr);
uint8_t* mapper_001_prg(Cardridge* card, uint16_t addr, MemoryBusMode mode);
int mapper_001_prg_write(Cardridge* card, uint16_t addr, uint8_t data);

#endif