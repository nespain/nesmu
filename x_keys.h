#ifndef X_KEYS_H
#define X_KEYS_H

#define K_ESC 9
#define K_SPACE 65
#define K_BACKSPACE 22
#define K_RETURN 36
#define K_BACKSLASH 51
#define K_1 10
#define K_2 (K_1 + 1)
#define K_3 (K_2 + 1)
#define K_4 (K_3 + 1)
#define K_5 (K_4 + 1)
#define K_6 (K_5 + 1)
#define K_7 (K_6 + 1)
#define K_8 (K_7 + 1)
#define K_9 (K_8 + 1)
#define K_0 (K_9 + 1)

#define K_W 25
#define K_I 31
#define K_O 32
#define K_P 33
#define K_A 38
#define K_S 39
#define K_D 40
#define K_B 56

#endif