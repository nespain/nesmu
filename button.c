#include "button.h"
#include "drw.h"
#include <stddef.h>

#define toggle(x) x = !x

void btn_init(Button* btn, int x, int y, int w, int h,  Color clr, const char* title) {
    btn->color = clr;
    btn->title = title;
    btn->x = x;
    btn->y = y;
    btn->w = w;
    btn->h = h;
    btn->on_click = NULL;
}

void btn_update(Button* btn, int x, int y) {
    if(x > btn->x && x < btn->x+btn->w &&
       y > btn->y && y < btn->y+btn->h) {

        toggle(btn->state);

        if(btn->on_click != NULL) btn->on_click(btn->state);
    }
}

void btn_drw(DJWindow* window, Button* btn) {
    draw_rect(window, btn->x, btn->y, btn->w, btn->h, btn->color);
    print_text(window, btn->x, btn->y+15, btn->title, WHITE);
}