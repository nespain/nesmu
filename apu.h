#ifndef APU_H
#define APU_H

#include "6502.h"
#include "audio.h"

struct s_pulse_ch {
    uint8_t length;
    uint8_t volume;
};
struct s_trig_ch {
    uint8_t length;
};
struct s_noise_ch {
    uint8_t length;
};

typedef struct s_apu {
	byte_t (*memory)[0x18];
    AudioDevice* audio;
    byte_t step;

    struct s_pulse_ch pulse1;
    struct s_pulse_ch pulse2;
    struct s_trig_ch trig;
    struct s_noise_ch noise;
} APU;

void init_apu(APU* apu, byte_t (*memory)[0x18]);
void cycle_apu(APU* apu);
void cleanup_apu(APU* apu);

#endif
