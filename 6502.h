#ifndef NMOS6502_H
#define NMOS6502_H
#include <stdint.h>
#include <stdbool.h>
#include "opcodes.h"


typedef enum {
	CarryMask = 0x01, ZeroMask = 0x02, 
	InterruptMask = 0x04, DecimalMask = 0x08,
	BreakMask = 0x10, ConstMask = 0x20,
	OverflowMask = 0x40, NegativeMask = 0x80
} FlagMask;
typedef enum {T0, T1, T2, T3, T4, T5, T6,
R0 = 128, R1, R2, R3, R4, R5, R6, R7,
I0 = 256, I1, I2, I3, I4, I5, I6,
JAM = 42069} InstructionState;
typedef enum e_busmode {Write, Read, Observer} MemoryBusMode;
typedef uint8_t byte_t;

#define STACK_START 0x100
typedef enum {
    NMIVec = 0xFFFA, ResetVec = 0xFFFC, IRQVec = 0xFFFE
} Vectors;

struct s_stack {
	uint16_t data[256];
	uint8_t top;
};

typedef struct s_6502_cpu {
	/* registers */
	uint16_t program_counter;
	byte_t accumulator;
	byte_t flags;
	byte_t stack_ptr;
	byte_t reg_x;
	byte_t reg_y;

	// Is active high
	bool reset_line;
    int nmi_line;
    int irq_line;
    // If it is other than 0 the cpu is not executing a step
    int rdy_line;

	// Can be used genericly during instruction execution
	uint16_t buffer;
    uint16_t buffer2; // mainly used for indirect addressing
    uint8_t smallbuffer;
    bool overflow_occured;
    bool nmi_occured;
    bool irq_occured;
    bool sync;

	byte_t* data_pins;
	uint16_t* address_pins;
	// The currently loaded and decoded instruction
	Opcode instruction_buffer;
	InstructionState instruction_state;
	// whether cpu memory should read or write in this current cycle
	MemoryBusMode bus_mode;
	// The begin of the instruction currently being processed
    uint16_t current_instruction_pc;
	unsigned long long cycle;
	struct s_stack stacktrace;
} nmos6502;

void init(nmos6502* cpu, uint16_t* addr_bus, uint8_t* data_bus);
/**
 * Performs exactly one cycle of the cpu.
 * This does not equal one instruction processed
 */
int cycle(nmos6502* cpu);

void set_flag(nmos6502* cpu, byte_t mask, bool state);
#define set_carry(cpu, state) set_flag(cpu, CarryMask, state)
#define set_zero(cpu, state) set_flag(cpu, ZeroMask, state)
#define set_interrupt(cpu, state) set_flag(cpu, InterruptMask, state)
#define set_decimal(cpu, state) set_flag(cpu, DecimalMask, state)
#define set_break(cpu, state) set_flag(cpu, BreakMask, state)
#define set_const(cpu, state) set_flag(cpu, ConstMask, state)
#define set_overflow(cpu, state) set_flag(cpu, OverflowMask, state)
#define set_negative(cpu, state) set_flag(cpu, NegativeMask, state)
#define flag_set(cpu, mask) (((cpu)->flags & mask) == mask)

#endif
