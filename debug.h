#ifndef DEBUG_H
#define DEBUG_H

#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include "6502.h"
#include "ppu.h"
#include "membus.h"

#define instr_addr_string(instr) addressmode_desc[address_modes[instr]]

void hexdump_fmt(byte_t (*observe_func)(uint16_t), uint16_t addr, uint16_t num, uint8_t bytes_per_row);
void hexdump(byte_t (*observe_func)(uint16_t), uint16_t addr, uint16_t num);

const char* state_str(int s);
Color rgb_to_color(struct s_rgb input);

void parse_opcodes(byte_t (*read_byte)(uint16_t addr), uint16_t* addresses, char (*out)[24], uint16_t num_instructions, uint16_t start_addr);

void read_pattern_table(nes_ppu* ppu, struct s_membus* bus,  char dst[4 * 256 * 256], int index, byte_t palette);

int log_nes(FILE* outstream, nmos6502* cpu, nes_ppu* ppu, byte_t (*memdump_func)(uint16_t addr), const char* romname);
#endif
