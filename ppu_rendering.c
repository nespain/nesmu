#include "ppu_rendering.h"
#include "loopy.h"
#include <assert.h>
#include <string.h>

static void ppu_sprite_evaluation(nes_ppu* ppu);
static void ppu_memaccess_scanline(nes_ppu* ppu);
static void load_bg_shifter(nes_ppu* ppu);
static void update_shifter(nes_ppu* ppu);

void ppu_prerender_scanline(nes_ppu* ppu)
{
	if (ppu->current_cycle == 1) {
		// Clears Sprite overflow, Sprite 0 hit, Vblank in ppustatus $2002
		ppu->registers[PPUStatus - 0x2000] &= 0x1F;
		ppu_log("clr STATUS");
	}
    ppu_memaccess_scanline(ppu);
    if(ppu->current_cycle >= 280 && ppu->current_cycle < 305 && (should_render_bg(ppu) || should_render_sprites(ppu))) {
        loopy_transfer_y(&ppu->v_reg, ppu->t_reg);
    }
}

static void ppu_memaccess_scanline(nes_ppu* ppu)
{
    if(!should_render_bg(ppu)) return;
    // TODO: include sprites as well
    if((ppu->current_cycle >= 2 && ppu->current_cycle < 258) || (ppu->current_cycle >= 321 && ppu->current_cycle < 338)) {
        update_shifter(ppu);
        switch((ppu->current_cycle - 1) % 8) {
            case 0:
                load_bg_shifter(ppu);
                ppu_log("NT");
                ppu_set_addr(ppu, loopy_tile_addr(ppu->v_reg));
                break;
            case 1: ppu->bg_next.tile_id = *ppu->ppu_data; break;
            case 2:
                ppu_log("AT");
                ppu_set_addr(ppu, loopy_attr_addr(ppu->v_reg));
                break;
            case 3:
                ppu->bg_next.attribute = *ppu->ppu_data;
                // We only store the palette information for our concrete tile in the bottom 2 bits
                if(loopy_coarse_y(ppu->v_reg) & 0x02) ppu->bg_next.attribute >>= 4;
                if(loopy_coarse_x(ppu->v_reg) & 0x02) ppu->bg_next.attribute >>= 2;
                ppu->bg_next.attribute &= 0x03;
                break;
            case 4:
                ppu_log("BG_ls");
                ppu_set_addr(ppu, bg_pattern_table_addr(ppu) + ppu->bg_next.tile_id * 16 + loopy_fine_y(ppu->v_reg));
                break;
            case 5: ppu->bg_next.pattern_lsb = *ppu->ppu_data; break;
            case 6:
                ppu_log("BG_ms");
                ppu_set_addr(ppu, bg_pattern_table_addr(ppu) + ppu->bg_next.tile_id * 16 + loopy_fine_y(ppu->v_reg) + 8);
                break;
            case 7:
                ppu->bg_next.pattern_msb = *ppu->ppu_data;
                if(should_render_bg(ppu) || should_render_sprites(ppu)) {
                    loopy_inc_coarse_x(&ppu->v_reg);
                }
                break;
        }
    }
	if(ppu->current_cycle == 256) {
        if(should_render_bg(ppu) || should_render_sprites(ppu)) {
            loopy_inc_fine_y(&ppu->v_reg);
        }
	}
	if(ppu->current_cycle == 257) {
		load_bg_shifter(ppu);
		loopy_transfer_x(&ppu->v_reg, ppu->t_reg);
	}
    if(ppu->current_cycle >= 257 && ppu->current_cycle <= 320) {
        oamaddr(ppu) = 0x00;
    }

	if(ppu->current_cycle == 338 || ppu->current_cycle == 340) {
		ppu_set_addr(ppu, loopy_tile_addr(ppu->v_reg));
	}
	if(ppu->current_cycle == 339 || ppu->current_cycle == 341) {
		ppu->bg_next.tile_id = *ppu->ppu_data;
	}
}

void ppu_visible_scanline(nes_ppu* ppu)
{
    ppu_memaccess_scanline(ppu);

    if(should_render_bg(ppu) && 1 <= ppu->current_cycle && ppu->current_cycle <= 256) {
        ppu_drw_pixel(ppu);
    }
    if((should_render_sprites(ppu) || should_render_bg(ppu)) && (ppu->current_cycle >= 1 && ppu->current_cycle <= 340)) {
        ppu_sprite_evaluation(ppu);
    }
}

void ppu_postrender_scanline(nes_ppu* ppu)
{
    ppu->last_t_reg = ppu->t_reg;
}

void ppu_vblank_scanline(nes_ppu* ppu)
{
	if(ppu->current_scanline == 241 && ppu->current_cycle == 1) {
		if(is_nmi_enabled(ppu))
			(*ppu->nmi_lane)++;
		ppu->registers[PPUStatus - 0x2000] |= (1 << 7);
		ppu_log("set Vblank");
	}
}

static void load_bg_shifter(nes_ppu* ppu) {
    ppu_log("Shft");
    ppu->bg_shifter.pattern_lo = (ppu->bg_shifter.pattern_lo & 0xFF00) | ppu->bg_next.pattern_lsb;
    ppu->bg_shifter.pattern_hi = (ppu->bg_shifter.pattern_hi & 0xFF00) | ppu->bg_next.pattern_msb;
    ppu->bg_shifter.attrib_lo = (ppu->bg_shifter.attrib_lo & 0xFF00) | ((ppu->bg_next.attribute & 0x1) ? 0xFF : 0x00);
    ppu->bg_shifter.attrib_hi = (ppu->bg_shifter.attrib_hi & 0xFF00) | ((ppu->bg_next.attribute & 0x2) ? 0xFF : 0x00);
}
static void update_shifter(nes_ppu* ppu) {
    if(should_render_bg(ppu)) {
        ppu->bg_shifter.pattern_lo <<= 1;
        ppu->bg_shifter.pattern_hi <<= 1;
        ppu->bg_shifter.attrib_lo <<= 1;
        ppu->bg_shifter.attrib_hi <<= 1;
    }
    if(should_render_sprites(ppu) && ppu->current_cycle >= 1 && ppu->current_cycle <= 257) {
        for(int i=0; i<ppu->sprite_count; i++) {
            if(ppu->secondary_oam.entries[i].x_pos > 0) {
                ppu->secondary_oam.entries[i].x_pos--;
            } else {
                ppu->sprite_shifter_pattern_lo[i] <<= 1;
                ppu->sprite_shifter_pattern_hi[i] <<= 1;
            }
        }
    }
}

static uint8_t swap_byte(uint8_t b) {
    return ((b & 0x01) << 7) | ((b & 0x02) << 5) | ((b & 0x04) << 3) | ((b & 0x08) << 1)
         | ((b & 0x10) >> 1) | ((b & 0x20) >> 3) | ((b & 0x40) >> 5) | ((b & 0x80) >> 7);
}

void ppu_sprite_evaluation(nes_ppu* ppu) {
    assert(ppu->current_scanline >= 0 && ppu->current_scanline <= 239);
    assert(ppu->current_cycle >= 1 && ppu->current_cycle <= 340);

    // First a simple way and then after that we can try to create a cycle-accurate sprite evaluation

    if(ppu->current_cycle == 257) {
        memset(ppu->secondary_oam.bytes, 0xFF, 32);
        ppu->sprite_count = 0;

        ppu->sprite_n = 0;
        ppu->can_hit_sprite0 = false;
        while(ppu->sprite_n < 64 && ppu->sprite_count < 9) {
             int16_t diff = ((int16_t)ppu->current_scanline - (int16_t)ppu->OAM.entries[ppu->sprite_n].y_pos);
             if(diff >= 0 && diff < sprite_height(ppu)) {
                if(ppu->sprite_n == 0)
                    ppu->can_hit_sprite0 = true;
                 if(ppu->sprite_count < 8) {
                     ppu->secondary_oam.entries[ppu->sprite_count] = ppu->OAM.entries[ppu->sprite_n];
                     ppu->sprite_count++;
                 }
             }
             ppu->sprite_n++;
        }
        // Set actually correct implementation of sprite overflow
        ppu->registers[2] &= ~(1 << 5);
        ppu->registers[2] |= (ppu->sprite_count > 8) << 5;
    }

    if(ppu->current_cycle == 340) {
        for(uint8_t i=0; i<ppu->sprite_count; i++) {
            struct s_oamentry sprite = ppu->secondary_oam.entries[i];

            uint8_t sprite_pattern_lo, sprite_pattern_hi;
            uint16_t sprite_pattern_addr;
            if(sprite_height(ppu) == 8) {
                if(!sprite_vert_flip(sprite)) {
                    // Normal orientation
                    sprite_pattern_addr = base_sprite_addr(ppu)
                            | (sprite.tile_id << 4)
                            | (ppu->current_scanline - sprite.y_pos);
                } else {
                    sprite_pattern_addr = base_sprite_addr(ppu)
                                             | (sprite.tile_id << 4)
                                             | (7 - (ppu->current_scanline - sprite.y_pos));
                }
            } else {
                // 8x16 mode
                if(!sprite_vert_flip(ppu->secondary_oam.entries[i])) {
                    // Normal orientation
                    if((ppu->current_scanline - sprite.y_pos) < 8) {
                        // Top half
                        sprite_pattern_addr = ((sprite.tile_id & 0x01) << 12)
                                               | ((sprite.tile_id & 0xFE) << 4)
                                               | ((ppu->current_scanline - sprite.y_pos) & 0x7);
                    } else {
                        // Bottom half
                        sprite_pattern_addr = ((sprite.tile_id & 0x01) << 12)
                                                 | (((sprite.tile_id & 0xFE) + 1) << 4)
                                                 | ((ppu->current_scanline - sprite.y_pos) & 0x7);
                    }
                } else {
                    if((ppu->current_scanline - sprite.y_pos) < 8) {
                        // Top half
                        sprite_pattern_addr = ((sprite.tile_id & 0x01) << 12)
                                                 | ((sprite.tile_id & 0xFE) << 4)
                                                 | (7 - ((ppu->current_scanline - sprite.y_pos) & 0x7));
                    } else {
                        // Bottom half
                        sprite_pattern_addr = ((sprite.tile_id & 0x01) << 12)
                                                 | (((sprite.tile_id & 0xFE) + 1) << 4)
                                                 | (7 - ((ppu->current_scanline - sprite.y_pos) & 0x7));
                    }
                }
            }
            sprite_pattern_lo = *ppu_byte(ppu->membus, sprite_pattern_addr, Read);
            sprite_pattern_hi = *ppu_byte(ppu->membus, sprite_pattern_addr+8, Read);

            if(sprite_hor_flip(sprite)) {
                sprite_pattern_lo = swap_byte(sprite_pattern_lo);
                sprite_pattern_hi = swap_byte(sprite_pattern_hi);
            }

            ppu->sprite_shifter_pattern_lo[i] = sprite_pattern_lo;
            ppu->sprite_shifter_pattern_hi[i] = sprite_pattern_hi;
        }
    }

    return;

    if(ppu->current_cycle >= 1 && ppu->current_cycle <= 64) {
        if(ppu->current_cycle % 2 == 1) {
            // Read cycle
        } else {
            ppu->secondary_oam.bytes[(ppu->current_cycle / 2) - 1] = 0xFF;
        }
        ppu->sprite_count = 0;
        ppu->sprite_n = 0;
        ppu->sprite_evaluation_state = ReadY;
        ppu->can_hit_sprite0 = false;
    } else if(ppu->current_cycle >= 65 && ppu->current_cycle <= 256) {
        if(ppu->sprite_n != 64) {
            // Sprite evaluation
            switch(ppu->sprite_evaluation_state) {
                case ReadY:
                    ppu->sprite_evaluation_buffer = ppu->OAM.entries[ppu->sprite_n].y_pos;
                    int16_t diff = ((int16_t)ppu->current_scanline - (int16_t)ppu->OAM.entries[ppu->sprite_n].y_pos);
                    bool in_range = diff >= 0 && diff < sprite_height(ppu);
                    if (in_range) {
                        if(ppu->sprite_n == 0) {
                            ppu->can_hit_sprite0 = true;
                        }
                        ppu->sprite_evaluation_state++;
                    } else {
                        ppu->sprite_n++;
                    }
                    break;
                case WriteY:
                    ppu->secondary_oam.entries[ppu->sprite_count].y_pos
                            = ppu->sprite_evaluation_buffer;
                    ppu->sprite_evaluation_state++;
                    break;
                case ReadT:
                    ppu->sprite_evaluation_buffer = ppu->OAM.entries[ppu->sprite_n].tile_id;
                    ppu->sprite_evaluation_state++;
                    break;
                case WriteT:
                    ppu->secondary_oam.entries[ppu->sprite_count].tile_id
                            = ppu->sprite_evaluation_buffer;
                    ppu->sprite_evaluation_state++;
                    break;
                case ReadA:
                    ppu->sprite_evaluation_buffer = ppu->OAM.entries[ppu->sprite_n].attributes;
                    ppu->sprite_evaluation_state++;
                    break;
                case WriteA:
                    ppu->secondary_oam.entries[ppu->sprite_count].attributes
                            = ppu->sprite_evaluation_buffer;
                    ppu->sprite_evaluation_state++;
                    break;
                case ReadX:
                    ppu->sprite_evaluation_buffer = ppu->OAM.entries[ppu->sprite_n].x_pos;
                    ppu->sprite_evaluation_state++;
                    break;
                case WriteX:
                    ppu->secondary_oam.entries[ppu->sprite_count].x_pos
                            = ppu->sprite_evaluation_buffer;
                    ppu->sprite_count++;
                    ppu->sprite_n++;
                    ppu->sprite_evaluation_state = ReadY;
                    break;
                default: assert(0);
            }
        }
    } else if(ppu->current_cycle >= 257 && ppu->current_cycle <= 320) {
        // Sprite fetches
    } else if(ppu->current_cycle >= 321 && ppu->current_cycle <= 340) {
        // Background render pipine initialization
    }
}