#include "cardridge.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "logger.h"

#include "mappers/mapper_004.h"
#include "mappers/mapper_001.h"

#define INVALID_DEFAULT default: \
log_error("Mapper %d not supported yet", cardridge->mapper_type); \
destroy_cardridge(cardridge);                                  \
exit(3);

void destroy_cardridge(Cardridge* cardridge)
{
    if(cardridge->PRG != NULL) free(cardridge->PRG);
    if(cardridge->CHR != NULL) free(cardridge->CHR);
	if(cardridge->PRG_RAM != NULL) free(cardridge->PRG_RAM);

	// If the cardridge has battery backed ram we write it to a file
    if(cardridge->battery_ram) {
        char save_filename[strlen(cardridge->filename) + 10];
        strcpy(save_filename, cardridge->filename);
        strcat(save_filename, ".save");
        log_debug("Wrote to savefile %s", save_filename);
        FILE *f = fopen(save_filename, "w");
        fwrite(cardridge->battery_ram, 0x2000, 1, f);
        fclose(f);
        free(cardridge->battery_ram);
    }
    free(cardridge->filename);
}

static byte_t dummychr;

byte_t* cardridge_chr(Cardridge* cardridge, uint16_t addr)
{
    assert(cardridge->CHR != NULL);
    switch(cardridge->mapper_type) {
        case(NROM_mapper):
            if(addr > 0x2000) return NULL;
            return &cardridge->CHR[addr];
        case(MMC1):
            return mapper_001_chr(cardridge, addr);
        case(uxROM):
            if(addr > 0x2000) return NULL;
            return &cardridge->CHR[addr];
        case(CNROM):
            if(addr > 0x2000) return NULL;
            if(cardridge->registers[0] >= cardridge->num_chr_banks) return &dummychr;
            return &cardridge->CHR[cardridge->registers[0] * 0x2000 + addr];
        case(MMC3):
            return mapper_004_chr(cardridge, addr);
        case(AxROM):
            if(addr > 0x2000) return NULL;
            return &cardridge->CHR[addr];
        INVALID_DEFAULT
    }
}

byte_t* cardridge_cpu_bus(Cardridge* cardridge, uint16_t addr, MemoryBusMode mode)
{
    if(addr < 0x4020) return NULL;
    switch(cardridge->mapper_type) {
        case(NROM_mapper): {
            if (0x6000 <= addr && addr <= 0x7fff) return &cardridge->PRG_RAM[addr - 0x6000];
            if (addr < 0x8000 || addr > 0xffff) return NULL;
            if (cardridge->num_prg_banks == 1) {
                return &cardridge->PRG[(addr & ~0x4000) - 0x8000];
            } else {
                return &cardridge->PRG[addr - 0x8000];
            }
        } break;
        case(CNROM): {
            if (0x6000 <= addr && addr <= 0x7fff) return &cardridge->PRG_RAM[addr - 0x6000];
            if (mode == Write) return &cardridge->registers[0];
            if (addr < 0x8000 || addr > 0xffff) return NULL;
            if (cardridge->num_prg_banks == 1) {
                return &cardridge->PRG[(addr & ~0x4000) - 0x8000];
            } else {
                return &cardridge->PRG[addr - 0x8000];
            }
        } break;
        case(uxROM): {
            if (addr < 0x8000 || addr > 0xFFFF) return NULL;
            if (mode == Write) return &cardridge->registers[0];
            if (addr >= 0xc000)
                return &cardridge->PRG[0x4000 * (cardridge->num_prg_banks - 1) + addr - 0xc000];
            else {
                return &cardridge->PRG[0x4000 * (cardridge->registers[0] & 0xF) + addr - 0x8000];
            }
        } break;
        case AxROM: {
            if(addr < 0x8000 || addr > 0xFFFF) return NULL;
            if (mode == Write) return &cardridge->registers[0];
            return &cardridge->PRG[0x4000 * (cardridge->registers[0] & 0x7) + addr - 0x8000];
        } break;
        case MMC1 :
            return mapper_001_prg(cardridge, addr, mode);
        case MMC3:
            return mapper_004_prg(cardridge, addr, mode);
        INVALID_DEFAULT
    }
    return &cardridge->PRG[addr - 0x4020];
}

int cardridge_write(Cardridge* cardridge, uint16_t addr, byte_t data) {
    switch(cardridge->mapper_type) {
        case(NROM_mapper):
			if(0x6000 <= addr && addr <= 0x7fff) {
				cardridge->PRG_RAM[addr - 0x6000] = data; 
				return false;
			}
			return true;
        case(uxROM):
            if(0x8000 <= addr && addr <= 0xFFFF) {
                cardridge->registers[0] = data;
                return 0;
            } else return 1;
        case(AxROM):
            if(0x8000 <= addr && addr <= 0xFFFF) {
                cardridge->registers[0] = data;
                return 0;
            } else return 1;
        case(CNROM):
            if(0x8000 <= addr && addr <= 0xFFFF) {
                cardridge->registers[0] = data;
                return 0;
            } else return 1;
        case(MMC1):
            return mapper_001_prg_write(cardridge, addr, data);
        case MMC3: return mapper_004_prg_write(cardridge, addr, data);
        INVALID_DEFAULT
    }
    return 0;
}
