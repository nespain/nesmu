#include "ppu.h"
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include "logger.h"
#include "ppu_rendering.h"

static void load_default_pal(nes_ppu* ppu) {
    uint32_t PALETTE[64] = {
            0x7C7C7C, 0x0000FC, 0x0000BC, 0x4428BC, 0x940084, 0xA80020, 0xA81000, 0x881400, 0x503000, 0x007800, 0x006800, 0x005800, 0x004058, 0x000000, 0x000000, 0x000000,
            0xBCBCBC, 0x0078F8, 0x0058F8, 0x6844FC, 0xD800CC, 0xE40058, 0xF83800, 0xE45C10, 0xAC7C00, 0x00B800, 0x00A800, 0x00A844, 0x008888, 0x000000, 0x000000, 0x000000,
            0xF8F8F8, 0x3CBCFC, 0x6888FC, 0x9878F8, 0xF878F8, 0xF85898, 0xF87858, 0xFCA044, 0xF8B800, 0xB8F818, 0x58D854, 0x58F898, 0x00E8D8, 0x787878, 0x000000, 0x000000,
            0xFCFCFC, 0xA4E4FC, 0xB8B8F8, 0xD8B8F8, 0xF8B8F8, 0xF8A4C0, 0xF0D0B0, 0xFCE0A8, 0xF8D878, 0xD8F878, 0xB8F8B8, 0xB8F8D8, 0x00FCFC, 0xF8D8F8, 0x000000, 0x000000
    };
    for(int i=0; i<64; i++) {
        ppu->colors[i].r = PALETTE[i] >> 24;
        ppu->colors[i].g = (PALETTE[i] >> 16) & 0xFF;
        ppu->colors[i].b = (PALETTE[i] >> 8) & 0xFF;
    }
}

void init_ppu(nes_ppu* ppu, uint8_t registers[0x8], uint16_t* cpu_addr, uint8_t* cpu_data, uint16_t* ppu_addr, uint8_t* ppu_data, int* nmi)
{
    ppu->cpu_addr = cpu_addr;
    ppu->cpu_data = cpu_data;
    ppu->ppu_addr = ppu_addr;
    ppu->ppu_data = ppu_data;

    ppu->registers = registers;
    ppu->nmi_lane = nmi;

    ppu->current_cycle = 0;
    ppu->frame_nr = 0;
    ppu->current_scanline = 0;
    ppu->frame_finished = false;

    ppu->fb = NULL;

    load_default_pal(ppu);

    ppu->log_file = fopen("ppu.log", "w");
    ppu->should_log = false;
    ppu->begin_log = false;
    ppu->address_latch = false;
    ppu->dma_active = false;
}
void cleanup_ppu(nes_ppu* ppu) {
    fclose(ppu->log_file);
}

void ppu_set_fb(nes_ppu* ppu, struct s_rgb* fb, int scale)
{
    ppu->fb = fb;
    ppu->scale = scale;
}

static void ppu_set_px(nes_ppu* ppu, uint8_t x, uint8_t y, uint8_t palette, uint8_t pixel) {
    struct s_rgb c = get_color(ppu, palette, pixel, Read);
    int fb_x = x * ppu->scale;
    int fb_y = y * ppu->scale;
    int w = 256 * ppu->scale;
    for(int i=0; i<ppu->scale; i++) {
        for(int j=0; j<ppu->scale; j++) {
            ppu->fb[fb_x+i + (fb_y+j)*w] = c;
        }
    }
}

void ppu_drw_pixel(nes_ppu* ppu) {

    uint8_t bg_pixel = 0x00;
    uint8_t bg_palette = 0x00;

    if(should_render_bg(ppu)) {

        uint16_t bit_mux = 0x8000 >> ppu->fine_x;
        uint8_t p0_pixel = (ppu->bg_shifter.pattern_lo & bit_mux) > 0;
        uint8_t p1_pixel = (ppu->bg_shifter.pattern_hi & bit_mux) > 0;
        bg_pixel = (p1_pixel << 1) | p0_pixel;

        uint8_t bg_pal0 = (ppu->bg_shifter.attrib_lo & bit_mux) > 0;
        uint8_t bg_pal1 = (ppu->bg_shifter.attrib_hi & bit_mux) > 0;
        bg_palette = (bg_pal1 << 1) | bg_pal0;

    }

    uint8_t fg_pixel = 0x00;
    uint8_t fg_palette = 0x00;
    // Set if the foreground overlays the background
    bool fg_overlays = false;

    uint8_t sprite_nr = 0;
    if(should_render_sprites(ppu)) {
        // TODO: do the drawing
        for(uint8_t i = 0; i<ppu->sprite_count; i++) {
            struct s_oamentry sprite = ppu->secondary_oam.entries[i];
            if(sprite.x_pos == 0) {
                uint8_t lo = (ppu->sprite_shifter_pattern_lo[i] & 0x80) > 0;
                uint8_t hi = (ppu->sprite_shifter_pattern_hi[i] & 0x80) > 0;
                fg_pixel = (hi << 1) | lo;
                fg_palette = sprite_palette(sprite);
                fg_overlays = sprite_priority(sprite) == 0;

                if(fg_pixel != 0) {
                    sprite_nr = i;
                    break;
                }
            }
        }
    }

    int x = ppu->current_cycle-1;
    int y = ppu->current_scanline;
    if(!should_render_left_bg(ppu) && x < 8) {
        bg_pixel = 0x00;
    }
    if(!should_render_left_sprites(ppu) && x < 8) {
        fg_pixel = 0x00;
    }

    uint8_t pixel = 0x00;
    uint8_t palette = 0x00;

    if(bg_pixel == 0 && fg_pixel == 0) {
        // Both are transparent, so bg color
        pixel = 0x00;
        palette = 0x00;
    } else if(bg_pixel == 0 && fg_pixel > 0) {
        // Foreground overlays transparent background
        pixel = fg_pixel;
        palette = fg_palette;
    } else if(bg_pixel > 0 && fg_pixel == 0) {
        // Bg shows through the foreground
        pixel = bg_pixel;
        palette = bg_palette;
    } else {
        // TODO: trigger sprite 0 hit here
        if(sprite_nr == 0 && x != 255 && ppu->can_hit_sprite0) {
            ppu->can_hit_sprite0 = false;
            ppu->registers[2] |= (1 << 6);
        }
        if(fg_overlays) {
            // Foreground overlays bg
            pixel = fg_pixel;
            palette = fg_palette;
        } else {
            pixel = bg_pixel;
            palette = bg_palette;
        }
    }
    ppu_set_px(ppu, x, y, palette, pixel);

}

inline void ppu_set_addr(nes_ppu* ppu, uint16_t addr) {
    *ppu->ppu_addr = addr;
}

void cycle_ppu(nes_ppu* ppu) {
    ppu->frame_finished = false;


    if(ppu->current_cycle == 0 && ppu->current_scanline == 0) {
        // Odd frame cycle skip
        if(ppu->frame_nr % 2 == 1)
            ppu->current_cycle = 1;

        ppu->should_log = ppu->begin_log;
        if(ppu->should_log) {
            log_info("Starting recording of this frames timings");
        }
    }
    if(ppu->should_log && ppu->current_cycle == 0) {
        fprintf(ppu->log_file, "\nL:%03d ", ppu->current_scanline);
    }

    if (ppu->current_scanline == 261) {
        ppu_prerender_scanline(ppu);
    } else if(ppu->current_scanline >= 0 && ppu->current_scanline <= 239) {
        ppu_visible_scanline(ppu);
    } else if(ppu->current_scanline == 240) {
		ppu_postrender_scanline(ppu);
    } else if(ppu->current_scanline >= 241 && ppu->current_scanline <= 260) {
		ppu_vblank_scanline(ppu);
    } else assert(0);

    ppu->current_cycle++;
    if(ppu->current_cycle == 341) {
        ppu->current_cycle = 0;
        ppu->current_scanline ++;
        if(ppu->current_scanline == 262) {
            ppu->current_scanline = 0;
            ppu->frame_finished = true;
            ppu->frame_nr++;
            ppu->should_log = false;
            ppu->begin_log = false;
        }
    }
}

void load_pal(nes_ppu* ppu, const char* filename)
{
    FILE* f = fopen(filename, "r");
    if(!f) {
        log_error("Could not open file %s", filename);
        return;
    }
    for(int i=0; i<64; i++) {
        fread(&ppu->colors[i].r, 1, 1, f);
        fread(&ppu->colors[i].g, 1, 1, f);
        fread(&ppu->colors[i].b, 1, 1, f);
    }
    fclose(f);
}

uint8_t vram_increment(nes_ppu* ppu) {
    return ppu->registers[0] & 0x4 ? 32 : 1;
}
uint16_t base_nametable_addr(nes_ppu* ppu) {
    static const uint16_t offsets[4] = {0x2000, 0x2400, 0x2800, 0x2c00};
    return offsets[ppu->registers[0] & 0x3];
}
inline uint16_t base_attribute_addr(nes_ppu* ppu) {
    return base_nametable_addr(ppu) + 0x3c0;
}
inline uint16_t bg_pattern_table_addr(nes_ppu* ppu) {
    return ppu->registers[0] & 0x10 ? 0x1000 : 0x0000;
}
inline uint16_t base_sprite_addr(nes_ppu* ppu) {
    return ppu->registers[0] & 0x8 ? 0x1000 : 0x0000;
}
inline uint8_t  sprite_height(nes_ppu* ppu) {
    return ppu->registers[0] & 0x20 ? 16 : 8;
}
inline bool should_render_left_bg(nes_ppu* ppu)
{
    return ppu->registers[1] & 0x02;
}
inline bool should_render_left_sprites(nes_ppu* ppu)
{
    return ppu->registers[1] & 0x04;
}
bool is_nmi_enabled(nes_ppu* ppu) {
    return ppu->registers[0] & 0x80;
}

bool should_render_sprites(nes_ppu* ppu) {
    return ppu->registers[1] & 0x10;
}

inline bool should_render_bg(nes_ppu* ppu) {
    return ppu->registers[1] & 0x8;
}
inline struct s_rgb get_color(nes_ppu* ppu, byte_t palette, byte_t pixel, MemoryBusMode mode)
{
    assert(palette <= 7);
    assert(pixel <= 3);
    uint8_t color = *ppu_byte(ppu->membus, PALETTE_START + ((palette << 2) | pixel), mode);
    assert(color <= 0x40);
    return ppu->colors[color];
}

DJWindow* ppu_win;
