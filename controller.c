#include "controller.h"
#include "logger.h"

void controller_set(NesController* ctr, enum e_controllerbuttons button, bool status)
{
    if(status) {
        ctr->buffer |= (1 << button);
    } else {
        ctr->buffer &= ~(1 << button);
    }
	ctr->buttons[button] = status;
}
void controller_buffer(NesController* ctr)
{
    ctr->shift_register = ctr->buffer;
}
uint8_t controller_read(NesController* ctr)
{
    uint8_t status = ctr->shift_register & 0x1;
    ctr->shift_register >>= 1;
    return status;
}
