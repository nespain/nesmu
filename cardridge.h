#ifndef CARDRIDGE_H
#define CARDRIDGE_H

#include "6502.h"

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

typedef enum {VerticalMirror, HorizontalMirror, OneScreen_LO, OneScreen_HI} MirrorMode;
typedef enum {NROM_mapper = 0, MMC1, uxROM, CNROM = 0x03, MMC3 = 0x04, AxROM = 0x07} MapperType;

#define NUM_REGISTERS 24

typedef struct s_cardridge {
    MirrorMode nametable_mirroring;
    MapperType mapper_type;

    uint8_t num_prg_banks;
    uint8_t num_chr_banks;

    byte_t* PRG;
    byte_t* CHR;
	byte_t* PRG_RAM;

    byte_t* battery_ram;
    char* filename;

    byte_t registers[NUM_REGISTERS];
    int* irq_line;
} Cardridge;

void destroy_cardridge(Cardridge* cardridge);
byte_t* cardridge_chr(Cardridge* cardridge, uint16_t addr);
byte_t* cardridge_cpu_bus(Cardridge* cardridge, uint16_t addr, MemoryBusMode mode);
int cardridge_write(Cardridge* cardridge, uint16_t addr, byte_t data);

#endif
