#include "6502.h"
#include <assert.h>
#include <stddef.h>
#include <stdbool.h>
#include <sys/types.h>
#include <stdio.h>
#include "debug.h"
#include "logger.h"

#include "colors.h"

static char buf[1024];
const char* state_str(int s) {
    if(s >= 1024) {
        buf[0] = 'R';
        buf[1] = '0' + s - 1024;
    } else {
        buf[0] = 'T';
        buf[1] = '0' + s;
    }
    buf[2] = '\0';
    return buf;
}

static void stack_push(struct s_stack* s, uint16_t v) {
	//if(s->data[s->top] == v) return;
	s->data[s->top] = v;
	s->top++;
}
static void stack_pop(struct s_stack* s) {
	s->top--;
}

#define INVALID_STATE_STR log_error("Invalid instruction state %s", state_str(cpu->instruction_state))
#define INVALID_DEFAULT default: \
INVALID_STATE_STR; \
return 2;

#define HANDLE_FLAG_OPS(suffix, name) \
    case(SE##suffix): \
        set_##name(cpu, true); \
        fetch(cpu); \
        break; \
    case(CL##suffix): \
        set_##name(cpu, false); \
        fetch(cpu); \
        break; \

static inline byte_t read_data(nmos6502* cpu) {
	return *cpu->data_pins;
}

void set_flag(nmos6502* cpu, byte_t mask, bool state) {
	if(state)
		cpu->flags |= mask;
	else
		cpu->flags &= (~mask);
}
static void set_reg(nmos6502* cpu, byte_t* dst, byte_t value) {
    if(dst != NULL) *dst = value;
    set_zero(cpu, value == 0);
    set_negative(cpu, value & 0x80);
}
static inline void set_a(nmos6502* cpu, byte_t value) {
    set_reg(cpu, &cpu->accumulator, value);
}
static inline void set_x(nmos6502* cpu, byte_t value) {
    set_reg(cpu, &cpu->reg_x, value);
}
static inline void set_y(nmos6502* cpu, byte_t value) {
    set_reg(cpu, &cpu->reg_y, value);
}

static void fetch(nmos6502* cpu)
{
    cpu->current_instruction_pc = cpu->program_counter;
    cpu->instruction_state = T0;
	cpu->bus_mode = Read;
	*cpu->address_pins = cpu->program_counter;
    cpu->sync = true;
}

void init(nmos6502* cpu, uint16_t* addr_bus, byte_t* data_bus)
{
	cpu->address_pins = addr_bus;
	cpu->data_pins = data_bus;
	cpu->program_counter = 0;
	cpu->cycle = 0;
    cpu->nmi_line = false;
    cpu->irq_line = false;
	cpu->reset_line = false;
	cpu->stack_ptr = 0xFD;
	
	cpu->stacktrace.top = 0;
}

static int reset_procedure(nmos6502* cpu) {
	switch(cpu->instruction_state) {
		case R0:
            log_debug("Handling Reset");
			cpu->instruction_state++;
			cpu->bus_mode = Read;
            set_interrupt(cpu, true);
            set_const(cpu, true);
			break;
		case R1:
			cpu->instruction_state++;
			cpu->bus_mode = Read;
			break;
		case R2:
			*cpu->address_pins = STACK_START + cpu->stack_ptr;
			cpu->bus_mode = Read;
			cpu->instruction_state++;
			break;
		case R3:
			*cpu->address_pins = STACK_START + cpu->stack_ptr - 1;
			cpu->bus_mode = Read;
			cpu->instruction_state++;
			break;
		case R4:
			*cpu->address_pins = STACK_START + cpu->stack_ptr - 2;
			cpu->bus_mode = Read;
			cpu->instruction_state++;
			break;
		case R5:
			*cpu->address_pins = ResetVec; // PCL
			cpu->bus_mode = Read;
			cpu->instruction_state++;
			break;
		case R6:
			*cpu->address_pins = ResetVec+1; // PCH
			cpu->bus_mode = Read;
			cpu->program_counter = read_data(cpu); // Read the low bits
			cpu->instruction_state++;
			break;
		case R7:
			cpu->program_counter |= read_data(cpu) << 8;
            log_debug("Reset complete. Starting of at %#04x", cpu->program_counter);
			cpu->stacktrace.top = 0;
			stack_push(&cpu->stacktrace, cpu->program_counter);
            fetch(cpu);
			break;
		INVALID_DEFAULT
	}
    return 0;
}
int interrupt_procedure(nmos6502* cpu, bool is_break) {
    /*
     *  #  address R/W description
     *--- ------- --- -----------------------------------------------
        0    PC     R  fetch opcode (and discard it - $00 (BRK) is forced into the opcode register instead)
        1    PC     R  read next instruction byte (actually the same as above, since PC increment is suppressed. Also discarded.)
        2  $0100,S  W  push PCH on stack, decrement S
        3  $0100,S  W  push PCL on stack, decrement S
        *** At this point, the signal status determines which interrupt vector is used ***
        4  $0100,S  W  push P on stack (with B flag *clear*), decrement S
        5   A       R  fetch PCL (A = FFFE for IRQ, A = FFFA for NMI), set I flag
        6   A       R  fetch PCH (A = FFFF for IRQ, A = FFFB for NMI)
     */
    switch(cpu->instruction_state) {
        case(I0):
            //log_debug("Handling interrupt old pc was at %#04x", cpu->program_counter);
            // Read instruction byte and prepare reading it again
            (void) read_data(cpu);
            cpu->instruction_buffer = BRK;
            cpu->bus_mode = Read;
            *cpu->address_pins = cpu->program_counter;
            cpu->instruction_state++;
            break;
        case(I1):
        case(T1):
            // Read instruction byte and prepare writing PCH
            (void) read_data(cpu);
            cpu->bus_mode = Write;
            *cpu->address_pins = STACK_START + cpu->stack_ptr;
            *cpu->data_pins = cpu->program_counter >> 8;
            cpu->stack_ptr--;
            cpu->instruction_state++;
            break;
        case(I2):
        case(T2):
            // Push PCH and prepare writing PCL
            cpu->bus_mode = Write;
            *cpu->address_pins = STACK_START + cpu->stack_ptr;
            *cpu->data_pins = cpu->program_counter & 0x00FF;
            cpu->stack_ptr--;
            cpu->instruction_state++;
            break;
        case(I3):
        case(T3):
            // Push PCL and prepare pushing flag reg
            cpu->bus_mode = Write;
            *cpu->address_pins = STACK_START + cpu->stack_ptr;
            *cpu->data_pins = cpu->flags & ~BreakMask;
            if(is_break) *cpu->data_pins |= BreakMask;
            set_interrupt(cpu, true);
            cpu->stack_ptr--;
            cpu->instruction_state++;
            break;
        case(I4):
        case(T4):
            // Push flags and prepare fetching PCL
            cpu->bus_mode = Read;
            *cpu->address_pins = cpu->nmi_occured ? NMIVec : IRQVec;
            cpu->instruction_state++;
            break;
        case(I5):
        case(T5):
            // Read PCL and prepare fetching PCH
            cpu->buffer = read_data(cpu);
            cpu->bus_mode = Read;
            *cpu->address_pins = cpu->nmi_occured ? NMIVec : IRQVec;
            *cpu->address_pins += 1;
            cpu->instruction_state++;
            break;
        case(I6):
        case(T6):
            // Fetch PCH and set PC
            cpu->buffer |= read_data(cpu) << 8;
            cpu->program_counter = cpu->buffer;
            fetch(cpu);
            if(cpu->nmi_occured) {
                cpu->nmi_occured = false;
            } else {
                cpu->irq_occured = false;
            }
            break;
        INVALID_DEFAULT
    }
    return 0;
}

/**
 * Returns the instructions immediate value
 */
__attribute_warn_unused_result__ byte_t IMM(nmos6502* cpu) {
    assert(cpu->instruction_state == T1 && TTY_RED"Invalid instruction state.\nImmediate mode instruction needs to be in state T1."TTY_RESET);
    byte_t res = read_data(cpu);
    cpu->program_counter++;
    return res;
}
/**
 * Performs T1 of the zpg instruction.
 * Zeropage address is then in cpu->buffer
 * The data can then be fetched in T2 manually using read_data(cpu)
 */
void ZPG(nmos6502* cpu) {
    assert(cpu->instruction_state == T1 && TTY_RED"Invalid instruction state. this zpg assistant is always in T1"TTY_RESET);
    cpu->buffer = IMM(cpu);
    cpu->bus_mode = Read;
    *cpu->address_pins = cpu->buffer;
    cpu->instruction_state++;
}

/**
 * Performs T1 and T2 of zpg index instruction
 * The zeropage address is stored in cpu->buffer
 * The data byte can be obtained in T3 using read_data(cpu)
 */
int ZPG_index(nmos6502* cpu, byte_t index) {
    assert(cpu->instruction_state >= T1 && cpu->instruction_state <= T2);
    switch(cpu->instruction_state) {
        case(T1):
            cpu->smallbuffer = IMM(cpu);
            cpu->bus_mode = Read;
            *cpu->address_pins = cpu->smallbuffer;
            cpu->instruction_state++;
            break;
        case(T2):
            cpu->bus_mode = Read;
            cpu->smallbuffer += index;
            cpu->buffer = cpu->smallbuffer;
            *cpu->address_pins = cpu->smallbuffer;
            cpu->instruction_state++;
            break;
        default: assert(0);
    }
    return 0;
}
#define ZPG_X(cpu) ZPG_index(cpu, cpu->reg_x)
#define ZPG_Y(cpu) ZPG_index(cpu, cpu->reg_y)

/**
 * Performs the T1 and T2 steps of an absolute instruction by fetching the correct address and updating the instruction state
 */
void ABS(nmos6502* cpu) {
    assert((cpu->instruction_state == T1 || cpu->instruction_state == T2) && TTY_RED"Invalid instruction state.\nAbsolute addressing is done in state T1 and T2."TTY_RESET);
    if(cpu->instruction_state == T1) {
        cpu->buffer = read_data(cpu);
        cpu->program_counter++;
        cpu->bus_mode = Read;
        *cpu->address_pins = cpu->program_counter;
    } else if (cpu->instruction_state == T2) {
        cpu->buffer |= read_data(cpu) << 8;
        cpu->bus_mode = Read;
        *cpu->address_pins = cpu->buffer;
        cpu->program_counter++;
    }
    cpu->instruction_state++;
}
bool ABS_I(nmos6502* cpu, byte_t index) {
    switch(cpu->instruction_state) {
        case(T1):
            ABS(cpu);
            break;
        case(T2):
            ABS(cpu);
            cpu->bus_mode = Read;
            *cpu->address_pins = cpu->buffer + index;
            break;
        case(T3):
            if((cpu->buffer & 0xFF00) == ((cpu->buffer + index) & 0xFF00)) {
                return true;
            } else {
                cpu->bus_mode = Read;
                *cpu->address_pins = cpu->buffer + (uint16_t) index;
                cpu->instruction_state++;
            }
            break;
        case(T4):
            return true;
        default:
            INVALID_STATE_STR;
    }
    return 0;
}
#define ABS_X(cpu) ABS_I(cpu, cpu->reg_x)
#define ABS_Y(cpu) ABS_I(cpu, cpu->reg_y)
/**
 * Operates on Cycle T1-T5
 * After it returns true the result should be written into cpu->smallbuffer
 */
bool ABS_INDEX_WRITE(nmos6502* cpu, byte_t index) {
    switch(cpu->instruction_state) {
        case(T1):
            ABS(cpu);
            break;
        case(T2):
            ABS(cpu);
            cpu->buffer += index;
            cpu->bus_mode = Read;
            *cpu->address_pins = cpu->buffer;
            break;
        case(T3):
            cpu->bus_mode = Read;
            *cpu->address_pins = cpu->buffer;
            cpu->instruction_state++;
            break;
        case(T4):
            cpu->smallbuffer = read_data(cpu);
            cpu->bus_mode = Write;
            *cpu->address_pins = cpu->buffer;
            *cpu->data_pins = cpu->smallbuffer;
            cpu->instruction_state++;
            return true;
        case(T5):
            cpu->bus_mode = Write;
            *cpu->address_pins = cpu->buffer;
            *cpu->data_pins = cpu->smallbuffer;
            cpu->instruction_state++;
            break;
        case(T6): fetch(cpu); break;
        INVALID_DEFAULT
    }
    return false;
}
#define ABS_X_WRITE(cpu) ABS_INDEX_WRITE(cpu, cpu->reg_x)
#define ABS_Y_WRITE(cpu) ABS_INDEX_WRITE(cpu, cpu->reg_y)

/**
 * Performs T1-T4 of an indirect instruction and loads the loaded data into cpu->buffer2
 */
void IND(nmos6502* cpu) {
    assert((cpu->instruction_state >= T1 || cpu->instruction_state <= T4) && TTY_RED"Invalid instruction state.\nIndirect addressing is done between state T1 and T4."TTY_RESET);
    if(cpu->instruction_state == T1) {
        ABS(cpu);
    } else if(cpu->instruction_state == 2) {
        ABS(cpu);
        cpu->bus_mode = Read;
        *cpu->address_pins = cpu->buffer;
    } else if(cpu->instruction_state == T3){
        cpu->buffer2 = read_data(cpu);
        cpu->bus_mode = Read;
        *cpu->address_pins = cpu->buffer+1;
        cpu->instruction_state++;
    } else {
        cpu->buffer2 |= read_data(cpu) << 8;
        cpu->instruction_state++;
    }
}
void IND_X(nmos6502* cpu) {
    assert(cpu->instruction_state >= T1 && cpu->instruction_state < T5);
    switch(cpu->instruction_state) {
        case(T1):
            cpu->smallbuffer = IMM(cpu);
            cpu->bus_mode = Read;
            *cpu->address_pins = cpu->smallbuffer;
            cpu->instruction_state++;
            break;
        case(T2):
            cpu->bus_mode = Read;
            cpu->smallbuffer += cpu->reg_x;
            *cpu->address_pins = cpu->smallbuffer;
            cpu->smallbuffer++;
            cpu->instruction_state++;
            break;
        case(T3):
            cpu->buffer = read_data(cpu);
            cpu->bus_mode = Read;
            *cpu->address_pins = cpu->smallbuffer;
            cpu->instruction_state++;
            break;
        case(T4):
            cpu->buffer |= read_data(cpu) << 8;
            cpu->bus_mode = Read;
            *cpu->address_pins = cpu->buffer;
            cpu->instruction_state++;
            break;
        default: assert(0);
    }
}
/** Does step T1-T4/5 of indirect Y addressing mode. when the function returns true, the data cell content can be found with
 * read_data(cpu);
 */
bool IND_Y(nmos6502* cpu) {
    assert(cpu->instruction_state >= T1 && cpu->instruction_state <= T5);
    switch(cpu->instruction_state) {
        case(T1):
            // Fetch page zero indirect address
            cpu->smallbuffer = read_data(cpu);
            cpu->program_counter++;
            cpu->bus_mode = Read;
            *cpu->address_pins = cpu->smallbuffer;
            cpu->smallbuffer++;
            cpu->instruction_state++;
            break;
        case(T2):
            //fetch low order byte of base address
            cpu->buffer = read_data(cpu);
            cpu->bus_mode = Read;
            *cpu->address_pins = cpu->smallbuffer;
            cpu->instruction_state++;
            break;
        case(T3):
            // We do the calculation in a 8-bit int to simulate possible overflow
            cpu->smallbuffer = cpu->buffer + cpu->reg_y;
            cpu->overflow_occured = cpu->smallbuffer < cpu->buffer;

            cpu->buffer |= (read_data(cpu) << 8);
            cpu->buffer += cpu->reg_y;

            cpu->bus_mode = Read;
            *cpu->address_pins = cpu->buffer;
            cpu->instruction_state++;
            break;
        case(T4):
            if(cpu->overflow_occured) {
                cpu->bus_mode = Read;
                *cpu->address_pins = cpu->buffer;
                cpu->instruction_state++;
            } else {
                return true;
            }
            break;
        case(T5):
            return true;
            break;
        INVALID_DEFAULT
    }
    return false;
}

int store_zpg(nmos6502* cpu, byte_t value) {
    switch(cpu->instruction_state) {
        case(T1):
            cpu->bus_mode = Write;
            *cpu->address_pins = IMM(cpu);
            *cpu->data_pins = value;
            cpu->instruction_state++;
            break;
        case(T2):
            fetch(cpu);
            break;
        INVALID_DEFAULT
    }
    return 0;
}
int store_zpg_index(nmos6502* cpu, byte_t value, byte_t index) {
    switch(cpu->instruction_state) {
        case(T1): ZPG_index(cpu, index); break;
        case(T2):
            ZPG_index(cpu, index);
            cpu->bus_mode = Write;
            *cpu->data_pins = value;
            break;
        case(T3):
            fetch(cpu);
            break;
        INVALID_DEFAULT
    }
    return 0;
}
#define store_zpg_x(cpu, value) store_zpg_index(cpu, value, cpu->reg_x)
#define store_zpg_y(cpu, value) store_zpg_index(cpu, value, cpu->reg_y)

int store_abs(nmos6502* cpu, byte_t value) {
    switch(cpu->instruction_state) {
        case(T1):
            ABS(cpu);
            break;
        case(T2):
            ABS(cpu);
            cpu->bus_mode = Write;
            *cpu->address_pins = cpu->buffer;
            *cpu->data_pins = value;
            break;
        case(T3):
            fetch(cpu);
            break;
        INVALID_DEFAULT
    }
    return 0;
}
int store_abs_index(nmos6502* cpu, byte_t value, byte_t index) {
    switch(cpu->instruction_state) {
        case(T1):
            ABS(cpu);
            break;
        case(T2):
            ABS(cpu);
            cpu->bus_mode= Read;
            *cpu->address_pins = cpu->buffer + index;
            break;
        case(T3):
            cpu->bus_mode = Write;
            *cpu->address_pins = cpu->buffer + index;
            *cpu->data_pins = value;
            cpu->instruction_state++;
            break;
        case(T4):
            fetch(cpu);
            break;
        INVALID_DEFAULT
    }
    return 0;
}
int load_abs(nmos6502* cpu, byte_t* dst) {
    switch(cpu->instruction_state) {
        case(T1):
            ABS(cpu);
            break;
        case(T2):
            ABS(cpu);
            cpu->bus_mode = Read;
            *cpu->address_pins = cpu->buffer;
            break;
        case(T3):
            set_reg(cpu, dst, read_data(cpu));
            fetch(cpu);
            break;
        INVALID_DEFAULT
    }
    return 0;
}
int load_zpg(nmos6502* cpu, byte_t* dst) {
    switch(cpu->instruction_state) {
        case(T1): ZPG(cpu); break;
        case(T2):
            set_reg(cpu, dst, read_data(cpu));
            fetch(cpu);
            break;
        INVALID_DEFAULT
    }
    return 0;
}
int load_zpg_index(nmos6502* cpu, byte_t* dst, byte_t index) {
    switch(cpu->instruction_state) {
        case(T1): ZPG_index(cpu, index); break;
        case(T2): ZPG_index(cpu, index); break;
        case(T3):
            set_reg(cpu, dst, read_data(cpu));
            fetch(cpu);
            break;
        INVALID_DEFAULT
    }
    return 0;
}
#define load_zpg_x(cpu, dst) load_zpg_index(cpu, dst, cpu->reg_x);
#define load_zpg_y(cpu, dst) load_zpg_index(cpu, dst, cpu->reg_y);

int load_abs_index(nmos6502* cpu, byte_t* dst, byte_t index) {
    if(ABS_I(cpu, index)) {
        set_reg(cpu, dst, read_data(cpu));
        fetch(cpu);
    }
    return 0;
}

int handle_branch(nmos6502* cpu, bool condition) {
    switch(cpu->instruction_state) {
        case(T1):
            int8_t offset = IMM(cpu);
            if(!condition) {
                fetch(cpu);
                return 0;
            }

            // We store the old page in the smallbuffer
            cpu->smallbuffer = cpu->program_counter >> 8;

            cpu->program_counter += offset;
            cpu->instruction_state++;
            break;
        case(T2):
            // Check if the page is crossed
            if(cpu->smallbuffer != (cpu->program_counter >> 8)) {
                cpu->bus_mode = Read;
                *cpu->address_pins = cpu->program_counter;
                cpu->instruction_state++;
            } else {
                fetch(cpu);
            }
            break;
        case(T3): fetch(cpu); break;
        INVALID_DEFAULT
    }
    return 0;
}

void cmp(nmos6502* cpu, byte_t reg, byte_t param)
{
    set_zero(cpu, reg == param);
    uint8_t sub = reg - param;
    set_negative(cpu, sub >> 7);
    set_carry(cpu, reg >= param);
}

int push(nmos6502* cpu, byte_t value) {
    switch(cpu->instruction_state) {
        case(T1):
            cpu->bus_mode = Write;
            *cpu->address_pins = STACK_START + cpu->stack_ptr;
            *cpu->data_pins = value;
            cpu->stack_ptr--;
            cpu->instruction_state++;
            break;
        case(T2):
            fetch(cpu);
            break;
        default:
            log_error("Invalid instruction state");
            return 2;
    }
    return 0;
}

void adc(nmos6502* cpu, byte_t operand) {
    byte_t carry = flag_set(cpu, CarryMask) ? 0x1 : 0x0;
    uint16_t sum = cpu->accumulator + operand + carry;
    set_carry(cpu, sum > 0xff);
    // Overflow flag is set for a+b=c when
    // sign(a) = sign(b) and sin(a) != sign(c)
    set_overflow(cpu, ~(cpu->accumulator ^ operand) & (cpu->accumulator ^ sum) & 0x80);
    set_a(cpu, sum & 0xFF);
}
#define sbc(cpu, operand) adc(cpu, ~operand)

int change_mem_abs(nmos6502* cpu, int8_t delta) {
    switch(cpu->instruction_state) {
        case(T1):
            ABS(cpu);
            break;
        case(T2):
            ABS(cpu);
            cpu->bus_mode = Read;
            *cpu->address_pins = cpu->buffer;
            break;
        case(T3):
            cpu->smallbuffer = read_data(cpu);
            cpu->bus_mode = Write;
            *cpu->address_pins = cpu->buffer;
            *cpu->data_pins = cpu->smallbuffer;
            cpu->smallbuffer += delta;
            set_reg(cpu, NULL, cpu->smallbuffer);
            cpu->instruction_state++;
            break;
        case(T4):
            cpu->bus_mode = Write;
            *cpu->address_pins = cpu->buffer;
            *cpu->data_pins = cpu->smallbuffer;
            cpu->instruction_state++;
            break;
        case(T5): fetch(cpu); break;
        INVALID_DEFAULT
    }
    return 0;
}

void asl_smallbuffer(nmos6502* cpu) {
    set_carry(cpu, cpu->smallbuffer >> 7);
    cpu->smallbuffer <<= 1;
    set_reg(cpu, NULL, cpu->smallbuffer);
}
void lsr_smallbuffer(nmos6502* cpu) {
    set_carry(cpu, cpu->smallbuffer & 0x1);
    cpu->smallbuffer >>= 1;
    set_reg(cpu, NULL, cpu->smallbuffer);
}
void ror_smallbuffer(nmos6502* cpu) {
    byte_t carry = flag_set(cpu, CarryMask) ? 0x01 : 0x00;
    set_carry(cpu, cpu->smallbuffer & 0x1);
    cpu->smallbuffer = (cpu->smallbuffer >> 1) | (carry << 7);
    set_reg(cpu, NULL, cpu->smallbuffer);
}
void rol_smallbuffer(nmos6502* cpu) {
    byte_t carry = flag_set(cpu, CarryMask) ? 0x01 : 0x00;
    set_carry(cpu, cpu->smallbuffer >> 7);
    cpu->smallbuffer = (cpu->smallbuffer << 1) | carry;
    set_reg(cpu, NULL, cpu->smallbuffer);

}

int cycle(nmos6502 *cpu)
{
    if(cpu->rdy_line) return 0;

    cpu->sync = false;
	if(cpu->reset_line) {
		cpu->instruction_state = R0;
	}
    if(cpu->nmi_line) {
        //log_debug("Received NMI");
        cpu->nmi_occured = true;
        cpu->nmi_line--;
    }
    if(cpu->irq_line && !flag_set(cpu, InterruptMask)) {
        cpu->irq_occured = true;
        cpu->irq_line--;
    }

	if(cpu->instruction_state >= R0 && cpu->instruction_state <= R7) {
		int status = reset_procedure(cpu);
        cpu->cycle++;
		return status;
	}
	if(cpu->instruction_state == T0) {
        if(cpu->nmi_occured || cpu->irq_occured) {
            cpu->instruction_state = I0;
        } else {
            cpu->instruction_buffer = read_data(cpu);
            switch(cpu->instruction_buffer) {
                case(JAM_a):
                case(JAM_b):
                case(JAM_c):
                case(JAM_d):
                case(JAM_e):
                case(JAM_f):
                case(JAM_g):
                case(JAM_h):
                case(JAM_i):
                case(JAM_j):
                case(JAM_k):
                case(JAM_l):
                    cpu->instruction_state = JAM;
                    break;
                default: break;
            }
            cpu->program_counter++;
            cpu->bus_mode = Read;
            *cpu->address_pins = cpu->program_counter;
            cpu->instruction_state = T1;
        }
        cpu->cycle++;
        return 0;
	}
    if(cpu->instruction_state >= I0 && cpu->instruction_state <= I6) {
        int res = interrupt_procedure(cpu, false);
        cpu->cycle++;
        return res;
    }

	switch(cpu->instruction_buffer) {
		case(LDA_imm):
            set_a(cpu, IMM(cpu));
			fetch(cpu);
			break;
        case(LDA_zpg):
            load_zpg(cpu, &cpu->accumulator);
            break;
        case(LDA_zpgX):
            load_zpg_x(cpu, &cpu->accumulator);
            break;
        case(LDY_imm):
            set_y(cpu, IMM(cpu));
            fetch(cpu);
            break;
        case(LDY_zpg):
            load_zpg(cpu, &cpu->reg_y);
            break;
        case(LDY_zpgX):
            load_zpg_x(cpu, &cpu->reg_y);
            break;
        case(LDX_imm):
            set_x(cpu, IMM(cpu));
            fetch(cpu);
            break;
        case(LDX_zpg):
            load_zpg(cpu, &cpu->reg_x);
            break;
        case(LDX_zpgY):
            load_zpg_index(cpu, &cpu->reg_x, cpu->reg_y);
            break;
        HANDLE_FLAG_OPS(I, interrupt);
        HANDLE_FLAG_OPS(C, carry);
        HANDLE_FLAG_OPS(D, decimal);
        case(CLV):
            set_overflow(cpu, false);
            fetch(cpu);
            break;
        case(STA_zpg):
            store_zpg(cpu, cpu->accumulator);
            break;
        case(STA_zpgX):
            store_zpg_x(cpu, cpu->accumulator);
            break;
		case(STA_abs):
            if(store_abs(cpu, cpu->accumulator)) return 2;
			break;
        case(STA_absY):
            store_abs_index(cpu, cpu->accumulator, cpu->reg_y);
            break;
        case(STA_absX):
            store_abs_index(cpu, cpu->accumulator, cpu->reg_x);
            break;
        case(STA_indX):
            switch(cpu->instruction_state) {
                case(T1): IND_X(cpu); break;
                case(T2): IND_X(cpu); break;
                case(T3): IND_X(cpu); break;
                case(T4):
                    IND_X(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->accumulator;
                    break;
                case(T5):
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(i_SAX_zpg):
            store_zpg(cpu, cpu->accumulator & cpu->reg_x);
            break;
        case(i_SAX_zpgY):
            store_zpg_y(cpu, cpu->accumulator & cpu->reg_x);
            break;
        case(i_SAX_abs):
            if(store_abs(cpu, cpu->accumulator & cpu->reg_x)) return 2;
            break;
        case(i_SAX_indX):
            switch(cpu->instruction_state) {
                case(T1): IND_X(cpu); break;
                case(T2): IND_X(cpu); break;
                case(T3): IND_X(cpu); break;
                case(T4):
                    IND_X(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = (cpu->accumulator & cpu->reg_x);
                    break;
                case(T5): fetch(cpu); break;
                INVALID_DEFAULT
            } break;
        case(STX_abs):
            if(store_abs(cpu, cpu->reg_x)) return 2;
            break;
        case(STY_abs):
            if(store_abs(cpu, cpu->reg_y)) return 2;
            break;
        case(STY_zpg):
            store_zpg(cpu, cpu->reg_y);
            break;
        case(STY_zpgX):
            store_zpg_x(cpu, cpu->reg_y);
            break;
		case(TXS):
			assert(cpu->instruction_state == T1 && "invalid instruction state");
			cpu->stack_ptr = cpu->reg_x;
			fetch(cpu);
			break;
        case(TXA):
            set_a(cpu, cpu->reg_x);
            fetch(cpu);
            break;
        case(TYA):
            set_a(cpu, cpu->reg_y);
            fetch(cpu);
            break;
        case(PHA):
            push(cpu, cpu->accumulator);
            break;
        case(PHP):
            push(cpu, cpu->flags | BreakMask | ConstMask);
            break;
		case(LDA_abs):
			load_abs(cpu, &cpu->accumulator);
            break;
        case(LDA_absX):
            load_abs_index(cpu, &cpu->accumulator, cpu->reg_x);
            break;
        case(LDA_absY):
            load_abs_index(cpu, &cpu->accumulator, cpu->reg_y);
            break;
        case(LDA_indY):
            if(cpu->instruction_state >= T1 && cpu->instruction_state <= T5) {
                if(IND_Y(cpu)) {
                    set_a(cpu, read_data(cpu));
                    fetch(cpu);
                }
            } else {
                INVALID_STATE_STR;
                return 2;
            } break;
        case(i_LAX_zpg):
            switch(cpu->instruction_state) {
                case(T1): ZPG(cpu); break;
                case(T2):
                    set_a(cpu, read_data(cpu));
                    set_x(cpu, read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(i_LAX_zpgY):
            switch(cpu->instruction_state) {
                case(T1): ZPG_Y(cpu); break;
                case(T2): ZPG_Y(cpu); break;
                case(T3):
                    set_a(cpu, read_data(cpu));
                    set_x(cpu, read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(i_LAX_abs):
            switch(cpu->instruction_state) {
                case(T1): ABS(cpu); break;
                case(T2): ABS(cpu); break;
                case(T3):
                    set_a(cpu, read_data(cpu));
                    set_x(cpu, read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(i_LAX_absY):
            if(ABS_Y(cpu)) {
                set_a(cpu, read_data(cpu));
                set_x(cpu, read_data(cpu));
                fetch(cpu);
            } break;
        case(i_LAX_indX):
            if(cpu->instruction_state >= T1 && cpu->instruction_state <= T4) {
                IND_X(cpu);
            } else if(cpu->instruction_state == T5) {
                set_a(cpu, read_data(cpu));
                set_x(cpu, read_data(cpu));
                fetch(cpu);
            } else {
                INVALID_STATE_STR;
                return 2;
            } break;
        case(i_LAX_indY):
            if(cpu->instruction_state >= T1 && cpu->instruction_state <= T5) {
                if(IND_Y(cpu)) {
                    set_a(cpu, read_data(cpu));
                    set_x(cpu, read_data(cpu));
                    fetch(cpu);
                }
            } else {
                INVALID_STATE_STR;
                return 2;
            } break;
        case(LDY_abs):
            load_abs(cpu, &cpu->reg_y);
            break;
        case(LDY_absX):
            load_abs_index(cpu, &cpu->reg_y, cpu->reg_x);
            break;
        case(LDX_abs):
            load_abs(cpu, &cpu->reg_x);
            break;
        case(LDX_absY):
            load_abs_index(cpu, &cpu->reg_x, cpu->reg_y);
            break;
        case(BMI):
            handle_branch(cpu, flag_set(cpu, NegativeMask));
            break;
		case(BPL):
            handle_branch(cpu, !flag_set(cpu, NegativeMask));
            break;
        case(BCS):
            handle_branch(cpu, flag_set(cpu, CarryMask));
            break;
        case(BCC):
            handle_branch(cpu, !flag_set(cpu, CarryMask));
            break;
        case(BEQ):
            handle_branch(cpu, flag_set(cpu, ZeroMask));
            break;
        case(BNE):
            handle_branch(cpu, !flag_set(cpu, ZeroMask));
            break;
        case(BVS):
            handle_branch(cpu, flag_set(cpu, OverflowMask));
            break;
        case(BVC):
            handle_branch(cpu, !flag_set(cpu, OverflowMask));
            break;
        case(CMP_imm):
            cmp(cpu, cpu->accumulator, IMM(cpu));
            fetch(cpu);
            break;
        case(CMP_zpg):
            switch(cpu->instruction_state) {
                case(T1): ZPG(cpu); break;
                case(T2):
                    cmp(cpu, cpu->accumulator, read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(CMP_zpgX):
            switch(cpu->instruction_state) {
                case(T1): ZPG_X(cpu); break;
                case(T2): ZPG_X(cpu); break;
                case(T3):
                    cmp(cpu, cpu->accumulator, read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(CMP_abs):
            switch(cpu->instruction_state) {
                case(T1):
                    ABS(cpu);
                    break;
                case(T2):
                    ABS(cpu);
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->buffer;
                    break;
                case(T3):
                    cmp(cpu, cpu->accumulator, read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(CMP_absY):
            if(ABS_I(cpu, cpu->reg_y)) {
                cmp(cpu, cpu->accumulator, read_data(cpu));
                fetch(cpu);
            }
            break;
        case(CMP_absX):
            if(ABS_I(cpu, cpu->reg_x)) {
                cmp(cpu, cpu->accumulator, read_data(cpu));
                fetch(cpu);
            }
            break;
        case(CPX_imm):
            cmp(cpu, cpu->reg_x, IMM(cpu));
            fetch(cpu);
            break;
        case(CPX_zpg):
            switch(cpu->instruction_state) {
                case(T1):
                    ZPG(cpu);
                    break;
                case(T2):
                    cmp(cpu, cpu->reg_x, read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(CPX_abs):
            switch(cpu->instruction_state) {
                case(T1):
                    ABS(cpu);
                    break;
                case(T2):
                    ABS(cpu);
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->buffer;
                    break;
                case(T3):
                    cmp(cpu, cpu->reg_x, read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(CPY_imm):
            cmp(cpu, cpu->reg_y, IMM(cpu));
            fetch(cpu);
            break;
        case(CPY_zpg):
            switch(cpu->instruction_state) {
                case(T1):
                    ZPG(cpu);
                    break;
                case(T2):
                    cmp(cpu, cpu->reg_y, read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(CPY_abs):
            switch(cpu->instruction_state) {
                case(T1):
                    ABS(cpu);
                    break;
                case(T2):
                    ABS(cpu);
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->buffer;
                    break;
                case(T3):
                    cmp(cpu, cpu->reg_y, read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(DEC_zpg):
            switch(cpu->instruction_state) {
                case(T1):
                    ZPG(cpu);
                    break;
                case(T2):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->smallbuffer--;
                    cpu->instruction_state++;
                    break;
                case(T3):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    set_reg(cpu, NULL, cpu->smallbuffer);
                    cpu->instruction_state++;
                    break;
                case(T4):
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(DEC_zpgX):
            switch(cpu->instruction_state) {
                case(T1): ZPG_X(cpu); break;
                case(T2): ZPG_X(cpu); break;
                case(T3):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->smallbuffer--;
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    set_reg(cpu, NULL, cpu->smallbuffer);
                    cpu->instruction_state++;
                    break;
                case(T5):
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(DEC_abs):
            change_mem_abs(cpu, -1);
            break;
        case(DEC_absX):
            if(ABS_X_WRITE(cpu)) {
                cpu->smallbuffer--;
                set_reg(cpu, NULL, cpu->smallbuffer);
            } break;
        case(DEX):
            set_x(cpu, cpu->reg_x-1);
            fetch(cpu);
            break;
        case(DEY):
            set_y(cpu, cpu->reg_y-1);
            fetch(cpu);
            break;
        case(INC_zpg):
            switch(cpu->instruction_state) {
                case(T1):
                    ZPG(cpu);
                    break;
                case(T2):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->smallbuffer++;
                    cpu->instruction_state++;
                    break;
                case(T3):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    set_reg(cpu, NULL, cpu->smallbuffer);
                    cpu->instruction_state++;
                    break;
                case(T4):
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(INC_zpgX):
            switch(cpu->instruction_state) {
                case(T1): ZPG_X(cpu); break;
                case(T2): ZPG_X(cpu); break;
                case(T3):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->smallbuffer++;
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    set_reg(cpu, NULL, cpu->smallbuffer);
                    cpu->instruction_state++;
                    break;
                case(T5):
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(INC_abs):
            change_mem_abs(cpu, 1);
            break;
        case(INC_absX):
            if(ABS_X_WRITE(cpu)) {
                cpu->smallbuffer++;
                set_reg(cpu, NULL, cpu->smallbuffer);
            } break;
        case(INX):
            set_x(cpu, cpu->reg_x+1);
            fetch(cpu);
            break;
        case(INY):
            set_y(cpu, cpu->reg_y+1);
            fetch(cpu);
            break;
        case(JSR):
            switch(cpu->instruction_state) {
                case(T1):
                    cpu->buffer = read_data(cpu);
                    cpu->program_counter++;
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->stack_ptr + STACK_START;
                    cpu->instruction_state++;
                    break;
                case(T2):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->stack_ptr + STACK_START;
                    cpu->stack_ptr--;
                    *cpu->data_pins = (cpu->program_counter >> 8);
                    cpu->instruction_state++;
                    break;
                case(T3):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->stack_ptr + STACK_START;
                    cpu->stack_ptr--;
                    *cpu->data_pins = cpu->program_counter & 0xFF;
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->program_counter;
                    cpu->instruction_state++;
                    break;
                case(T5):
					stack_push(&cpu->stacktrace, cpu->program_counter);
                    cpu->buffer |= (read_data(cpu) << 8);
                    cpu->program_counter = cpu->buffer;
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(STX_zpg):
            store_zpg(cpu, cpu->reg_x);
            break;
        case(STX_zpgY):
            store_zpg_index(cpu, cpu->reg_x, cpu->reg_y);
            break;
        case(ASL_a):
            set_carry(cpu, cpu->accumulator >> 7);
            set_a(cpu, cpu->accumulator << 1);
            fetch(cpu);
            break;
        case(ASL_zpg):
        case i_SLO_zpg:
            switch(cpu->instruction_state) {
                case(T1):
                    ZPG(cpu);
                    break;
                case(T2):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;

                    asl_smallbuffer(cpu);
                    cpu->instruction_state++;
                    break;
                case(T3):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    if(cpu->instruction_buffer == i_SLO_zpg) {
                        set_a(cpu, cpu->accumulator | cpu->smallbuffer);
                    }
                    break;
                case(T4):
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(ASL_zpgX):
        case i_SLO_zpgX:
            switch(cpu->instruction_state) {
                case(T1): ZPG_X(cpu); break;
                case(T2): ZPG_X(cpu); break;
                case(T3):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    asl_smallbuffer(cpu);
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    if(cpu->instruction_buffer == i_SLO_zpgX) {
                        set_a(cpu, cpu->accumulator | cpu->smallbuffer);
                    }
                    break;
                case(T5):
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(ALS_abs):
        case i_SLO_abs:
            switch(cpu->instruction_state) {
                case(T1): ABS(cpu); break;
                case(T2): ABS(cpu); break;
                case(T3):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    asl_smallbuffer(cpu);
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    if(cpu->instruction_buffer == i_SLO_abs) {
                        set_a(cpu, cpu->accumulator | cpu->smallbuffer);
                    }
                    break;
                case(T5):
                    fetch(cpu); break;
                INVALID_DEFAULT
            }
            break;
        case(ASL_absX):
        case i_SLO_absX:
            if(ABS_X_WRITE(cpu)) {
                asl_smallbuffer(cpu);
                if(cpu->instruction_buffer == i_SLO_absX) {
                    set_a(cpu, cpu->accumulator | cpu->smallbuffer);
                }
            } break;
        case i_SLO_absY:
            if(ABS_Y_WRITE(cpu)) {
                asl_smallbuffer(cpu);
                if(cpu->instruction_buffer == i_SLO_absY) {
                    set_a(cpu, cpu->accumulator | cpu->smallbuffer);
                }
            } break;
        case(LSR_a):
            set_carry(cpu, cpu->accumulator & 0x1);
            set_a(cpu, cpu->accumulator >> 1);
            fetch(cpu);
            break;
        case(LSR_zpg):
            switch(cpu->instruction_state) {
                case(T1):
                    ZPG(cpu);
                    break;
                case(T2):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;

                    lsr_smallbuffer(cpu);
                    cpu->instruction_state++;
                    break;
                case(T3):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    break;
                case(T4):
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(LSR_zpgX):
            switch(cpu->instruction_state) {
                case(T1): ZPG_X(cpu); break;
                case(T2): ZPG_X(cpu); break;
                case(T3):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    lsr_smallbuffer(cpu);
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    break;
                case(T5):
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(LSR_abs):
            switch(cpu->instruction_state) {
                case(T1): ABS(cpu); break;
                case(T2): ABS(cpu); break;
                case(T3):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;

                    lsr_smallbuffer(cpu);
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    break;
                case(T5):
                    fetch(cpu); break;
                INVALID_DEFAULT
            }
            break;
        case(LSR_absX):
            if(ABS_X_WRITE(cpu)) {
                lsr_smallbuffer(cpu);
            } break;
        case(TSX):
            set_x(cpu, cpu->stack_ptr);
            fetch(cpu);
            break;
        case(TAX):
            set_x(cpu, cpu->accumulator);
            fetch(cpu);
            break;
        case(TAY):
            set_y(cpu, cpu->accumulator);
            fetch(cpu);
            break;
        case(STA_indY):
            switch(cpu->instruction_state) {
                case(T1):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->program_counter++;
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->smallbuffer;
                    cpu->smallbuffer++;
                    cpu->instruction_state++;
                    break;
                case(T2):
                    cpu->buffer = read_data(cpu);
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    break;
                case(T3):
                    // We do the calculation in a 8-bit int to simulate possible overflow
                    cpu->smallbuffer = cpu->buffer + cpu->reg_y;
                    bool carry = cpu->smallbuffer < cpu->buffer;
                    cpu->buffer = ((read_data(cpu) + (carry ? 1 : 0)) << 8);
                    cpu->buffer |= cpu->smallbuffer;
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->buffer;
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->accumulator;
                    cpu->instruction_state++;
                    break;
                case(T5): fetch(cpu); break;
                INVALID_DEFAULT
            }
            break;
        case(RTS):
            switch(cpu->instruction_state) {
                case(T1):
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->stack_ptr + STACK_START;
                    cpu->stack_ptr++;
                    cpu->instruction_state++;
                    break;
                case(T2):
                    // This is a dummy read
                    read_data(cpu);
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->stack_ptr + STACK_START;
                    cpu->stack_ptr++;
                    cpu->instruction_state++;
                    break;
                case(T3):
                    cpu->buffer = read_data(cpu);
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->stack_ptr + STACK_START;
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->buffer |= read_data(cpu) << 8;
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->buffer;
                    cpu->instruction_state++;
                    break;
                case(T5):
                    cpu->program_counter = cpu->buffer+1;
					stack_pop(&cpu->stacktrace);
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(BIT_abs):
            switch(cpu->instruction_state) {
                case(T1):
                    ABS(cpu);
                    break;
                case(T2):
                    ABS(cpu);
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->buffer;
                    break;
				case(T3): {
                    byte_t value = read_data(cpu);
                    set_zero(cpu, (value & cpu->accumulator) == 0);
                    set_negative(cpu, value >> 7);
                    set_overflow(cpu, (value >> 6) & 0x1);
                    fetch(cpu);
                    break; }
                INVALID_DEFAULT
            }
            break;
        case(BIT_zpg):
            switch(cpu->instruction_state) {
                case(T1):
                    ZPG(cpu);
                    break;
                case(T2):
                    byte_t value = read_data(cpu);
                    set_zero(cpu, (value & cpu->accumulator) == 0);
                    set_negative(cpu, value >> 7);
                    set_overflow(cpu, (value >> 6) & 0x1);
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(ORA_imm):
            set_a(cpu, cpu->accumulator | IMM(cpu));
            fetch(cpu);
            break;
        case(ORA_zpg):
            switch(cpu->instruction_state) {
                case(T1):
                    ZPG(cpu);
                    break;
                case(T2):
                    set_a(cpu, cpu->accumulator | read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(ORA_zpgX):
            switch(cpu->instruction_state) {
                case(T1): ZPG_X(cpu); break;
                case(T2): ZPG_X(cpu); break;
                case(T3):
                    set_a(cpu, cpu->accumulator | read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(ORA_abs):
            switch(cpu->instruction_state) {
                case(T1): ABS(cpu); break;
                case(T2):
                    ABS(cpu);
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->buffer;
                    break;
                case(T3):
                    set_a(cpu, cpu->accumulator | read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(ORA_absX):
            if(ABS_I(cpu, cpu->reg_x)) {
                set_a(cpu, cpu->accumulator | read_data(cpu));
                fetch(cpu);
            } break;
        case(ORA_absY):
            if(ABS_I(cpu, cpu->reg_y)) {
                set_a(cpu, cpu->accumulator | read_data(cpu));
                fetch(cpu);
            } break;
        case(AND_imm):
            set_a(cpu, cpu->accumulator & IMM(cpu));
            fetch(cpu);
            break;
        case(AND_zpg):
            switch(cpu->instruction_state) {
                case(T1): ZPG(cpu); break;
                case(T2):
                    set_a(cpu, cpu->accumulator & read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(AND_zpgX):
            switch(cpu->instruction_state) {
                case(T1): ZPG_X(cpu); break;
                case(T2): ZPG_X(cpu); break;
                case(T3):
                    set_a(cpu, cpu->accumulator & read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(AND_abs):
            switch(cpu->instruction_state) {
                case(T1): ABS(cpu); break;
                case(T2):
                    ABS(cpu);
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->buffer;
                    break;
                case(T3):
                    set_a(cpu, cpu->accumulator & read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(AND_absY):
            if(ABS_I(cpu, cpu->reg_y)) {
                set_a(cpu, cpu->accumulator & read_data(cpu));
                fetch(cpu);
            }
            break;
        case(AND_absX):
            if(ABS_I(cpu, cpu->reg_x)) {
                set_a(cpu, cpu->accumulator & read_data(cpu));
                fetch(cpu);
            } break;
        case(EOR_imm):
            set_a(cpu, cpu->accumulator ^ IMM(cpu));
            fetch(cpu);
            break;
        case(EOR_zpg):
            switch(cpu->instruction_state) {
                case(T1): ZPG(cpu); break;
                case(T2):
                    set_a(cpu, cpu->accumulator ^ read_data(cpu));
                    fetch(cpu);
                    break;
                default:
                    log_error("Invalid instruction state");
                    return 2;
            } break;
        case(EOR_zpgX):
            switch(cpu->instruction_state) {
                case(T1): ZPG_X(cpu); break;
                case(T2): ZPG_X(cpu); break;
                case(T3):
                    set_a(cpu, cpu->accumulator ^ read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(EOR_abs):
            switch(cpu->instruction_state) {
                case(T1): ABS(cpu); break;
                case(T2):
                    ABS(cpu);
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->buffer;
                    break;
                case(T3):
                    set_a(cpu, cpu->accumulator ^ read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(EOR_absY):
            if(ABS_I(cpu, cpu->reg_y)) {
                set_a(cpu, cpu->accumulator ^ read_data(cpu));
                fetch(cpu);
            } break;
        case(EOR_absX):
            if(ABS_I(cpu, cpu->reg_x)) {
                set_a(cpu, cpu->accumulator ^ read_data(cpu));
                fetch(cpu);
            }
            break;
        case(JMP_abs):
            switch(cpu->instruction_state) {
                case(T1): ABS(cpu); break;
                case(T2):
                    ABS(cpu);
                    cpu->program_counter = cpu->buffer;
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(JMP_ind):
            switch(cpu->instruction_state) {
                case(T1): ABS(cpu); break;
                case(T2):
                    ABS(cpu);
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->buffer;
                    // Match the indirect jump behavior when on a page boundary
                    if((cpu->buffer & 0x00FF) == 0xFF)
                        cpu->buffer &= 0xFF00;
                    else
                        cpu->buffer++;
                    break;
                case(T3):
                    cpu->buffer2 = read_data(cpu);
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->buffer;
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->buffer2 |= read_data(cpu) << 8;
                    cpu->instruction_state++;
                    cpu->program_counter = cpu->buffer2;
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(PLA):
            switch(cpu->instruction_state) {
                case(T1):
                    cpu->bus_mode = Read;
                    *cpu->address_pins = STACK_START + cpu->stack_ptr;
                    cpu->stack_ptr++;
                    cpu->instruction_state++;
                    break;
                case(T2):
                    cpu->bus_mode = Read;
                    *cpu->address_pins = STACK_START + cpu->stack_ptr;
                    cpu->instruction_state++;
                    break;
                case(T3):
                    set_a(cpu, read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(PLP):
            switch(cpu->instruction_state) {
                case(T1):
                    cpu->bus_mode = Read;
                    *cpu->address_pins = STACK_START + cpu->stack_ptr;
                    cpu->stack_ptr++;
                    cpu->instruction_state++;
                    break;
                case(T2):
                    cpu->bus_mode = Read;
                    *cpu->address_pins = STACK_START + cpu->stack_ptr;
                    cpu->instruction_state++;
                    break;
                case(T3):
                    // Because the const and break bit are ignored when pulling
                    // we have to save them and apply them afterwards
                    byte_t old_ignored = cpu->flags & (ConstMask |  BreakMask);
                    cpu->flags = read_data(cpu);
                    cpu->flags &= ~(ConstMask | BreakMask);
                    cpu->flags |= old_ignored;
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(ROL_a):
            byte_t carry = flag_set(cpu, CarryMask) ? 0x01 : 0x00;
            set_carry(cpu, cpu->accumulator >> 7);
            set_a(cpu, (cpu->accumulator << 1) | carry);
            fetch(cpu);
            break;
        case(ROL_zpg):
        case(i_RLA_zpg):
            switch(cpu->instruction_state) {
                case(T1): ZPG(cpu); break;
                case(T2):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;

                    rol_smallbuffer(cpu);
                    cpu->instruction_state++;
                    break;
                case(T3):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    if(cpu->instruction_buffer == i_RLA_zpg) {
                        set_a(cpu, cpu->accumulator & cpu->smallbuffer);
                    }
                    break;
                case(T4): fetch(cpu); break;
                INVALID_DEFAULT
            }
            break;
        case(ROL_zpgX):
        case(i_RLA_zpgX):
            switch(cpu->instruction_state) {
                case(T1): ZPG_X(cpu); break;
                case(T2): ZPG_X(cpu); break;
                case(T3):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    rol_smallbuffer(cpu);
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    if(cpu->instruction_buffer == i_RLA_zpgX) {
                        set_a(cpu, cpu->accumulator & cpu->smallbuffer);
                    }
                    break;
                case(T5): fetch(cpu); break;
                INVALID_DEFAULT
            } break;
        case(ROL_abs):
        case(i_RLA_abs):
            switch(cpu->instruction_state) {
                case(T1): ABS(cpu); break;
                case(T2): ABS(cpu); break;
                case(T3):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    rol_smallbuffer(cpu);
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    if(cpu->instruction_buffer == i_RLA_abs) {
                        set_a(cpu, cpu->accumulator & cpu->smallbuffer);
                    }
                    break;
                case(T5):
                    fetch(cpu); break;
                INVALID_DEFAULT
            }
            break;
        case(ROL_absX):
        case(i_RLA_absX):
            if(ABS_X_WRITE(cpu)) {
                rol_smallbuffer(cpu);
                if(cpu->instruction_buffer == i_RLA_absX) {
                    set_a(cpu, cpu->accumulator & cpu->smallbuffer);
                }
            } break;
        case(i_RLA_absY):
            if(ABS_Y_WRITE(cpu)) {
                rol_smallbuffer(cpu);
                set_a(cpu, cpu->accumulator & cpu->smallbuffer);
            } break;
        case(ROR_a): {
            byte_t carry = flag_set(cpu, CarryMask) ? 0x01 : 0x00;
            set_carry(cpu, cpu->accumulator & 0x1);
            set_a(cpu, (cpu->accumulator >> 1) | (carry << 7));
            fetch(cpu);
        } break;
        case(ROR_zpg):
            switch(cpu->instruction_state) {
                case(T1):
                    ZPG(cpu);
                    break;
                case(T2):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    ror_smallbuffer(cpu);
                    cpu->instruction_state++;
                    break;
                case(T3):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    break;
                case(T4):
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(ROR_zpgX):
            switch(cpu->instruction_state) {
                case(T1): ZPG_X(cpu); break;
                case(T2): ZPG_X(cpu); break;
                case(T3):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    ror_smallbuffer(cpu);
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    break;
                case(T5):
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(ROR_abs):
            switch(cpu->instruction_state) {
                case(T1): ABS(cpu); break;
                case(T2): ABS(cpu); break;
                case(T3):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    ror_smallbuffer(cpu);
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    break;
                case(T5):
                    fetch(cpu); break;
                INVALID_DEFAULT
            }
            break;
        case(ROR_absX):
            if(ABS_X_WRITE(cpu)) {
                ror_smallbuffer(cpu);
            } break;
        case(ADC_imm):
            adc(cpu, IMM(cpu));
            fetch(cpu);
            break;
        case(ADC_zpg):
            switch(cpu->instruction_state) {
                case(T1):
                    ZPG(cpu);
                    break;
                case(T2):
                    adc(cpu, read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(ADC_zpgX):
            switch(cpu->instruction_state) {
                case(T1): ZPG_X(cpu); break;
                case(T2): ZPG_X(cpu); break;
                case(T3):
                    adc(cpu, read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(ADC_abs):
            switch(cpu->instruction_state) {
                case(T1):
                    ABS(cpu);
                    break;
                case(T2):
                    ABS(cpu);
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->buffer;
                    break;
                case(T3):
                    adc(cpu, read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(ADC_absY):
            if(ABS_I(cpu, cpu->reg_y)) {
                adc(cpu, read_data(cpu));
                fetch(cpu);
            }
            break;
        case(ADC_absX):
            if(ABS_I(cpu, cpu->reg_x)) {
                adc(cpu, read_data(cpu));
                fetch(cpu);
            } break;
        case(SBC_imm):
        case(i_USBC):
            adc(cpu, ~IMM(cpu));
            fetch(cpu);
            break;
        case(SBC_zpg):
            switch(cpu->instruction_state) {
                case(T1):
                    ZPG(cpu);
                    break;
                case(T2):
                    adc(cpu, ~read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(SBC_zpgX):
            switch(cpu->instruction_state) {
                case(T1): ZPG_X(cpu); break;
                case(T2): ZPG_X(cpu); break;
                case(T3):
                    adc(cpu, ~read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(SBC_abs):
            switch(cpu->instruction_state) {
                case(T1):
                    ABS(cpu);
                    break;
                case(T2):
                    ABS(cpu);
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->buffer;
                    break;
                case(T3):
                    adc(cpu, ~read_data(cpu));
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(SBC_absY):
            if(ABS_I(cpu, cpu->reg_y)) {
                adc(cpu, ~read_data(cpu));
                fetch(cpu);
            } break;
        case(SBC_absX):
            if(ABS_I(cpu, cpu->reg_x)) {
                adc(cpu, ~read_data(cpu));
                fetch(cpu);
            } break;
        case(SBC_indX):
            if(cpu->instruction_state >= T1 && cpu->instruction_state <= T4)
                IND_X(cpu);
            else if(cpu->instruction_state == T5) {
                adc(cpu, ~read_data(cpu));
                fetch(cpu);
            } else {
                INVALID_STATE_STR;
                return 2;
            }
            break;
        case(SBC_indY):
            if(cpu->instruction_state >= T1 && cpu->instruction_state <= T5) {
                if(IND_Y(cpu)) {
                    adc(cpu, ~read_data(cpu));
                    fetch(cpu);
                }
            } else {
                INVALID_STATE_STR;
                return 2;
            } break;
        case(RTI):
            switch(cpu->instruction_state) {
                case(T1):
                    cpu->bus_mode = Read;
                    *cpu->address_pins = STACK_START + cpu->stack_ptr;
                    cpu->stack_ptr++;
                    cpu->instruction_state++;
                    break;
                case(T2):
                    cpu->bus_mode = Read;
                    *cpu->address_pins = STACK_START + cpu->stack_ptr;
                    cpu->stack_ptr++;
                    cpu->instruction_state++;
                    break;
                case(T3):
                    // Same as in PLP, we are ignoring the Const and Break flag
                    byte_t old_ignored = cpu->flags & (ConstMask |  BreakMask);
                    cpu->flags = read_data(cpu);
                    cpu->flags &= ~(ConstMask | BreakMask);
                    cpu->flags |= old_ignored;

                    cpu->bus_mode = Read;
                    *cpu->address_pins = STACK_START + cpu->stack_ptr;
                    cpu->stack_ptr++;
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->buffer = read_data(cpu);
                    cpu->bus_mode = Read;
                    *cpu->address_pins = STACK_START + cpu->stack_ptr;
                    cpu->instruction_state++;
                    break;
                case(T5):
                    cpu->buffer |= read_data(cpu) << 8;
                    cpu->program_counter = cpu->buffer;
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(NOP):
        case(i_NOP_a):
        case(i_NOP_b):
        case(i_NOP_c):
        case(i_NOP_d):
        case(i_NOP_e):
        case(i_NOP_f):
            fetch(cpu);
            break;
        case(i_NOP_imm_a):
        case(i_NOP_imm_b):
        case(i_NOP_imm_c):
        case(i_NOP_imm_d):
        case(i_NOP_imm_e):
            __attribute_maybe_unused__ byte_t a = IMM(cpu);
            fetch(cpu);
            break;
        case(i_NOP_zpg_a):
        case(i_NOP_zpg_b):
        case(i_NOP_zpg_c):
            switch(cpu->instruction_state) {
                case T1: ZPG(cpu); break;
                case T2: fetch(cpu); break;
                INVALID_DEFAULT
            } break;
        case(i_NOP_zpgX_a):
        case(i_NOP_zpgX_b):
        case(i_NOP_zpgX_c):
        case(i_NOP_zpgX_d):
        case(i_NOP_zpgX_e):
        case(i_NOP_zpgX_f):
            switch(cpu->instruction_state) {
                case T1: ZPG_X(cpu); break;
                case T2: ZPG_X(cpu); break;
                case T3: fetch(cpu); break;
                INVALID_DEFAULT
            } break;
        case(i_NOP_abs):
            switch(cpu->instruction_state) {
                case T1: ABS(cpu); break;
                case T2: ABS(cpu); break;
                case T3: fetch(cpu); break;
                INVALID_DEFAULT
            } break;
        case(i_NOP_absX_a):
        case(i_NOP_absX_b):
        case(i_NOP_absX_c):
        case(i_NOP_absX_d):
        case(i_NOP_absX_e):
        case(i_NOP_absX_f):
            if(ABS_X(cpu)) {
                fetch(cpu);
            } break;
        case(LDA_indX):
            if(cpu->instruction_state >= T1 && cpu->instruction_state <= T4) {
                IND_X(cpu);
            } else if (cpu->instruction_state == T5) {
                set_a(cpu, read_data(cpu));
                fetch(cpu);
            } else {
                INVALID_STATE_STR;
                return 2;
            }
            break;
        case(ORA_indX):
            if(cpu->instruction_state >= T1 && cpu->instruction_state <= T4)
                IND_X(cpu);
            else if(cpu->instruction_state == T5) {
                set_a(cpu, cpu->accumulator | read_data(cpu));
                fetch(cpu);
            } else {
                INVALID_STATE_STR;
                return 2;
            }
            break;
        case(ORA_indY):
            if(cpu->instruction_state >= T1 && cpu->instruction_state <= T5) {
                if(IND_Y(cpu)) {
                    set_a(cpu, cpu->accumulator | read_data(cpu));
                    fetch(cpu);
                }
            } else {
                INVALID_STATE_STR;
                return 2;
            } break;
        case(AND_indX):
            if(cpu->instruction_state >= T1 && cpu->instruction_state <= T4)
                IND_X(cpu);
            else if(cpu->instruction_state == T5) {
                set_a(cpu, cpu->accumulator & read_data(cpu));
                fetch(cpu);
            } else {
                INVALID_STATE_STR;
                return 2;
            }
            break;
        case(AND_indY):
            if(cpu->instruction_state >= T1 && cpu->instruction_state <= T5) {
                if(IND_Y(cpu)) {
                    set_a(cpu, cpu->accumulator & read_data(cpu));
                    fetch(cpu);
                }
            } else {
                INVALID_STATE_STR;
                return 2;
            } break;
        case(EOR_indX):
            if(cpu->instruction_state >= T1 && cpu->instruction_state <= T4)
                IND_X(cpu);
            else if(cpu->instruction_state == T5) {
                set_a(cpu, cpu->accumulator ^ read_data(cpu));
                fetch(cpu);
            } else {
                INVALID_STATE_STR;
                return 2;
            }
            break;
        case(EOR_indY):
            if(cpu->instruction_state >= T1 && cpu->instruction_state <= T5) {
                if(IND_Y(cpu)) {
                    set_a(cpu, cpu->accumulator ^ read_data(cpu));
                    fetch(cpu);
                }
            } else {
                INVALID_STATE_STR;
                return 2;
            } break;
        case(ADC_indY):
            if(cpu->instruction_state >= T1 && cpu->instruction_state <= T5) {
                if(IND_Y(cpu)) {
                    adc(cpu, read_data(cpu));
                    fetch(cpu);
                }
            } else {
                INVALID_STATE_STR;
                return 2;
            } break;
        case(ADC_indX):
            if(cpu->instruction_state >= T1 && cpu->instruction_state <= T4)
                IND_X(cpu);
            else if(cpu->instruction_state == T5) {
                adc(cpu, read_data(cpu));
                fetch(cpu);
            } else {
                INVALID_STATE_STR;
                return 2;
            }
            break;
        case(CMP_indX):
            if(cpu->instruction_state >= T1 && cpu->instruction_state <= T4)
                IND_X(cpu);
            else if(cpu->instruction_state == T5) {
                cmp(cpu, cpu->accumulator, read_data(cpu));
                fetch(cpu);
            } else {
                INVALID_STATE_STR;
                return 2;
            }
            break;
        case(CMP_indY):
            if(cpu->instruction_state >= T1 && cpu->instruction_state <= T5) {
                if(IND_Y(cpu)) {
                    cmp(cpu, cpu->accumulator, read_data(cpu));
                    fetch(cpu);
                }
            } else {
                INVALID_STATE_STR;
                return 2;
            } break;
        case i_ANC_a:
        case i_ANC_b:
            set_a(cpu, cpu->accumulator & IMM(cpu));
            set_carry(cpu, cpu->accumulator >> 7);
            fetch(cpu);
            break;
        case JAM_a:
        case JAM_b:
        case JAM_c:
        case JAM_d:
        case JAM_e:
        case JAM_f:
        case JAM_g:
        case JAM_h:
        case JAM_i:
        case JAM_j:
        case JAM_k:
        case JAM_l:
            break;
        case i_LAS:
            if(ABS_Y(cpu)) {
                uint8_t m_and_sp = cpu->stack_ptr & read_data(cpu);
                set_a(cpu, m_and_sp);
                set_x(cpu, m_and_sp);
                cpu->stack_ptr = m_and_sp;
                fetch(cpu);
            }
            break;
        case i_ALR:
            cpu->smallbuffer = cpu->accumulator & IMM(cpu);
            lsr_smallbuffer(cpu);
            set_a(cpu, cpu->smallbuffer);
            fetch(cpu);
            break;
        case i_SBX: {
            uint8_t op = IMM(cpu);
            uint8_t old_x = cpu->reg_x;
            set_x(cpu, (cpu->accumulator & cpu->reg_x) - op);
            cmp(cpu, old_x & cpu->accumulator, op);
            fetch(cpu);
            break; }
        case i_ISC_zpg:
            switch(cpu->instruction_state) {
                case T1: ZPG(cpu); break;
                case T2:
                    cpu->smallbuffer = read_data(cpu);
                    *cpu->address_pins = cpu->buffer;
                    cpu->bus_mode = Write;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->smallbuffer++;
                    cpu->instruction_state++;
                    break;
                case T3:
                    adc(cpu, ~cpu->smallbuffer);
                    *cpu->address_pins = cpu->buffer;
                    cpu->bus_mode = Write;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    break;
                case T4:
                    fetch(cpu);
                    break;
                INVALID_DEFAULT;
            } break;
        case i_ISC_zpgX:
            switch(cpu->instruction_state) {
                case T1: ZPG_X(cpu); break;
                case T2: ZPG_X(cpu); break;
                case T3:
                    cpu->smallbuffer = read_data(cpu);
                    *cpu->address_pins = cpu->buffer;
                    cpu->bus_mode = Write;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->smallbuffer++;
                    cpu->instruction_state++;
                    break;
                case T4:
                    adc(cpu, ~cpu->smallbuffer);
                    *cpu->address_pins = cpu->buffer;
                    cpu->bus_mode = Write;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    break;
                case T5:
                    fetch(cpu);
                    break;
                INVALID_DEFAULT;
            } break;
        case i_ISC_abs:
            switch(cpu->instruction_state) {
                case T1: ABS(cpu); break;
                case T2: ABS(cpu); break;
                case T3:
                    cpu->smallbuffer = read_data(cpu);
                    *cpu->address_pins = cpu->buffer;
                    cpu->bus_mode = Write;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->smallbuffer++;
                    cpu->instruction_state++;
                    break;
                case T4:
                    adc(cpu, ~cpu->smallbuffer);
                    *cpu->address_pins = cpu->buffer;
                    cpu->bus_mode = Write;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    break;
                case T5:
                    fetch(cpu);
                    break;
                INVALID_DEFAULT;
            }
            break;
        case i_ISC_absX:
            if(ABS_X_WRITE(cpu)) {
                cpu->smallbuffer++;
                adc(cpu, ~cpu->smallbuffer);
            } break;
        case i_ISC_absY:
            if(ABS_Y_WRITE(cpu)) {
                cpu->smallbuffer++;
                adc(cpu, ~cpu->smallbuffer);
            } break;
        case i_SHA_absY:
            if(ABS_Y_WRITE(cpu)) {
                cpu->smallbuffer = cpu->accumulator & cpu->reg_x & ((cpu->buffer >> 8) + 1);
            } break;
        case i_SHA_indY:
            switch(cpu->instruction_state) {
                case(T1):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->program_counter++;
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->smallbuffer;
                    cpu->smallbuffer++;
                    cpu->instruction_state++;
                    break;
                case(T2):
                    cpu->buffer = read_data(cpu);
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->smallbuffer;
                    cpu->instruction_state++;
                    break;
                case(T3):
                    // We do the calculation in a 8-bit int to simulate possible overflow
                    cpu->smallbuffer = cpu->buffer + cpu->reg_y;
                    bool carry = cpu->smallbuffer < cpu->buffer;
                    cpu->buffer = ((read_data(cpu) + (carry ? 1 : 0)) << 8);
                    cpu->buffer |= cpu->smallbuffer;
                    cpu->bus_mode = Read;
                    *cpu->address_pins = cpu->buffer;
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->accumulator & cpu->reg_x & ((cpu->buffer >> 8) + 1);
                    cpu->instruction_state++;
                    break;
                case(T5): fetch(cpu); break;
                INVALID_DEFAULT
            }
            break;
        case i_SHX_absY:
            if(ABS_Y_WRITE(cpu)) {
                cpu->smallbuffer = cpu->reg_x & ((cpu->buffer >> 8) + 1);
            } break;
        case i_SHY_absX:
            if(ABS_X_WRITE(cpu)) {
                cpu->smallbuffer = cpu->reg_y & ((cpu->buffer >> 8) + 1);
            } break;
        case BRK:
            if(cpu->instruction_state == T1)
                cpu->program_counter++;
            interrupt_procedure(cpu, true);
            break;
        case(i_DCP_absX):
            if(ABS_X_WRITE(cpu)) {
                cpu->smallbuffer--;
                set_reg(cpu, NULL, cpu->smallbuffer);
				cmp(cpu, cpu->accumulator, cpu->smallbuffer);
            } break;
        case(i_DCP_indX):
			// TODO: This instruction seems to be incorrect and to actually need 8 cycles inestad of 7
            switch(cpu->instruction_state) {
                case(T1): IND_X(cpu); break;
                case(T2): IND_X(cpu); break;
                case(T3): IND_X(cpu); break;
                case(T4):
                    IND_X(cpu);
                case(T5):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = read_data(cpu) - 1;
                    cmp(cpu, cpu->accumulator, *cpu->data_pins);
					cpu->instruction_state++;
                    break;
                case(T6):
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(i_DCP_zpg):
            switch(cpu->instruction_state) {
                case(T1):
                    ZPG(cpu);
                    break;
                case(T2):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->smallbuffer--;
                    cpu->instruction_state++;
                    break;
                case(T3):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    set_reg(cpu, NULL, cpu->smallbuffer);
					cmp(cpu, cpu->accumulator, cpu->smallbuffer);
                    cpu->instruction_state++;
                    break;
                case(T4):
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            }
            break;
        case(i_DCP_zpgX):
            switch(cpu->instruction_state) {
                case(T1): ZPG_X(cpu); break;
                case(T2): ZPG_X(cpu); break;
                case(T3):
                    cpu->smallbuffer = read_data(cpu);
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    cpu->smallbuffer--;
                    cpu->instruction_state++;
                    break;
                case(T4):
                    cpu->bus_mode = Write;
                    *cpu->address_pins = cpu->buffer;
                    *cpu->data_pins = cpu->smallbuffer;
                    set_reg(cpu, NULL, cpu->smallbuffer);
					cmp(cpu, cpu->accumulator, cpu->smallbuffer);
                    cpu->instruction_state++;
                    break;
                case(T5):
                    fetch(cpu);
                    break;
                INVALID_DEFAULT
            } break;
        case(i_DCP_abs):
			if(cpu->instruction_state == T4)
				cmp(cpu, cpu->accumulator, cpu->smallbuffer);
            change_mem_abs(cpu, -1);
            break;
        case(i_DCP_absY):
            if(ABS_X_WRITE(cpu)) {
                cpu->smallbuffer--;
				cmp(cpu, cpu->accumulator, cpu->smallbuffer);
            } break;
        case(i_DCP_indY):
			// TODO: This instruction seems to be incorrect and actually need 8 cycles instead of 7
			if(cpu->instruction_state == T6) {
				fetch(cpu);
			} else if(IND_Y(cpu)) {
				cpu->smallbuffer = read_data(cpu);
				cpu->smallbuffer--;
				cpu->bus_mode = Write;
				*cpu->address_pins = cpu->buffer;
				*cpu->data_pins = cpu->smallbuffer;
				cpu->instruction_state = T6;
			} 
            break;
        default:
            log_error("Opcode %#02x is not supported (PC:%#02x) (%s %s)",
                      cpu->instruction_buffer, cpu->program_counter-1,
                      mnemonics[cpu->instruction_buffer], instr_addr_string(cpu->instruction_buffer));
			return 1;
	}
	cpu->cycle++;
	return 0;
}
