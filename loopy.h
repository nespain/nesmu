#ifndef LOOPY_H
#define LOOPY_H
#include <stdint.h>

void loopy_set_nt(uint16_t* loopy, uint8_t index);
void loopy_set_coarse_x(uint16_t* loopy, uint8_t value);
void loopy_set_coarse_y(uint16_t* loopy, uint8_t value);
void loopy_set_fine_y(uint16_t* loopy, uint8_t value);
void loopy_inc_coarse_x(uint16_t* loopy);
void loopy_inc_fine_y(uint16_t* loopy);
// Transfers the nametable and coarse data from one loopy register into another
void loopy_transfer_x(uint16_t* dst, uint16_t src);
void loopy_transfer_y(uint16_t* dst, uint16_t src);

uint16_t loopy_tile_addr(uint16_t loopy);
uint16_t loopy_attr_addr(uint16_t loopy);
uint8_t loopy_fine_y(uint16_t loopy);
uint8_t loopy_coarse_x(uint16_t loopy);
uint8_t loopy_coarse_y(uint16_t loopy);
uint8_t loopy_nt(uint16_t loopy);
uint8_t loopy_nt_x(uint16_t loopy);
uint8_t loopy_nt_y(uint16_t loopy);

#endif
