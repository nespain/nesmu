#ifndef CONTROLER_H
#define CONTROLER_H

#include <stdint.h>
#include <stdbool.h>

enum e_controllerbuttons {A_btn, B_btn, Select_Btn, Start_Btn, Up_Btn, Down_Btn, Left_Btn, Right_Btn, NumButtons};

typedef struct s_controller {
    uint16_t address;
    uint8_t buffer;
    uint8_t shift_register;
	bool buttons[NumButtons];
} NesController;

// Sets the corresponding button as pressed or not pressed in the buffer
void controller_set(NesController* ctr, enum e_controllerbuttons button, bool status);
// Saves the current state of the controller into it's shift register ready to be read
void controller_buffer(NesController* ctr);
// Returns another bit of the controller
uint8_t controller_read(NesController* ctr);

#endif
