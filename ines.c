#include "ines.h"
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "logger.h"

#define is_set(x, mask) (((x) & (mask)) == (mask))

void insert_cardridge(Cardridge* cardridge, const char *filename)
{
	FILE* f = fopen(filename, "rb");
	assert(f && "could not open cardridge file");

    cardridge->filename = malloc(strlen(filename) + 1);
    strcpy(cardridge->filename, filename);

	NesHeader header;
	fread(&header, 1, sizeof(NesHeader), f);
	assert(!strncmp("NES", header.title, 3));

    // Header processing
	bool has_trainer = header.flags[0] & 0x04;
	if(has_trainer) {
		printf("Found a trainer in file");
        fseek(f, 512, SEEK_CUR);
	}
    cardridge->nametable_mirroring = is_set(header.flags[0], 0x01) ? VerticalMirror : HorizontalMirror;
    cardridge->mapper_type = ((header.flags[1] >> 4) << 4) | (header.flags[0] >> 4);

    cardridge->num_chr_banks = header.chr_size;
    cardridge->num_prg_banks = header.prg_size;

    cardridge->PRG = malloc(cardridge->num_prg_banks * 0x4000);
    fread(cardridge->PRG, 0x4000, cardridge->num_prg_banks, f);

    cardridge->CHR = malloc(cardridge->num_chr_banks * 0x2000);
    fread(cardridge->CHR, 0x2000, cardridge->num_chr_banks, f);

    cardridge->battery_ram = NULL;
    if(header.flags[0] & 0x2) {
        cardridge->battery_ram = calloc(0x2000, 1);
    }

	cardridge->PRG_RAM = NULL;
	cardridge->PRG_RAM = malloc((header.prg_ram_size+1) * 0x2000);

    // Then we have chr ram. TODO: I assume a size of 8k for now
    if(cardridge->num_chr_banks == 0) {
        cardridge->CHR = malloc(0x2000);
    }

	fclose(f);

    if(cardridge->battery_ram != NULL) {
        char save_filename[strlen(filename) + 10];
        strcpy(save_filename, filename);
        strcat(save_filename, ".save");

        log_debug("Opening save file %s", save_filename);
        f = fopen(save_filename, "rb");
        if(f != NULL) {
            fread(cardridge->battery_ram, 0x2000, 1, f);
            fclose(f);
        }
    }

    for(int i=0; i<NUM_REGISTERS; i++) {
        cardridge->registers[i] = 0;
    }

    log_info("The cardridge does %s have battery backed ram at $6000-$7fff", cardridge->battery_ram != NULL ? "" : "NOT");
    log_info("The cardridge has %d PRG banks", cardridge->num_prg_banks);
    log_info("The cardridge has %d CHR banks", cardridge->num_chr_banks);
    log_info("Cardridge mapper: %03d", cardridge->mapper_type);
	
}
